export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** 
 * A date string, such as 2007-12-03, compliant with the ISO 8601 standard for
   * representation of dates and times using the Gregorian calendar.
 */
  Date: any,
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any,
};











export type BooleanQueryOperatorInput = {
  eq?: Maybe<Scalars['Boolean']>,
  ne?: Maybe<Scalars['Boolean']>,
  in?: Maybe<Array<Maybe<Scalars['Boolean']>>>,
  nin?: Maybe<Array<Maybe<Scalars['Boolean']>>>,
};


export type DateQueryOperatorInput = {
  eq?: Maybe<Scalars['Date']>,
  ne?: Maybe<Scalars['Date']>,
  gt?: Maybe<Scalars['Date']>,
  gte?: Maybe<Scalars['Date']>,
  lt?: Maybe<Scalars['Date']>,
  lte?: Maybe<Scalars['Date']>,
  in?: Maybe<Array<Maybe<Scalars['Date']>>>,
  nin?: Maybe<Array<Maybe<Scalars['Date']>>>,
};

export type Directory = Node & {
  sourceInstanceName: Scalars['String'],
  absolutePath: Scalars['String'],
  relativePath: Scalars['String'],
  extension: Scalars['String'],
  size: Scalars['Int'],
  prettySize: Scalars['String'],
  modifiedTime: Scalars['Date'],
  accessTime: Scalars['Date'],
  changeTime: Scalars['Date'],
  birthTime: Scalars['Date'],
  root: Scalars['String'],
  dir: Scalars['String'],
  base: Scalars['String'],
  ext: Scalars['String'],
  name: Scalars['String'],
  relativeDirectory: Scalars['String'],
  dev: Scalars['Int'],
  mode: Scalars['Int'],
  nlink: Scalars['Int'],
  uid: Scalars['Int'],
  gid: Scalars['Int'],
  rdev: Scalars['Int'],
  ino: Scalars['Float'],
  atimeMs: Scalars['Float'],
  mtimeMs: Scalars['Float'],
  ctimeMs: Scalars['Float'],
  atime: Scalars['Date'],
  mtime: Scalars['Date'],
  ctime: Scalars['Date'],
  birthtime?: Maybe<Scalars['Date']>,
  birthtimeMs?: Maybe<Scalars['Float']>,
  blksize?: Maybe<Scalars['Int']>,
  blocks?: Maybe<Scalars['Int']>,
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
};


export type DirectoryModifiedTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryAccessTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryChangeTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryBirthTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryAtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryMtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryCtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type DirectoryConnection = {
  totalCount: Scalars['Int'],
  edges: Array<DirectoryEdge>,
  nodes: Array<Directory>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<DirectoryGroupConnection>,
};


export type DirectoryConnectionDistinctArgs = {
  field: DirectoryFieldsEnum
};


export type DirectoryConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: DirectoryFieldsEnum
};

export type DirectoryEdge = {
  next?: Maybe<Directory>,
  node: Directory,
  previous?: Maybe<Directory>,
};

export type DirectoryFieldsEnum = 
  'sourceInstanceName' |
  'absolutePath' |
  'relativePath' |
  'extension' |
  'size' |
  'prettySize' |
  'modifiedTime' |
  'accessTime' |
  'changeTime' |
  'birthTime' |
  'root' |
  'dir' |
  'base' |
  'ext' |
  'name' |
  'relativeDirectory' |
  'dev' |
  'mode' |
  'nlink' |
  'uid' |
  'gid' |
  'rdev' |
  'ino' |
  'atimeMs' |
  'mtimeMs' |
  'ctimeMs' |
  'atime' |
  'mtime' |
  'ctime' |
  'birthtime' |
  'birthtimeMs' |
  'blksize' |
  'blocks' |
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type';

export type DirectoryFilterInput = {
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  absolutePath?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  ino?: Maybe<FloatQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
};

export type DirectoryGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<DirectoryEdge>,
  nodes: Array<Directory>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type DirectorySortInput = {
  fields?: Maybe<Array<Maybe<DirectoryFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type DuotoneGradient = {
  highlight: Scalars['String'],
  shadow: Scalars['String'],
  opacity?: Maybe<Scalars['Int']>,
};

export type File = Node & {
  sourceInstanceName: Scalars['String'],
  absolutePath: Scalars['String'],
  relativePath: Scalars['String'],
  extension: Scalars['String'],
  size: Scalars['Int'],
  prettySize: Scalars['String'],
  modifiedTime: Scalars['Date'],
  accessTime: Scalars['Date'],
  changeTime: Scalars['Date'],
  birthTime: Scalars['Date'],
  root: Scalars['String'],
  dir: Scalars['String'],
  base: Scalars['String'],
  ext: Scalars['String'],
  name: Scalars['String'],
  relativeDirectory: Scalars['String'],
  dev: Scalars['Int'],
  mode: Scalars['Int'],
  nlink: Scalars['Int'],
  uid: Scalars['Int'],
  gid: Scalars['Int'],
  rdev: Scalars['Int'],
  ino: Scalars['Float'],
  atimeMs: Scalars['Float'],
  mtimeMs: Scalars['Float'],
  ctimeMs: Scalars['Float'],
  atime: Scalars['Date'],
  mtime: Scalars['Date'],
  ctime: Scalars['Date'],
  birthtime?: Maybe<Scalars['Date']>,
  birthtimeMs?: Maybe<Scalars['Float']>,
  blksize?: Maybe<Scalars['Int']>,
  blocks?: Maybe<Scalars['Int']>,
  /** Copy file to static directory and return public url to it */
  publicURL?: Maybe<Scalars['String']>,
  childImageSharp?: Maybe<ImageSharp>,
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  childMarkdownRemark?: Maybe<MarkdownRemark>,
};


export type FileModifiedTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileAccessTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileChangeTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileBirthTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileAtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileMtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type FileCtimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type FileConnection = {
  totalCount: Scalars['Int'],
  edges: Array<FileEdge>,
  nodes: Array<File>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<FileGroupConnection>,
};


export type FileConnectionDistinctArgs = {
  field: FileFieldsEnum
};


export type FileConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: FileFieldsEnum
};

export type FileEdge = {
  next?: Maybe<File>,
  node: File,
  previous?: Maybe<File>,
};

export type FileFieldsEnum = 
  'sourceInstanceName' |
  'absolutePath' |
  'relativePath' |
  'extension' |
  'size' |
  'prettySize' |
  'modifiedTime' |
  'accessTime' |
  'changeTime' |
  'birthTime' |
  'root' |
  'dir' |
  'base' |
  'ext' |
  'name' |
  'relativeDirectory' |
  'dev' |
  'mode' |
  'nlink' |
  'uid' |
  'gid' |
  'rdev' |
  'ino' |
  'atimeMs' |
  'mtimeMs' |
  'ctimeMs' |
  'atime' |
  'mtime' |
  'ctime' |
  'birthtime' |
  'birthtimeMs' |
  'blksize' |
  'blocks' |
  'publicURL' |
  'childImageSharp___fixed___base64' |
  'childImageSharp___fixed___tracedSVG' |
  'childImageSharp___fixed___aspectRatio' |
  'childImageSharp___fixed___width' |
  'childImageSharp___fixed___height' |
  'childImageSharp___fixed___src' |
  'childImageSharp___fixed___srcSet' |
  'childImageSharp___fixed___srcWebp' |
  'childImageSharp___fixed___srcSetWebp' |
  'childImageSharp___fixed___originalName' |
  'childImageSharp___resolutions___base64' |
  'childImageSharp___resolutions___tracedSVG' |
  'childImageSharp___resolutions___aspectRatio' |
  'childImageSharp___resolutions___width' |
  'childImageSharp___resolutions___height' |
  'childImageSharp___resolutions___src' |
  'childImageSharp___resolutions___srcSet' |
  'childImageSharp___resolutions___srcWebp' |
  'childImageSharp___resolutions___srcSetWebp' |
  'childImageSharp___resolutions___originalName' |
  'childImageSharp___fluid___base64' |
  'childImageSharp___fluid___tracedSVG' |
  'childImageSharp___fluid___aspectRatio' |
  'childImageSharp___fluid___src' |
  'childImageSharp___fluid___srcSet' |
  'childImageSharp___fluid___srcWebp' |
  'childImageSharp___fluid___srcSetWebp' |
  'childImageSharp___fluid___sizes' |
  'childImageSharp___fluid___originalImg' |
  'childImageSharp___fluid___originalName' |
  'childImageSharp___fluid___presentationWidth' |
  'childImageSharp___fluid___presentationHeight' |
  'childImageSharp___sizes___base64' |
  'childImageSharp___sizes___tracedSVG' |
  'childImageSharp___sizes___aspectRatio' |
  'childImageSharp___sizes___src' |
  'childImageSharp___sizes___srcSet' |
  'childImageSharp___sizes___srcWebp' |
  'childImageSharp___sizes___srcSetWebp' |
  'childImageSharp___sizes___sizes' |
  'childImageSharp___sizes___originalImg' |
  'childImageSharp___sizes___originalName' |
  'childImageSharp___sizes___presentationWidth' |
  'childImageSharp___sizes___presentationHeight' |
  'childImageSharp___original___width' |
  'childImageSharp___original___height' |
  'childImageSharp___original___src' |
  'childImageSharp___resize___src' |
  'childImageSharp___resize___tracedSVG' |
  'childImageSharp___resize___width' |
  'childImageSharp___resize___height' |
  'childImageSharp___resize___aspectRatio' |
  'childImageSharp___resize___originalName' |
  'childImageSharp___id' |
  'childImageSharp___parent___id' |
  'childImageSharp___parent___parent___id' |
  'childImageSharp___parent___parent___children' |
  'childImageSharp___parent___children' |
  'childImageSharp___parent___children___id' |
  'childImageSharp___parent___children___children' |
  'childImageSharp___parent___internal___content' |
  'childImageSharp___parent___internal___contentDigest' |
  'childImageSharp___parent___internal___description' |
  'childImageSharp___parent___internal___fieldOwners' |
  'childImageSharp___parent___internal___ignoreType' |
  'childImageSharp___parent___internal___mediaType' |
  'childImageSharp___parent___internal___owner' |
  'childImageSharp___parent___internal___type' |
  'childImageSharp___children' |
  'childImageSharp___children___id' |
  'childImageSharp___children___parent___id' |
  'childImageSharp___children___parent___children' |
  'childImageSharp___children___children' |
  'childImageSharp___children___children___id' |
  'childImageSharp___children___children___children' |
  'childImageSharp___children___internal___content' |
  'childImageSharp___children___internal___contentDigest' |
  'childImageSharp___children___internal___description' |
  'childImageSharp___children___internal___fieldOwners' |
  'childImageSharp___children___internal___ignoreType' |
  'childImageSharp___children___internal___mediaType' |
  'childImageSharp___children___internal___owner' |
  'childImageSharp___children___internal___type' |
  'childImageSharp___internal___content' |
  'childImageSharp___internal___contentDigest' |
  'childImageSharp___internal___description' |
  'childImageSharp___internal___fieldOwners' |
  'childImageSharp___internal___ignoreType' |
  'childImageSharp___internal___mediaType' |
  'childImageSharp___internal___owner' |
  'childImageSharp___internal___type' |
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'childMarkdownRemark___id' |
  'childMarkdownRemark___frontmatter___title' |
  'childMarkdownRemark___frontmatter___templateKey' |
  'childMarkdownRemark___frontmatter___draft' |
  'childMarkdownRemark___frontmatter___featured' |
  'childMarkdownRemark___frontmatter___featuredMedia___sourceInstanceName' |
  'childMarkdownRemark___frontmatter___featuredMedia___absolutePath' |
  'childMarkdownRemark___frontmatter___featuredMedia___relativePath' |
  'childMarkdownRemark___frontmatter___featuredMedia___extension' |
  'childMarkdownRemark___frontmatter___featuredMedia___size' |
  'childMarkdownRemark___frontmatter___featuredMedia___prettySize' |
  'childMarkdownRemark___frontmatter___featuredMedia___modifiedTime' |
  'childMarkdownRemark___frontmatter___featuredMedia___accessTime' |
  'childMarkdownRemark___frontmatter___featuredMedia___changeTime' |
  'childMarkdownRemark___frontmatter___featuredMedia___birthTime' |
  'childMarkdownRemark___frontmatter___featuredMedia___root' |
  'childMarkdownRemark___frontmatter___featuredMedia___dir' |
  'childMarkdownRemark___frontmatter___featuredMedia___base' |
  'childMarkdownRemark___frontmatter___featuredMedia___ext' |
  'childMarkdownRemark___frontmatter___featuredMedia___name' |
  'childMarkdownRemark___frontmatter___featuredMedia___relativeDirectory' |
  'childMarkdownRemark___frontmatter___featuredMedia___dev' |
  'childMarkdownRemark___frontmatter___featuredMedia___mode' |
  'childMarkdownRemark___frontmatter___featuredMedia___nlink' |
  'childMarkdownRemark___frontmatter___featuredMedia___uid' |
  'childMarkdownRemark___frontmatter___featuredMedia___gid' |
  'childMarkdownRemark___frontmatter___featuredMedia___rdev' |
  'childMarkdownRemark___frontmatter___featuredMedia___ino' |
  'childMarkdownRemark___frontmatter___featuredMedia___atimeMs' |
  'childMarkdownRemark___frontmatter___featuredMedia___mtimeMs' |
  'childMarkdownRemark___frontmatter___featuredMedia___ctimeMs' |
  'childMarkdownRemark___frontmatter___featuredMedia___atime' |
  'childMarkdownRemark___frontmatter___featuredMedia___mtime' |
  'childMarkdownRemark___frontmatter___featuredMedia___ctime' |
  'childMarkdownRemark___frontmatter___featuredMedia___birthtime' |
  'childMarkdownRemark___frontmatter___featuredMedia___birthtimeMs' |
  'childMarkdownRemark___frontmatter___featuredMedia___blksize' |
  'childMarkdownRemark___frontmatter___featuredMedia___blocks' |
  'childMarkdownRemark___frontmatter___featuredMedia___publicURL' |
  'childMarkdownRemark___frontmatter___featuredMedia___id' |
  'childMarkdownRemark___frontmatter___featuredMedia___children' |
  'childMarkdownRemark___frontmatter___date' |
  'childMarkdownRemark___frontmatter___stackshareUrl' |
  'childMarkdownRemark___frontmatter___repoUrl' |
  'childMarkdownRemark___frontmatter___liveUrl' |
  'childMarkdownRemark___frontmatter___description' |
  'childMarkdownRemark___frontmatter___path' |
  'childMarkdownRemark___frontmatter___hero___sourceInstanceName' |
  'childMarkdownRemark___frontmatter___hero___absolutePath' |
  'childMarkdownRemark___frontmatter___hero___relativePath' |
  'childMarkdownRemark___frontmatter___hero___extension' |
  'childMarkdownRemark___frontmatter___hero___size' |
  'childMarkdownRemark___frontmatter___hero___prettySize' |
  'childMarkdownRemark___frontmatter___hero___modifiedTime' |
  'childMarkdownRemark___frontmatter___hero___accessTime' |
  'childMarkdownRemark___frontmatter___hero___changeTime' |
  'childMarkdownRemark___frontmatter___hero___birthTime' |
  'childMarkdownRemark___frontmatter___hero___root' |
  'childMarkdownRemark___frontmatter___hero___dir' |
  'childMarkdownRemark___frontmatter___hero___base' |
  'childMarkdownRemark___frontmatter___hero___ext' |
  'childMarkdownRemark___frontmatter___hero___name' |
  'childMarkdownRemark___frontmatter___hero___relativeDirectory' |
  'childMarkdownRemark___frontmatter___hero___dev' |
  'childMarkdownRemark___frontmatter___hero___mode' |
  'childMarkdownRemark___frontmatter___hero___nlink' |
  'childMarkdownRemark___frontmatter___hero___uid' |
  'childMarkdownRemark___frontmatter___hero___gid' |
  'childMarkdownRemark___frontmatter___hero___rdev' |
  'childMarkdownRemark___frontmatter___hero___ino' |
  'childMarkdownRemark___frontmatter___hero___atimeMs' |
  'childMarkdownRemark___frontmatter___hero___mtimeMs' |
  'childMarkdownRemark___frontmatter___hero___ctimeMs' |
  'childMarkdownRemark___frontmatter___hero___atime' |
  'childMarkdownRemark___frontmatter___hero___mtime' |
  'childMarkdownRemark___frontmatter___hero___ctime' |
  'childMarkdownRemark___frontmatter___hero___birthtime' |
  'childMarkdownRemark___frontmatter___hero___birthtimeMs' |
  'childMarkdownRemark___frontmatter___hero___blksize' |
  'childMarkdownRemark___frontmatter___hero___blocks' |
  'childMarkdownRemark___frontmatter___hero___publicURL' |
  'childMarkdownRemark___frontmatter___hero___id' |
  'childMarkdownRemark___frontmatter___hero___children' |
  'childMarkdownRemark___frontmatter___subtitle' |
  'childMarkdownRemark___frontmatter___ingredients' |
  'childMarkdownRemark___frontmatter___ingredients___name' |
  'childMarkdownRemark___frontmatter___ingredients___quantity' |
  'childMarkdownRemark___frontmatter___ingredients___unit' |
  'childMarkdownRemark___frontmatter___instructions' |
  'childMarkdownRemark___frontmatter___servingSize' |
  'childMarkdownRemark___frontmatter___prepTime' |
  'childMarkdownRemark___frontmatter___cookTime' |
  'childMarkdownRemark___frontmatter___tags' |
  'childMarkdownRemark___excerpt' |
  'childMarkdownRemark___rawMarkdownBody' |
  'childMarkdownRemark___fileAbsolutePath' |
  'childMarkdownRemark___fields___slug' |
  'childMarkdownRemark___html' |
  'childMarkdownRemark___htmlAst' |
  'childMarkdownRemark___excerptAst' |
  'childMarkdownRemark___headings' |
  'childMarkdownRemark___headings___value' |
  'childMarkdownRemark___headings___depth' |
  'childMarkdownRemark___timeToRead' |
  'childMarkdownRemark___tableOfContents' |
  'childMarkdownRemark___wordCount___paragraphs' |
  'childMarkdownRemark___wordCount___sentences' |
  'childMarkdownRemark___wordCount___words' |
  'childMarkdownRemark___parent___id' |
  'childMarkdownRemark___parent___parent___id' |
  'childMarkdownRemark___parent___parent___children' |
  'childMarkdownRemark___parent___children' |
  'childMarkdownRemark___parent___children___id' |
  'childMarkdownRemark___parent___children___children' |
  'childMarkdownRemark___parent___internal___content' |
  'childMarkdownRemark___parent___internal___contentDigest' |
  'childMarkdownRemark___parent___internal___description' |
  'childMarkdownRemark___parent___internal___fieldOwners' |
  'childMarkdownRemark___parent___internal___ignoreType' |
  'childMarkdownRemark___parent___internal___mediaType' |
  'childMarkdownRemark___parent___internal___owner' |
  'childMarkdownRemark___parent___internal___type' |
  'childMarkdownRemark___children' |
  'childMarkdownRemark___children___id' |
  'childMarkdownRemark___children___parent___id' |
  'childMarkdownRemark___children___parent___children' |
  'childMarkdownRemark___children___children' |
  'childMarkdownRemark___children___children___id' |
  'childMarkdownRemark___children___children___children' |
  'childMarkdownRemark___children___internal___content' |
  'childMarkdownRemark___children___internal___contentDigest' |
  'childMarkdownRemark___children___internal___description' |
  'childMarkdownRemark___children___internal___fieldOwners' |
  'childMarkdownRemark___children___internal___ignoreType' |
  'childMarkdownRemark___children___internal___mediaType' |
  'childMarkdownRemark___children___internal___owner' |
  'childMarkdownRemark___children___internal___type' |
  'childMarkdownRemark___internal___content' |
  'childMarkdownRemark___internal___contentDigest' |
  'childMarkdownRemark___internal___description' |
  'childMarkdownRemark___internal___fieldOwners' |
  'childMarkdownRemark___internal___ignoreType' |
  'childMarkdownRemark___internal___mediaType' |
  'childMarkdownRemark___internal___owner' |
  'childMarkdownRemark___internal___type';

export type FileFilterInput = {
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  absolutePath?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  ino?: Maybe<FloatQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  publicURL?: Maybe<StringQueryOperatorInput>,
  childImageSharp?: Maybe<ImageSharpFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  childMarkdownRemark?: Maybe<MarkdownRemarkFilterInput>,
};

export type FileGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<FileEdge>,
  nodes: Array<File>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type FileSortInput = {
  fields?: Maybe<Array<Maybe<FileFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type FloatQueryOperatorInput = {
  eq?: Maybe<Scalars['Float']>,
  ne?: Maybe<Scalars['Float']>,
  gt?: Maybe<Scalars['Float']>,
  gte?: Maybe<Scalars['Float']>,
  lt?: Maybe<Scalars['Float']>,
  lte?: Maybe<Scalars['Float']>,
  in?: Maybe<Array<Maybe<Scalars['Float']>>>,
  nin?: Maybe<Array<Maybe<Scalars['Float']>>>,
};

export type GoodreadsAuthor = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  name?: Maybe<Scalars['String']>,
  role?: Maybe<Scalars['String']>,
  image_url?: Maybe<Scalars['String']>,
  small_image_url?: Maybe<Scalars['String']>,
  link?: Maybe<Scalars['String']>,
  average_rating?: Maybe<Scalars['String']>,
  ratings_count?: Maybe<Scalars['String']>,
  text_reviews_count?: Maybe<Scalars['String']>,
  goodreadsId?: Maybe<Scalars['String']>,
  books?: Maybe<Array<Maybe<GoodreadsBook>>>,
};

export type GoodreadsAuthorConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsAuthorEdge>,
  nodes: Array<GoodreadsAuthor>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<GoodreadsAuthorGroupConnection>,
};


export type GoodreadsAuthorConnectionDistinctArgs = {
  field: GoodreadsAuthorFieldsEnum
};


export type GoodreadsAuthorConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: GoodreadsAuthorFieldsEnum
};

export type GoodreadsAuthorEdge = {
  next?: Maybe<GoodreadsAuthor>,
  node: GoodreadsAuthor,
  previous?: Maybe<GoodreadsAuthor>,
};

export type GoodreadsAuthorFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'name' |
  'role' |
  'image_url' |
  'small_image_url' |
  'link' |
  'average_rating' |
  'ratings_count' |
  'text_reviews_count' |
  'goodreadsId' |
  'books' |
  'books___id' |
  'books___parent___id' |
  'books___parent___parent___id' |
  'books___parent___parent___children' |
  'books___parent___children' |
  'books___parent___children___id' |
  'books___parent___children___children' |
  'books___parent___internal___content' |
  'books___parent___internal___contentDigest' |
  'books___parent___internal___description' |
  'books___parent___internal___fieldOwners' |
  'books___parent___internal___ignoreType' |
  'books___parent___internal___mediaType' |
  'books___parent___internal___owner' |
  'books___parent___internal___type' |
  'books___children' |
  'books___children___id' |
  'books___children___parent___id' |
  'books___children___parent___children' |
  'books___children___children' |
  'books___children___children___id' |
  'books___children___children___children' |
  'books___children___internal___content' |
  'books___children___internal___contentDigest' |
  'books___children___internal___description' |
  'books___children___internal___fieldOwners' |
  'books___children___internal___ignoreType' |
  'books___children___internal___mediaType' |
  'books___children___internal___owner' |
  'books___children___internal___type' |
  'books___internal___content' |
  'books___internal___contentDigest' |
  'books___internal___description' |
  'books___internal___fieldOwners' |
  'books___internal___ignoreType' |
  'books___internal___mediaType' |
  'books___internal___owner' |
  'books___internal___type' |
  'books___isbn' |
  'books___isbn13' |
  'books___text_reviews_count' |
  'books___uri' |
  'books___title' |
  'books___title_without_series' |
  'books___image_url' |
  'books___small_image_url' |
  'books___large_image_url' |
  'books___link' |
  'books___num_pages' |
  'books___format' |
  'books___edition_information' |
  'books___publisher' |
  'books___publication_day' |
  'books___publication_year' |
  'books___publication_month' |
  'books___average_rating' |
  'books___ratings_count' |
  'books___description' |
  'books___published' |
  'books___work___id' |
  'books___work___uri' |
  'books___goodreadsId' |
  'books___reviews' |
  'books___reviews___id' |
  'books___reviews___parent___id' |
  'books___reviews___parent___children' |
  'books___reviews___children' |
  'books___reviews___children___id' |
  'books___reviews___children___children' |
  'books___reviews___internal___content' |
  'books___reviews___internal___contentDigest' |
  'books___reviews___internal___description' |
  'books___reviews___internal___fieldOwners' |
  'books___reviews___internal___ignoreType' |
  'books___reviews___internal___mediaType' |
  'books___reviews___internal___owner' |
  'books___reviews___internal___type' |
  'books___reviews___rating' |
  'books___reviews___votes' |
  'books___reviews___spoiler_flag' |
  'books___reviews___spoilers_state' |
  'books___reviews___recommended_for' |
  'books___reviews___recommended_by' |
  'books___reviews___started_at' |
  'books___reviews___read_at' |
  'books___reviews___date_added' |
  'books___reviews___date_updated' |
  'books___reviews___read_count' |
  'books___reviews___body' |
  'books___reviews___comments_count' |
  'books___reviews___url' |
  'books___reviews___link' |
  'books___reviews___owned' |
  'books___reviews___goodreadsId' |
  'books___reviews___book___id' |
  'books___reviews___book___children' |
  'books___reviews___book___isbn' |
  'books___reviews___book___isbn13' |
  'books___reviews___book___text_reviews_count' |
  'books___reviews___book___uri' |
  'books___reviews___book___title' |
  'books___reviews___book___title_without_series' |
  'books___reviews___book___image_url' |
  'books___reviews___book___small_image_url' |
  'books___reviews___book___large_image_url' |
  'books___reviews___book___link' |
  'books___reviews___book___num_pages' |
  'books___reviews___book___format' |
  'books___reviews___book___edition_information' |
  'books___reviews___book___publisher' |
  'books___reviews___book___publication_day' |
  'books___reviews___book___publication_year' |
  'books___reviews___book___publication_month' |
  'books___reviews___book___average_rating' |
  'books___reviews___book___ratings_count' |
  'books___reviews___book___description' |
  'books___reviews___book___published' |
  'books___reviews___book___goodreadsId' |
  'books___reviews___book___reviews' |
  'books___reviews___book___authors' |
  'books___authors' |
  'books___authors___id' |
  'books___authors___parent___id' |
  'books___authors___parent___children' |
  'books___authors___children' |
  'books___authors___children___id' |
  'books___authors___children___children' |
  'books___authors___internal___content' |
  'books___authors___internal___contentDigest' |
  'books___authors___internal___description' |
  'books___authors___internal___fieldOwners' |
  'books___authors___internal___ignoreType' |
  'books___authors___internal___mediaType' |
  'books___authors___internal___owner' |
  'books___authors___internal___type' |
  'books___authors___name' |
  'books___authors___role' |
  'books___authors___image_url' |
  'books___authors___small_image_url' |
  'books___authors___link' |
  'books___authors___average_rating' |
  'books___authors___ratings_count' |
  'books___authors___text_reviews_count' |
  'books___authors___goodreadsId' |
  'books___authors___books' |
  'books___authors___books___id' |
  'books___authors___books___children' |
  'books___authors___books___isbn' |
  'books___authors___books___isbn13' |
  'books___authors___books___text_reviews_count' |
  'books___authors___books___uri' |
  'books___authors___books___title' |
  'books___authors___books___title_without_series' |
  'books___authors___books___image_url' |
  'books___authors___books___small_image_url' |
  'books___authors___books___large_image_url' |
  'books___authors___books___link' |
  'books___authors___books___num_pages' |
  'books___authors___books___format' |
  'books___authors___books___edition_information' |
  'books___authors___books___publisher' |
  'books___authors___books___publication_day' |
  'books___authors___books___publication_year' |
  'books___authors___books___publication_month' |
  'books___authors___books___average_rating' |
  'books___authors___books___ratings_count' |
  'books___authors___books___description' |
  'books___authors___books___published' |
  'books___authors___books___goodreadsId' |
  'books___authors___books___reviews' |
  'books___authors___books___authors';

export type GoodreadsAuthorFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  role?: Maybe<StringQueryOperatorInput>,
  image_url?: Maybe<StringQueryOperatorInput>,
  small_image_url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  average_rating?: Maybe<StringQueryOperatorInput>,
  ratings_count?: Maybe<StringQueryOperatorInput>,
  text_reviews_count?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  books?: Maybe<GoodreadsBookFilterListInput>,
};

export type GoodreadsAuthorFilterListInput = {
  elemMatch?: Maybe<GoodreadsAuthorFilterInput>,
};

export type GoodreadsAuthorGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsAuthorEdge>,
  nodes: Array<GoodreadsAuthor>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type GoodreadsAuthorSortInput = {
  fields?: Maybe<Array<Maybe<GoodreadsAuthorFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type GoodreadsBook = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  isbn?: Maybe<Scalars['String']>,
  isbn13?: Maybe<Scalars['String']>,
  text_reviews_count?: Maybe<Scalars['String']>,
  uri?: Maybe<Scalars['String']>,
  title?: Maybe<Scalars['String']>,
  title_without_series?: Maybe<Scalars['String']>,
  image_url?: Maybe<Scalars['String']>,
  small_image_url?: Maybe<Scalars['String']>,
  large_image_url?: Maybe<Scalars['String']>,
  link?: Maybe<Scalars['String']>,
  num_pages?: Maybe<Scalars['String']>,
  format?: Maybe<Scalars['String']>,
  edition_information?: Maybe<Scalars['String']>,
  publisher?: Maybe<Scalars['String']>,
  publication_day?: Maybe<Scalars['String']>,
  publication_year?: Maybe<Scalars['Date']>,
  publication_month?: Maybe<Scalars['String']>,
  average_rating?: Maybe<Scalars['String']>,
  ratings_count?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  published?: Maybe<Scalars['Date']>,
  work?: Maybe<GoodreadsBookWork>,
  goodreadsId?: Maybe<Scalars['String']>,
  reviews?: Maybe<Array<Maybe<GoodreadsReview>>>,
  authors?: Maybe<Array<Maybe<GoodreadsAuthor>>>,
};


export type GoodreadsBookPublication_YearArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};


export type GoodreadsBookPublishedArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type GoodreadsBookConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsBookEdge>,
  nodes: Array<GoodreadsBook>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<GoodreadsBookGroupConnection>,
};


export type GoodreadsBookConnectionDistinctArgs = {
  field: GoodreadsBookFieldsEnum
};


export type GoodreadsBookConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: GoodreadsBookFieldsEnum
};

export type GoodreadsBookEdge = {
  next?: Maybe<GoodreadsBook>,
  node: GoodreadsBook,
  previous?: Maybe<GoodreadsBook>,
};

export type GoodreadsBookFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'isbn' |
  'isbn13' |
  'text_reviews_count' |
  'uri' |
  'title' |
  'title_without_series' |
  'image_url' |
  'small_image_url' |
  'large_image_url' |
  'link' |
  'num_pages' |
  'format' |
  'edition_information' |
  'publisher' |
  'publication_day' |
  'publication_year' |
  'publication_month' |
  'average_rating' |
  'ratings_count' |
  'description' |
  'published' |
  'work___id' |
  'work___uri' |
  'goodreadsId' |
  'reviews' |
  'reviews___id' |
  'reviews___parent___id' |
  'reviews___parent___parent___id' |
  'reviews___parent___parent___children' |
  'reviews___parent___children' |
  'reviews___parent___children___id' |
  'reviews___parent___children___children' |
  'reviews___parent___internal___content' |
  'reviews___parent___internal___contentDigest' |
  'reviews___parent___internal___description' |
  'reviews___parent___internal___fieldOwners' |
  'reviews___parent___internal___ignoreType' |
  'reviews___parent___internal___mediaType' |
  'reviews___parent___internal___owner' |
  'reviews___parent___internal___type' |
  'reviews___children' |
  'reviews___children___id' |
  'reviews___children___parent___id' |
  'reviews___children___parent___children' |
  'reviews___children___children' |
  'reviews___children___children___id' |
  'reviews___children___children___children' |
  'reviews___children___internal___content' |
  'reviews___children___internal___contentDigest' |
  'reviews___children___internal___description' |
  'reviews___children___internal___fieldOwners' |
  'reviews___children___internal___ignoreType' |
  'reviews___children___internal___mediaType' |
  'reviews___children___internal___owner' |
  'reviews___children___internal___type' |
  'reviews___internal___content' |
  'reviews___internal___contentDigest' |
  'reviews___internal___description' |
  'reviews___internal___fieldOwners' |
  'reviews___internal___ignoreType' |
  'reviews___internal___mediaType' |
  'reviews___internal___owner' |
  'reviews___internal___type' |
  'reviews___rating' |
  'reviews___votes' |
  'reviews___spoiler_flag' |
  'reviews___spoilers_state' |
  'reviews___recommended_for' |
  'reviews___recommended_by' |
  'reviews___started_at' |
  'reviews___read_at' |
  'reviews___date_added' |
  'reviews___date_updated' |
  'reviews___read_count' |
  'reviews___body' |
  'reviews___comments_count' |
  'reviews___url' |
  'reviews___link' |
  'reviews___owned' |
  'reviews___goodreadsId' |
  'reviews___book___id' |
  'reviews___book___parent___id' |
  'reviews___book___parent___children' |
  'reviews___book___children' |
  'reviews___book___children___id' |
  'reviews___book___children___children' |
  'reviews___book___internal___content' |
  'reviews___book___internal___contentDigest' |
  'reviews___book___internal___description' |
  'reviews___book___internal___fieldOwners' |
  'reviews___book___internal___ignoreType' |
  'reviews___book___internal___mediaType' |
  'reviews___book___internal___owner' |
  'reviews___book___internal___type' |
  'reviews___book___isbn' |
  'reviews___book___isbn13' |
  'reviews___book___text_reviews_count' |
  'reviews___book___uri' |
  'reviews___book___title' |
  'reviews___book___title_without_series' |
  'reviews___book___image_url' |
  'reviews___book___small_image_url' |
  'reviews___book___large_image_url' |
  'reviews___book___link' |
  'reviews___book___num_pages' |
  'reviews___book___format' |
  'reviews___book___edition_information' |
  'reviews___book___publisher' |
  'reviews___book___publication_day' |
  'reviews___book___publication_year' |
  'reviews___book___publication_month' |
  'reviews___book___average_rating' |
  'reviews___book___ratings_count' |
  'reviews___book___description' |
  'reviews___book___published' |
  'reviews___book___work___id' |
  'reviews___book___work___uri' |
  'reviews___book___goodreadsId' |
  'reviews___book___reviews' |
  'reviews___book___reviews___id' |
  'reviews___book___reviews___children' |
  'reviews___book___reviews___rating' |
  'reviews___book___reviews___votes' |
  'reviews___book___reviews___spoiler_flag' |
  'reviews___book___reviews___spoilers_state' |
  'reviews___book___reviews___recommended_for' |
  'reviews___book___reviews___recommended_by' |
  'reviews___book___reviews___started_at' |
  'reviews___book___reviews___read_at' |
  'reviews___book___reviews___date_added' |
  'reviews___book___reviews___date_updated' |
  'reviews___book___reviews___read_count' |
  'reviews___book___reviews___body' |
  'reviews___book___reviews___comments_count' |
  'reviews___book___reviews___url' |
  'reviews___book___reviews___link' |
  'reviews___book___reviews___owned' |
  'reviews___book___reviews___goodreadsId' |
  'reviews___book___authors' |
  'reviews___book___authors___id' |
  'reviews___book___authors___children' |
  'reviews___book___authors___name' |
  'reviews___book___authors___role' |
  'reviews___book___authors___image_url' |
  'reviews___book___authors___small_image_url' |
  'reviews___book___authors___link' |
  'reviews___book___authors___average_rating' |
  'reviews___book___authors___ratings_count' |
  'reviews___book___authors___text_reviews_count' |
  'reviews___book___authors___goodreadsId' |
  'reviews___book___authors___books' |
  'authors' |
  'authors___id' |
  'authors___parent___id' |
  'authors___parent___parent___id' |
  'authors___parent___parent___children' |
  'authors___parent___children' |
  'authors___parent___children___id' |
  'authors___parent___children___children' |
  'authors___parent___internal___content' |
  'authors___parent___internal___contentDigest' |
  'authors___parent___internal___description' |
  'authors___parent___internal___fieldOwners' |
  'authors___parent___internal___ignoreType' |
  'authors___parent___internal___mediaType' |
  'authors___parent___internal___owner' |
  'authors___parent___internal___type' |
  'authors___children' |
  'authors___children___id' |
  'authors___children___parent___id' |
  'authors___children___parent___children' |
  'authors___children___children' |
  'authors___children___children___id' |
  'authors___children___children___children' |
  'authors___children___internal___content' |
  'authors___children___internal___contentDigest' |
  'authors___children___internal___description' |
  'authors___children___internal___fieldOwners' |
  'authors___children___internal___ignoreType' |
  'authors___children___internal___mediaType' |
  'authors___children___internal___owner' |
  'authors___children___internal___type' |
  'authors___internal___content' |
  'authors___internal___contentDigest' |
  'authors___internal___description' |
  'authors___internal___fieldOwners' |
  'authors___internal___ignoreType' |
  'authors___internal___mediaType' |
  'authors___internal___owner' |
  'authors___internal___type' |
  'authors___name' |
  'authors___role' |
  'authors___image_url' |
  'authors___small_image_url' |
  'authors___link' |
  'authors___average_rating' |
  'authors___ratings_count' |
  'authors___text_reviews_count' |
  'authors___goodreadsId' |
  'authors___books' |
  'authors___books___id' |
  'authors___books___parent___id' |
  'authors___books___parent___children' |
  'authors___books___children' |
  'authors___books___children___id' |
  'authors___books___children___children' |
  'authors___books___internal___content' |
  'authors___books___internal___contentDigest' |
  'authors___books___internal___description' |
  'authors___books___internal___fieldOwners' |
  'authors___books___internal___ignoreType' |
  'authors___books___internal___mediaType' |
  'authors___books___internal___owner' |
  'authors___books___internal___type' |
  'authors___books___isbn' |
  'authors___books___isbn13' |
  'authors___books___text_reviews_count' |
  'authors___books___uri' |
  'authors___books___title' |
  'authors___books___title_without_series' |
  'authors___books___image_url' |
  'authors___books___small_image_url' |
  'authors___books___large_image_url' |
  'authors___books___link' |
  'authors___books___num_pages' |
  'authors___books___format' |
  'authors___books___edition_information' |
  'authors___books___publisher' |
  'authors___books___publication_day' |
  'authors___books___publication_year' |
  'authors___books___publication_month' |
  'authors___books___average_rating' |
  'authors___books___ratings_count' |
  'authors___books___description' |
  'authors___books___published' |
  'authors___books___work___id' |
  'authors___books___work___uri' |
  'authors___books___goodreadsId' |
  'authors___books___reviews' |
  'authors___books___reviews___id' |
  'authors___books___reviews___children' |
  'authors___books___reviews___rating' |
  'authors___books___reviews___votes' |
  'authors___books___reviews___spoiler_flag' |
  'authors___books___reviews___spoilers_state' |
  'authors___books___reviews___recommended_for' |
  'authors___books___reviews___recommended_by' |
  'authors___books___reviews___started_at' |
  'authors___books___reviews___read_at' |
  'authors___books___reviews___date_added' |
  'authors___books___reviews___date_updated' |
  'authors___books___reviews___read_count' |
  'authors___books___reviews___body' |
  'authors___books___reviews___comments_count' |
  'authors___books___reviews___url' |
  'authors___books___reviews___link' |
  'authors___books___reviews___owned' |
  'authors___books___reviews___goodreadsId' |
  'authors___books___authors' |
  'authors___books___authors___id' |
  'authors___books___authors___children' |
  'authors___books___authors___name' |
  'authors___books___authors___role' |
  'authors___books___authors___image_url' |
  'authors___books___authors___small_image_url' |
  'authors___books___authors___link' |
  'authors___books___authors___average_rating' |
  'authors___books___authors___ratings_count' |
  'authors___books___authors___text_reviews_count' |
  'authors___books___authors___goodreadsId' |
  'authors___books___authors___books';

export type GoodreadsBookFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  isbn?: Maybe<StringQueryOperatorInput>,
  isbn13?: Maybe<StringQueryOperatorInput>,
  text_reviews_count?: Maybe<StringQueryOperatorInput>,
  uri?: Maybe<StringQueryOperatorInput>,
  title?: Maybe<StringQueryOperatorInput>,
  title_without_series?: Maybe<StringQueryOperatorInput>,
  image_url?: Maybe<StringQueryOperatorInput>,
  small_image_url?: Maybe<StringQueryOperatorInput>,
  large_image_url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  num_pages?: Maybe<StringQueryOperatorInput>,
  format?: Maybe<StringQueryOperatorInput>,
  edition_information?: Maybe<StringQueryOperatorInput>,
  publisher?: Maybe<StringQueryOperatorInput>,
  publication_day?: Maybe<StringQueryOperatorInput>,
  publication_year?: Maybe<DateQueryOperatorInput>,
  publication_month?: Maybe<StringQueryOperatorInput>,
  average_rating?: Maybe<StringQueryOperatorInput>,
  ratings_count?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  published?: Maybe<DateQueryOperatorInput>,
  work?: Maybe<GoodreadsBookWorkFilterInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  reviews?: Maybe<GoodreadsReviewFilterListInput>,
  authors?: Maybe<GoodreadsAuthorFilterListInput>,
};

export type GoodreadsBookFilterListInput = {
  elemMatch?: Maybe<GoodreadsBookFilterInput>,
};

export type GoodreadsBookGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsBookEdge>,
  nodes: Array<GoodreadsBook>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type GoodreadsBookSortInput = {
  fields?: Maybe<Array<Maybe<GoodreadsBookFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type GoodreadsBookWork = {
  id?: Maybe<Scalars['String']>,
  uri?: Maybe<Scalars['String']>,
};

export type GoodreadsBookWorkFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  uri?: Maybe<StringQueryOperatorInput>,
};

export type GoodreadsReview = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  rating?: Maybe<Scalars['String']>,
  votes?: Maybe<Scalars['String']>,
  spoiler_flag?: Maybe<Scalars['String']>,
  spoilers_state?: Maybe<Scalars['String']>,
  recommended_for?: Maybe<Scalars['String']>,
  recommended_by?: Maybe<Scalars['String']>,
  started_at?: Maybe<Scalars['String']>,
  read_at?: Maybe<Scalars['String']>,
  date_added?: Maybe<Scalars['String']>,
  date_updated?: Maybe<Scalars['String']>,
  read_count?: Maybe<Scalars['String']>,
  body?: Maybe<Scalars['String']>,
  comments_count?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  link?: Maybe<Scalars['String']>,
  owned?: Maybe<Scalars['String']>,
  goodreadsId?: Maybe<Scalars['String']>,
  book?: Maybe<GoodreadsBook>,
};

export type GoodreadsReviewConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsReviewEdge>,
  nodes: Array<GoodreadsReview>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<GoodreadsReviewGroupConnection>,
};


export type GoodreadsReviewConnectionDistinctArgs = {
  field: GoodreadsReviewFieldsEnum
};


export type GoodreadsReviewConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: GoodreadsReviewFieldsEnum
};

export type GoodreadsReviewEdge = {
  next?: Maybe<GoodreadsReview>,
  node: GoodreadsReview,
  previous?: Maybe<GoodreadsReview>,
};

export type GoodreadsReviewFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'rating' |
  'votes' |
  'spoiler_flag' |
  'spoilers_state' |
  'recommended_for' |
  'recommended_by' |
  'started_at' |
  'read_at' |
  'date_added' |
  'date_updated' |
  'read_count' |
  'body' |
  'comments_count' |
  'url' |
  'link' |
  'owned' |
  'goodreadsId' |
  'book___id' |
  'book___parent___id' |
  'book___parent___parent___id' |
  'book___parent___parent___children' |
  'book___parent___children' |
  'book___parent___children___id' |
  'book___parent___children___children' |
  'book___parent___internal___content' |
  'book___parent___internal___contentDigest' |
  'book___parent___internal___description' |
  'book___parent___internal___fieldOwners' |
  'book___parent___internal___ignoreType' |
  'book___parent___internal___mediaType' |
  'book___parent___internal___owner' |
  'book___parent___internal___type' |
  'book___children' |
  'book___children___id' |
  'book___children___parent___id' |
  'book___children___parent___children' |
  'book___children___children' |
  'book___children___children___id' |
  'book___children___children___children' |
  'book___children___internal___content' |
  'book___children___internal___contentDigest' |
  'book___children___internal___description' |
  'book___children___internal___fieldOwners' |
  'book___children___internal___ignoreType' |
  'book___children___internal___mediaType' |
  'book___children___internal___owner' |
  'book___children___internal___type' |
  'book___internal___content' |
  'book___internal___contentDigest' |
  'book___internal___description' |
  'book___internal___fieldOwners' |
  'book___internal___ignoreType' |
  'book___internal___mediaType' |
  'book___internal___owner' |
  'book___internal___type' |
  'book___isbn' |
  'book___isbn13' |
  'book___text_reviews_count' |
  'book___uri' |
  'book___title' |
  'book___title_without_series' |
  'book___image_url' |
  'book___small_image_url' |
  'book___large_image_url' |
  'book___link' |
  'book___num_pages' |
  'book___format' |
  'book___edition_information' |
  'book___publisher' |
  'book___publication_day' |
  'book___publication_year' |
  'book___publication_month' |
  'book___average_rating' |
  'book___ratings_count' |
  'book___description' |
  'book___published' |
  'book___work___id' |
  'book___work___uri' |
  'book___goodreadsId' |
  'book___reviews' |
  'book___reviews___id' |
  'book___reviews___parent___id' |
  'book___reviews___parent___children' |
  'book___reviews___children' |
  'book___reviews___children___id' |
  'book___reviews___children___children' |
  'book___reviews___internal___content' |
  'book___reviews___internal___contentDigest' |
  'book___reviews___internal___description' |
  'book___reviews___internal___fieldOwners' |
  'book___reviews___internal___ignoreType' |
  'book___reviews___internal___mediaType' |
  'book___reviews___internal___owner' |
  'book___reviews___internal___type' |
  'book___reviews___rating' |
  'book___reviews___votes' |
  'book___reviews___spoiler_flag' |
  'book___reviews___spoilers_state' |
  'book___reviews___recommended_for' |
  'book___reviews___recommended_by' |
  'book___reviews___started_at' |
  'book___reviews___read_at' |
  'book___reviews___date_added' |
  'book___reviews___date_updated' |
  'book___reviews___read_count' |
  'book___reviews___body' |
  'book___reviews___comments_count' |
  'book___reviews___url' |
  'book___reviews___link' |
  'book___reviews___owned' |
  'book___reviews___goodreadsId' |
  'book___reviews___book___id' |
  'book___reviews___book___children' |
  'book___reviews___book___isbn' |
  'book___reviews___book___isbn13' |
  'book___reviews___book___text_reviews_count' |
  'book___reviews___book___uri' |
  'book___reviews___book___title' |
  'book___reviews___book___title_without_series' |
  'book___reviews___book___image_url' |
  'book___reviews___book___small_image_url' |
  'book___reviews___book___large_image_url' |
  'book___reviews___book___link' |
  'book___reviews___book___num_pages' |
  'book___reviews___book___format' |
  'book___reviews___book___edition_information' |
  'book___reviews___book___publisher' |
  'book___reviews___book___publication_day' |
  'book___reviews___book___publication_year' |
  'book___reviews___book___publication_month' |
  'book___reviews___book___average_rating' |
  'book___reviews___book___ratings_count' |
  'book___reviews___book___description' |
  'book___reviews___book___published' |
  'book___reviews___book___goodreadsId' |
  'book___reviews___book___reviews' |
  'book___reviews___book___authors' |
  'book___authors' |
  'book___authors___id' |
  'book___authors___parent___id' |
  'book___authors___parent___children' |
  'book___authors___children' |
  'book___authors___children___id' |
  'book___authors___children___children' |
  'book___authors___internal___content' |
  'book___authors___internal___contentDigest' |
  'book___authors___internal___description' |
  'book___authors___internal___fieldOwners' |
  'book___authors___internal___ignoreType' |
  'book___authors___internal___mediaType' |
  'book___authors___internal___owner' |
  'book___authors___internal___type' |
  'book___authors___name' |
  'book___authors___role' |
  'book___authors___image_url' |
  'book___authors___small_image_url' |
  'book___authors___link' |
  'book___authors___average_rating' |
  'book___authors___ratings_count' |
  'book___authors___text_reviews_count' |
  'book___authors___goodreadsId' |
  'book___authors___books' |
  'book___authors___books___id' |
  'book___authors___books___children' |
  'book___authors___books___isbn' |
  'book___authors___books___isbn13' |
  'book___authors___books___text_reviews_count' |
  'book___authors___books___uri' |
  'book___authors___books___title' |
  'book___authors___books___title_without_series' |
  'book___authors___books___image_url' |
  'book___authors___books___small_image_url' |
  'book___authors___books___large_image_url' |
  'book___authors___books___link' |
  'book___authors___books___num_pages' |
  'book___authors___books___format' |
  'book___authors___books___edition_information' |
  'book___authors___books___publisher' |
  'book___authors___books___publication_day' |
  'book___authors___books___publication_year' |
  'book___authors___books___publication_month' |
  'book___authors___books___average_rating' |
  'book___authors___books___ratings_count' |
  'book___authors___books___description' |
  'book___authors___books___published' |
  'book___authors___books___goodreadsId' |
  'book___authors___books___reviews' |
  'book___authors___books___authors';

export type GoodreadsReviewFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  rating?: Maybe<StringQueryOperatorInput>,
  votes?: Maybe<StringQueryOperatorInput>,
  spoiler_flag?: Maybe<StringQueryOperatorInput>,
  spoilers_state?: Maybe<StringQueryOperatorInput>,
  recommended_for?: Maybe<StringQueryOperatorInput>,
  recommended_by?: Maybe<StringQueryOperatorInput>,
  started_at?: Maybe<StringQueryOperatorInput>,
  read_at?: Maybe<StringQueryOperatorInput>,
  date_added?: Maybe<StringQueryOperatorInput>,
  date_updated?: Maybe<StringQueryOperatorInput>,
  read_count?: Maybe<StringQueryOperatorInput>,
  body?: Maybe<StringQueryOperatorInput>,
  comments_count?: Maybe<StringQueryOperatorInput>,
  url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  owned?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  book?: Maybe<GoodreadsBookFilterInput>,
};

export type GoodreadsReviewFilterListInput = {
  elemMatch?: Maybe<GoodreadsReviewFilterInput>,
};

export type GoodreadsReviewGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsReviewEdge>,
  nodes: Array<GoodreadsReview>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type GoodreadsReviewSortInput = {
  fields?: Maybe<Array<Maybe<GoodreadsReviewFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type GoodreadsShelf = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  name?: Maybe<Scalars['String']>,
  exclusive?: Maybe<Scalars['String']>,
  review_shelf_id?: Maybe<Scalars['String']>,
  goodreadsId?: Maybe<Scalars['String']>,
  reviews?: Maybe<Array<Maybe<GoodreadsReview>>>,
  sortable?: Maybe<Scalars['String']>,
};

export type GoodreadsShelfConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsShelfEdge>,
  nodes: Array<GoodreadsShelf>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<GoodreadsShelfGroupConnection>,
};


export type GoodreadsShelfConnectionDistinctArgs = {
  field: GoodreadsShelfFieldsEnum
};


export type GoodreadsShelfConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: GoodreadsShelfFieldsEnum
};

export type GoodreadsShelfEdge = {
  next?: Maybe<GoodreadsShelf>,
  node: GoodreadsShelf,
  previous?: Maybe<GoodreadsShelf>,
};

export type GoodreadsShelfFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'name' |
  'exclusive' |
  'review_shelf_id' |
  'goodreadsId' |
  'reviews' |
  'reviews___id' |
  'reviews___parent___id' |
  'reviews___parent___parent___id' |
  'reviews___parent___parent___children' |
  'reviews___parent___children' |
  'reviews___parent___children___id' |
  'reviews___parent___children___children' |
  'reviews___parent___internal___content' |
  'reviews___parent___internal___contentDigest' |
  'reviews___parent___internal___description' |
  'reviews___parent___internal___fieldOwners' |
  'reviews___parent___internal___ignoreType' |
  'reviews___parent___internal___mediaType' |
  'reviews___parent___internal___owner' |
  'reviews___parent___internal___type' |
  'reviews___children' |
  'reviews___children___id' |
  'reviews___children___parent___id' |
  'reviews___children___parent___children' |
  'reviews___children___children' |
  'reviews___children___children___id' |
  'reviews___children___children___children' |
  'reviews___children___internal___content' |
  'reviews___children___internal___contentDigest' |
  'reviews___children___internal___description' |
  'reviews___children___internal___fieldOwners' |
  'reviews___children___internal___ignoreType' |
  'reviews___children___internal___mediaType' |
  'reviews___children___internal___owner' |
  'reviews___children___internal___type' |
  'reviews___internal___content' |
  'reviews___internal___contentDigest' |
  'reviews___internal___description' |
  'reviews___internal___fieldOwners' |
  'reviews___internal___ignoreType' |
  'reviews___internal___mediaType' |
  'reviews___internal___owner' |
  'reviews___internal___type' |
  'reviews___rating' |
  'reviews___votes' |
  'reviews___spoiler_flag' |
  'reviews___spoilers_state' |
  'reviews___recommended_for' |
  'reviews___recommended_by' |
  'reviews___started_at' |
  'reviews___read_at' |
  'reviews___date_added' |
  'reviews___date_updated' |
  'reviews___read_count' |
  'reviews___body' |
  'reviews___comments_count' |
  'reviews___url' |
  'reviews___link' |
  'reviews___owned' |
  'reviews___goodreadsId' |
  'reviews___book___id' |
  'reviews___book___parent___id' |
  'reviews___book___parent___children' |
  'reviews___book___children' |
  'reviews___book___children___id' |
  'reviews___book___children___children' |
  'reviews___book___internal___content' |
  'reviews___book___internal___contentDigest' |
  'reviews___book___internal___description' |
  'reviews___book___internal___fieldOwners' |
  'reviews___book___internal___ignoreType' |
  'reviews___book___internal___mediaType' |
  'reviews___book___internal___owner' |
  'reviews___book___internal___type' |
  'reviews___book___isbn' |
  'reviews___book___isbn13' |
  'reviews___book___text_reviews_count' |
  'reviews___book___uri' |
  'reviews___book___title' |
  'reviews___book___title_without_series' |
  'reviews___book___image_url' |
  'reviews___book___small_image_url' |
  'reviews___book___large_image_url' |
  'reviews___book___link' |
  'reviews___book___num_pages' |
  'reviews___book___format' |
  'reviews___book___edition_information' |
  'reviews___book___publisher' |
  'reviews___book___publication_day' |
  'reviews___book___publication_year' |
  'reviews___book___publication_month' |
  'reviews___book___average_rating' |
  'reviews___book___ratings_count' |
  'reviews___book___description' |
  'reviews___book___published' |
  'reviews___book___work___id' |
  'reviews___book___work___uri' |
  'reviews___book___goodreadsId' |
  'reviews___book___reviews' |
  'reviews___book___reviews___id' |
  'reviews___book___reviews___children' |
  'reviews___book___reviews___rating' |
  'reviews___book___reviews___votes' |
  'reviews___book___reviews___spoiler_flag' |
  'reviews___book___reviews___spoilers_state' |
  'reviews___book___reviews___recommended_for' |
  'reviews___book___reviews___recommended_by' |
  'reviews___book___reviews___started_at' |
  'reviews___book___reviews___read_at' |
  'reviews___book___reviews___date_added' |
  'reviews___book___reviews___date_updated' |
  'reviews___book___reviews___read_count' |
  'reviews___book___reviews___body' |
  'reviews___book___reviews___comments_count' |
  'reviews___book___reviews___url' |
  'reviews___book___reviews___link' |
  'reviews___book___reviews___owned' |
  'reviews___book___reviews___goodreadsId' |
  'reviews___book___authors' |
  'reviews___book___authors___id' |
  'reviews___book___authors___children' |
  'reviews___book___authors___name' |
  'reviews___book___authors___role' |
  'reviews___book___authors___image_url' |
  'reviews___book___authors___small_image_url' |
  'reviews___book___authors___link' |
  'reviews___book___authors___average_rating' |
  'reviews___book___authors___ratings_count' |
  'reviews___book___authors___text_reviews_count' |
  'reviews___book___authors___goodreadsId' |
  'reviews___book___authors___books' |
  'sortable';

export type GoodreadsShelfFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  exclusive?: Maybe<StringQueryOperatorInput>,
  review_shelf_id?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  reviews?: Maybe<GoodreadsReviewFilterListInput>,
  sortable?: Maybe<StringQueryOperatorInput>,
};

export type GoodreadsShelfGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<GoodreadsShelfEdge>,
  nodes: Array<GoodreadsShelf>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type GoodreadsShelfSortInput = {
  fields?: Maybe<Array<Maybe<GoodreadsShelfFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type ImageCropFocus = 
  'CENTER' |
  'NORTH' |
  'NORTHEAST' |
  'EAST' |
  'SOUTHEAST' |
  'SOUTH' |
  'SOUTHWEST' |
  'WEST' |
  'NORTHWEST' |
  'ENTROPY' |
  'ATTENTION';

export type ImageFit = 
  'COVER' |
  'CONTAIN' |
  'FILL';

export type ImageFormat = 
  'NO_CHANGE' |
  'JPG' |
  'PNG' |
  'WEBP';

export type ImageSharp = Node & {
  fixed?: Maybe<ImageSharpFixed>,
  resolutions?: Maybe<ImageSharpResolutions>,
  fluid?: Maybe<ImageSharpFluid>,
  sizes?: Maybe<ImageSharpSizes>,
  original?: Maybe<ImageSharpOriginal>,
  resize?: Maybe<ImageSharpResize>,
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
};


export type ImageSharpFixedArgs = {
  width?: Maybe<Scalars['Int']>,
  height?: Maybe<Scalars['Int']>,
  base64Width?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  grayscale?: Maybe<Scalars['Boolean']>,
  duotone?: Maybe<DuotoneGradient>,
  traceSVG?: Maybe<Potrace>,
  quality?: Maybe<Scalars['Int']>,
  jpegQuality?: Maybe<Scalars['Int']>,
  pngQuality?: Maybe<Scalars['Int']>,
  webpQuality?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  cropFocus?: Maybe<ImageCropFocus>,
  fit?: Maybe<ImageFit>,
  background?: Maybe<Scalars['String']>,
  rotate?: Maybe<Scalars['Int']>,
  trim?: Maybe<Scalars['Float']>
};


export type ImageSharpResolutionsArgs = {
  width?: Maybe<Scalars['Int']>,
  height?: Maybe<Scalars['Int']>,
  base64Width?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  grayscale?: Maybe<Scalars['Boolean']>,
  duotone?: Maybe<DuotoneGradient>,
  traceSVG?: Maybe<Potrace>,
  quality?: Maybe<Scalars['Int']>,
  jpegQuality?: Maybe<Scalars['Int']>,
  pngQuality?: Maybe<Scalars['Int']>,
  webpQuality?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  cropFocus?: Maybe<ImageCropFocus>,
  fit?: Maybe<ImageFit>,
  background?: Maybe<Scalars['String']>,
  rotate?: Maybe<Scalars['Int']>,
  trim?: Maybe<Scalars['Float']>
};


export type ImageSharpFluidArgs = {
  maxWidth?: Maybe<Scalars['Int']>,
  maxHeight?: Maybe<Scalars['Int']>,
  base64Width?: Maybe<Scalars['Int']>,
  grayscale?: Maybe<Scalars['Boolean']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  duotone?: Maybe<DuotoneGradient>,
  traceSVG?: Maybe<Potrace>,
  quality?: Maybe<Scalars['Int']>,
  jpegQuality?: Maybe<Scalars['Int']>,
  pngQuality?: Maybe<Scalars['Int']>,
  webpQuality?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  cropFocus?: Maybe<ImageCropFocus>,
  fit?: Maybe<ImageFit>,
  background?: Maybe<Scalars['String']>,
  rotate?: Maybe<Scalars['Int']>,
  trim?: Maybe<Scalars['Float']>,
  sizes?: Maybe<Scalars['String']>,
  srcSetBreakpoints?: Maybe<Array<Maybe<Scalars['Int']>>>
};


export type ImageSharpSizesArgs = {
  maxWidth?: Maybe<Scalars['Int']>,
  maxHeight?: Maybe<Scalars['Int']>,
  base64Width?: Maybe<Scalars['Int']>,
  grayscale?: Maybe<Scalars['Boolean']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  duotone?: Maybe<DuotoneGradient>,
  traceSVG?: Maybe<Potrace>,
  quality?: Maybe<Scalars['Int']>,
  jpegQuality?: Maybe<Scalars['Int']>,
  pngQuality?: Maybe<Scalars['Int']>,
  webpQuality?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  cropFocus?: Maybe<ImageCropFocus>,
  fit?: Maybe<ImageFit>,
  background?: Maybe<Scalars['String']>,
  rotate?: Maybe<Scalars['Int']>,
  trim?: Maybe<Scalars['Float']>,
  sizes?: Maybe<Scalars['String']>,
  srcSetBreakpoints?: Maybe<Array<Maybe<Scalars['Int']>>>
};


export type ImageSharpResizeArgs = {
  width?: Maybe<Scalars['Int']>,
  height?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  jpegQuality?: Maybe<Scalars['Int']>,
  pngQuality?: Maybe<Scalars['Int']>,
  webpQuality?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionLevel?: Maybe<Scalars['Int']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  grayscale?: Maybe<Scalars['Boolean']>,
  duotone?: Maybe<DuotoneGradient>,
  base64?: Maybe<Scalars['Boolean']>,
  traceSVG?: Maybe<Potrace>,
  toFormat?: Maybe<ImageFormat>,
  cropFocus?: Maybe<ImageCropFocus>,
  fit?: Maybe<ImageFit>,
  background?: Maybe<Scalars['String']>,
  rotate?: Maybe<Scalars['Int']>,
  trim?: Maybe<Scalars['Float']>
};

export type ImageSharpConnection = {
  totalCount: Scalars['Int'],
  edges: Array<ImageSharpEdge>,
  nodes: Array<ImageSharp>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<ImageSharpGroupConnection>,
};


export type ImageSharpConnectionDistinctArgs = {
  field: ImageSharpFieldsEnum
};


export type ImageSharpConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: ImageSharpFieldsEnum
};

export type ImageSharpEdge = {
  next?: Maybe<ImageSharp>,
  node: ImageSharp,
  previous?: Maybe<ImageSharp>,
};

export type ImageSharpFieldsEnum = 
  'fixed___base64' |
  'fixed___tracedSVG' |
  'fixed___aspectRatio' |
  'fixed___width' |
  'fixed___height' |
  'fixed___src' |
  'fixed___srcSet' |
  'fixed___srcWebp' |
  'fixed___srcSetWebp' |
  'fixed___originalName' |
  'resolutions___base64' |
  'resolutions___tracedSVG' |
  'resolutions___aspectRatio' |
  'resolutions___width' |
  'resolutions___height' |
  'resolutions___src' |
  'resolutions___srcSet' |
  'resolutions___srcWebp' |
  'resolutions___srcSetWebp' |
  'resolutions___originalName' |
  'fluid___base64' |
  'fluid___tracedSVG' |
  'fluid___aspectRatio' |
  'fluid___src' |
  'fluid___srcSet' |
  'fluid___srcWebp' |
  'fluid___srcSetWebp' |
  'fluid___sizes' |
  'fluid___originalImg' |
  'fluid___originalName' |
  'fluid___presentationWidth' |
  'fluid___presentationHeight' |
  'sizes___base64' |
  'sizes___tracedSVG' |
  'sizes___aspectRatio' |
  'sizes___src' |
  'sizes___srcSet' |
  'sizes___srcWebp' |
  'sizes___srcSetWebp' |
  'sizes___sizes' |
  'sizes___originalImg' |
  'sizes___originalName' |
  'sizes___presentationWidth' |
  'sizes___presentationHeight' |
  'original___width' |
  'original___height' |
  'original___src' |
  'resize___src' |
  'resize___tracedSVG' |
  'resize___width' |
  'resize___height' |
  'resize___aspectRatio' |
  'resize___originalName' |
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type';

export type ImageSharpFilterInput = {
  fixed?: Maybe<ImageSharpFixedFilterInput>,
  resolutions?: Maybe<ImageSharpResolutionsFilterInput>,
  fluid?: Maybe<ImageSharpFluidFilterInput>,
  sizes?: Maybe<ImageSharpSizesFilterInput>,
  original?: Maybe<ImageSharpOriginalFilterInput>,
  resize?: Maybe<ImageSharpResizeFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
};

export type ImageSharpFixed = {
  base64?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  aspectRatio?: Maybe<Scalars['Float']>,
  width: Scalars['Float'],
  height: Scalars['Float'],
  src: Scalars['String'],
  srcSet: Scalars['String'],
  srcWebp?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
};

export type ImageSharpFixedFilterInput = {
  base64?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  width?: Maybe<FloatQueryOperatorInput>,
  height?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpFluid = {
  base64?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  aspectRatio: Scalars['Float'],
  src: Scalars['String'],
  srcSet: Scalars['String'],
  srcWebp?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  sizes: Scalars['String'],
  originalImg?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
  presentationWidth?: Maybe<Scalars['Int']>,
  presentationHeight?: Maybe<Scalars['Int']>,
};

export type ImageSharpFluidFilterInput = {
  base64?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  sizes?: Maybe<StringQueryOperatorInput>,
  originalImg?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  presentationWidth?: Maybe<IntQueryOperatorInput>,
  presentationHeight?: Maybe<IntQueryOperatorInput>,
};

export type ImageSharpGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<ImageSharpEdge>,
  nodes: Array<ImageSharp>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type ImageSharpOriginal = {
  width?: Maybe<Scalars['Float']>,
  height?: Maybe<Scalars['Float']>,
  src?: Maybe<Scalars['String']>,
};

export type ImageSharpOriginalFilterInput = {
  width?: Maybe<FloatQueryOperatorInput>,
  height?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpResize = {
  src?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  width?: Maybe<Scalars['Int']>,
  height?: Maybe<Scalars['Int']>,
  aspectRatio?: Maybe<Scalars['Float']>,
  originalName?: Maybe<Scalars['String']>,
};

export type ImageSharpResizeFilterInput = {
  src?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  width?: Maybe<IntQueryOperatorInput>,
  height?: Maybe<IntQueryOperatorInput>,
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpResolutions = {
  base64?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  aspectRatio?: Maybe<Scalars['Float']>,
  width: Scalars['Float'],
  height: Scalars['Float'],
  src: Scalars['String'],
  srcSet: Scalars['String'],
  srcWebp?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
};

export type ImageSharpResolutionsFilterInput = {
  base64?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  width?: Maybe<FloatQueryOperatorInput>,
  height?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpSizes = {
  base64?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  aspectRatio: Scalars['Float'],
  src: Scalars['String'],
  srcSet: Scalars['String'],
  srcWebp?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  sizes: Scalars['String'],
  originalImg?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
  presentationWidth?: Maybe<Scalars['Int']>,
  presentationHeight?: Maybe<Scalars['Int']>,
};

export type ImageSharpSizesFilterInput = {
  base64?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  sizes?: Maybe<StringQueryOperatorInput>,
  originalImg?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  presentationWidth?: Maybe<IntQueryOperatorInput>,
  presentationHeight?: Maybe<IntQueryOperatorInput>,
};

export type ImageSharpSortInput = {
  fields?: Maybe<Array<Maybe<ImageSharpFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type Internal = {
  content?: Maybe<Scalars['String']>,
  contentDigest: Scalars['String'],
  description?: Maybe<Scalars['String']>,
  fieldOwners?: Maybe<Array<Maybe<Scalars['String']>>>,
  ignoreType?: Maybe<Scalars['Boolean']>,
  mediaType?: Maybe<Scalars['String']>,
  owner: Scalars['String'],
  type: Scalars['String'],
};

export type InternalFilterInput = {
  content?: Maybe<StringQueryOperatorInput>,
  contentDigest?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  fieldOwners?: Maybe<StringQueryOperatorInput>,
  ignoreType?: Maybe<BooleanQueryOperatorInput>,
  mediaType?: Maybe<StringQueryOperatorInput>,
  owner?: Maybe<StringQueryOperatorInput>,
  type?: Maybe<StringQueryOperatorInput>,
};

export type IntQueryOperatorInput = {
  eq?: Maybe<Scalars['Int']>,
  ne?: Maybe<Scalars['Int']>,
  gt?: Maybe<Scalars['Int']>,
  gte?: Maybe<Scalars['Int']>,
  lt?: Maybe<Scalars['Int']>,
  lte?: Maybe<Scalars['Int']>,
  in?: Maybe<Array<Maybe<Scalars['Int']>>>,
  nin?: Maybe<Array<Maybe<Scalars['Int']>>>,
};


export type JsonQueryOperatorInput = {
  eq?: Maybe<Scalars['JSON']>,
  ne?: Maybe<Scalars['JSON']>,
  in?: Maybe<Array<Maybe<Scalars['JSON']>>>,
  nin?: Maybe<Array<Maybe<Scalars['JSON']>>>,
  regex?: Maybe<Scalars['JSON']>,
  glob?: Maybe<Scalars['JSON']>,
};

export type MarkdownExcerptFormats = 
  'PLAIN' |
  'HTML' |
  'MARKDOWN';

export type MarkdownHeading = {
  value?: Maybe<Scalars['String']>,
  depth?: Maybe<Scalars['Int']>,
};

export type MarkdownHeadingFilterInput = {
  value?: Maybe<StringQueryOperatorInput>,
  depth?: Maybe<IntQueryOperatorInput>,
};

export type MarkdownHeadingFilterListInput = {
  elemMatch?: Maybe<MarkdownHeadingFilterInput>,
};

export type MarkdownHeadingLevels = 
  'h1' |
  'h2' |
  'h3' |
  'h4' |
  'h5' |
  'h6';

export type MarkdownRemark = Node & {
  id: Scalars['ID'],
  frontmatter?: Maybe<MarkdownRemarkFrontmatter>,
  excerpt?: Maybe<Scalars['String']>,
  rawMarkdownBody?: Maybe<Scalars['String']>,
  fileAbsolutePath?: Maybe<Scalars['String']>,
  fields?: Maybe<MarkdownRemarkFields>,
  html?: Maybe<Scalars['String']>,
  htmlAst?: Maybe<Scalars['JSON']>,
  excerptAst?: Maybe<Scalars['JSON']>,
  headings?: Maybe<Array<Maybe<MarkdownHeading>>>,
  timeToRead?: Maybe<Scalars['Int']>,
  tableOfContents?: Maybe<Scalars['String']>,
  wordCount?: Maybe<MarkdownWordCount>,
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
};


export type MarkdownRemarkExcerptArgs = {
  pruneLength?: Maybe<Scalars['Int']>,
  truncate?: Maybe<Scalars['Boolean']>,
  format?: Maybe<MarkdownExcerptFormats>
};


export type MarkdownRemarkExcerptAstArgs = {
  pruneLength?: Maybe<Scalars['Int']>,
  truncate?: Maybe<Scalars['Boolean']>
};


export type MarkdownRemarkHeadingsArgs = {
  depth?: Maybe<MarkdownHeadingLevels>
};


export type MarkdownRemarkTableOfContentsArgs = {
  absolute?: Maybe<Scalars['Boolean']>,
  pathToSlugField?: Maybe<Scalars['String']>,
  maxDepth?: Maybe<Scalars['Int']>,
  heading?: Maybe<Scalars['String']>
};

export type MarkdownRemarkConnection = {
  totalCount: Scalars['Int'],
  edges: Array<MarkdownRemarkEdge>,
  nodes: Array<MarkdownRemark>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<MarkdownRemarkGroupConnection>,
};


export type MarkdownRemarkConnectionDistinctArgs = {
  field: MarkdownRemarkFieldsEnum
};


export type MarkdownRemarkConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: MarkdownRemarkFieldsEnum
};

export type MarkdownRemarkEdge = {
  next?: Maybe<MarkdownRemark>,
  node: MarkdownRemark,
  previous?: Maybe<MarkdownRemark>,
};

export type MarkdownRemarkFields = {
  slug?: Maybe<Scalars['String']>,
};

export type MarkdownRemarkFieldsEnum = 
  'id' |
  'frontmatter___title' |
  'frontmatter___templateKey' |
  'frontmatter___draft' |
  'frontmatter___featured' |
  'frontmatter___featuredMedia___sourceInstanceName' |
  'frontmatter___featuredMedia___absolutePath' |
  'frontmatter___featuredMedia___relativePath' |
  'frontmatter___featuredMedia___extension' |
  'frontmatter___featuredMedia___size' |
  'frontmatter___featuredMedia___prettySize' |
  'frontmatter___featuredMedia___modifiedTime' |
  'frontmatter___featuredMedia___accessTime' |
  'frontmatter___featuredMedia___changeTime' |
  'frontmatter___featuredMedia___birthTime' |
  'frontmatter___featuredMedia___root' |
  'frontmatter___featuredMedia___dir' |
  'frontmatter___featuredMedia___base' |
  'frontmatter___featuredMedia___ext' |
  'frontmatter___featuredMedia___name' |
  'frontmatter___featuredMedia___relativeDirectory' |
  'frontmatter___featuredMedia___dev' |
  'frontmatter___featuredMedia___mode' |
  'frontmatter___featuredMedia___nlink' |
  'frontmatter___featuredMedia___uid' |
  'frontmatter___featuredMedia___gid' |
  'frontmatter___featuredMedia___rdev' |
  'frontmatter___featuredMedia___ino' |
  'frontmatter___featuredMedia___atimeMs' |
  'frontmatter___featuredMedia___mtimeMs' |
  'frontmatter___featuredMedia___ctimeMs' |
  'frontmatter___featuredMedia___atime' |
  'frontmatter___featuredMedia___mtime' |
  'frontmatter___featuredMedia___ctime' |
  'frontmatter___featuredMedia___birthtime' |
  'frontmatter___featuredMedia___birthtimeMs' |
  'frontmatter___featuredMedia___blksize' |
  'frontmatter___featuredMedia___blocks' |
  'frontmatter___featuredMedia___publicURL' |
  'frontmatter___featuredMedia___childImageSharp___id' |
  'frontmatter___featuredMedia___childImageSharp___children' |
  'frontmatter___featuredMedia___id' |
  'frontmatter___featuredMedia___parent___id' |
  'frontmatter___featuredMedia___parent___children' |
  'frontmatter___featuredMedia___children' |
  'frontmatter___featuredMedia___children___id' |
  'frontmatter___featuredMedia___children___children' |
  'frontmatter___featuredMedia___internal___content' |
  'frontmatter___featuredMedia___internal___contentDigest' |
  'frontmatter___featuredMedia___internal___description' |
  'frontmatter___featuredMedia___internal___fieldOwners' |
  'frontmatter___featuredMedia___internal___ignoreType' |
  'frontmatter___featuredMedia___internal___mediaType' |
  'frontmatter___featuredMedia___internal___owner' |
  'frontmatter___featuredMedia___internal___type' |
  'frontmatter___featuredMedia___childMarkdownRemark___id' |
  'frontmatter___featuredMedia___childMarkdownRemark___excerpt' |
  'frontmatter___featuredMedia___childMarkdownRemark___rawMarkdownBody' |
  'frontmatter___featuredMedia___childMarkdownRemark___fileAbsolutePath' |
  'frontmatter___featuredMedia___childMarkdownRemark___html' |
  'frontmatter___featuredMedia___childMarkdownRemark___htmlAst' |
  'frontmatter___featuredMedia___childMarkdownRemark___excerptAst' |
  'frontmatter___featuredMedia___childMarkdownRemark___headings' |
  'frontmatter___featuredMedia___childMarkdownRemark___timeToRead' |
  'frontmatter___featuredMedia___childMarkdownRemark___tableOfContents' |
  'frontmatter___featuredMedia___childMarkdownRemark___children' |
  'frontmatter___date' |
  'frontmatter___stackshareUrl' |
  'frontmatter___repoUrl' |
  'frontmatter___liveUrl' |
  'frontmatter___description' |
  'frontmatter___path' |
  'frontmatter___hero___sourceInstanceName' |
  'frontmatter___hero___absolutePath' |
  'frontmatter___hero___relativePath' |
  'frontmatter___hero___extension' |
  'frontmatter___hero___size' |
  'frontmatter___hero___prettySize' |
  'frontmatter___hero___modifiedTime' |
  'frontmatter___hero___accessTime' |
  'frontmatter___hero___changeTime' |
  'frontmatter___hero___birthTime' |
  'frontmatter___hero___root' |
  'frontmatter___hero___dir' |
  'frontmatter___hero___base' |
  'frontmatter___hero___ext' |
  'frontmatter___hero___name' |
  'frontmatter___hero___relativeDirectory' |
  'frontmatter___hero___dev' |
  'frontmatter___hero___mode' |
  'frontmatter___hero___nlink' |
  'frontmatter___hero___uid' |
  'frontmatter___hero___gid' |
  'frontmatter___hero___rdev' |
  'frontmatter___hero___ino' |
  'frontmatter___hero___atimeMs' |
  'frontmatter___hero___mtimeMs' |
  'frontmatter___hero___ctimeMs' |
  'frontmatter___hero___atime' |
  'frontmatter___hero___mtime' |
  'frontmatter___hero___ctime' |
  'frontmatter___hero___birthtime' |
  'frontmatter___hero___birthtimeMs' |
  'frontmatter___hero___blksize' |
  'frontmatter___hero___blocks' |
  'frontmatter___hero___publicURL' |
  'frontmatter___hero___childImageSharp___id' |
  'frontmatter___hero___childImageSharp___children' |
  'frontmatter___hero___id' |
  'frontmatter___hero___parent___id' |
  'frontmatter___hero___parent___children' |
  'frontmatter___hero___children' |
  'frontmatter___hero___children___id' |
  'frontmatter___hero___children___children' |
  'frontmatter___hero___internal___content' |
  'frontmatter___hero___internal___contentDigest' |
  'frontmatter___hero___internal___description' |
  'frontmatter___hero___internal___fieldOwners' |
  'frontmatter___hero___internal___ignoreType' |
  'frontmatter___hero___internal___mediaType' |
  'frontmatter___hero___internal___owner' |
  'frontmatter___hero___internal___type' |
  'frontmatter___hero___childMarkdownRemark___id' |
  'frontmatter___hero___childMarkdownRemark___excerpt' |
  'frontmatter___hero___childMarkdownRemark___rawMarkdownBody' |
  'frontmatter___hero___childMarkdownRemark___fileAbsolutePath' |
  'frontmatter___hero___childMarkdownRemark___html' |
  'frontmatter___hero___childMarkdownRemark___htmlAst' |
  'frontmatter___hero___childMarkdownRemark___excerptAst' |
  'frontmatter___hero___childMarkdownRemark___headings' |
  'frontmatter___hero___childMarkdownRemark___timeToRead' |
  'frontmatter___hero___childMarkdownRemark___tableOfContents' |
  'frontmatter___hero___childMarkdownRemark___children' |
  'frontmatter___subtitle' |
  'frontmatter___ingredients' |
  'frontmatter___ingredients___name' |
  'frontmatter___ingredients___quantity' |
  'frontmatter___ingredients___unit' |
  'frontmatter___instructions' |
  'frontmatter___servingSize' |
  'frontmatter___prepTime' |
  'frontmatter___cookTime' |
  'frontmatter___tags' |
  'excerpt' |
  'rawMarkdownBody' |
  'fileAbsolutePath' |
  'fields___slug' |
  'html' |
  'htmlAst' |
  'excerptAst' |
  'headings' |
  'headings___value' |
  'headings___depth' |
  'timeToRead' |
  'tableOfContents' |
  'wordCount___paragraphs' |
  'wordCount___sentences' |
  'wordCount___words' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type';

export type MarkdownRemarkFieldsFilterInput = {
  slug?: Maybe<StringQueryOperatorInput>,
};

export type MarkdownRemarkFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  frontmatter?: Maybe<MarkdownRemarkFrontmatterFilterInput>,
  excerpt?: Maybe<StringQueryOperatorInput>,
  rawMarkdownBody?: Maybe<StringQueryOperatorInput>,
  fileAbsolutePath?: Maybe<StringQueryOperatorInput>,
  fields?: Maybe<MarkdownRemarkFieldsFilterInput>,
  html?: Maybe<StringQueryOperatorInput>,
  htmlAst?: Maybe<JsonQueryOperatorInput>,
  excerptAst?: Maybe<JsonQueryOperatorInput>,
  headings?: Maybe<MarkdownHeadingFilterListInput>,
  timeToRead?: Maybe<IntQueryOperatorInput>,
  tableOfContents?: Maybe<StringQueryOperatorInput>,
  wordCount?: Maybe<MarkdownWordCountFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
};

export type MarkdownRemarkFrontmatter = {
  title?: Maybe<Scalars['String']>,
  templateKey?: Maybe<Scalars['String']>,
  draft?: Maybe<Scalars['Boolean']>,
  featured?: Maybe<Scalars['Boolean']>,
  featuredMedia?: Maybe<File>,
  date?: Maybe<Scalars['Date']>,
  stackshareUrl?: Maybe<Scalars['String']>,
  repoUrl?: Maybe<Scalars['String']>,
  liveUrl?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  path?: Maybe<Scalars['String']>,
  hero?: Maybe<File>,
  subtitle?: Maybe<Scalars['String']>,
  ingredients?: Maybe<Array<Maybe<MarkdownRemarkFrontmatterIngredients>>>,
  instructions?: Maybe<Scalars['String']>,
  servingSize?: Maybe<Scalars['String']>,
  prepTime?: Maybe<Scalars['String']>,
  cookTime?: Maybe<Scalars['String']>,
  tags?: Maybe<Array<Maybe<Scalars['String']>>>,
};


export type MarkdownRemarkFrontmatterDateArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type MarkdownRemarkFrontmatterFilterInput = {
  title?: Maybe<StringQueryOperatorInput>,
  templateKey?: Maybe<StringQueryOperatorInput>,
  draft?: Maybe<BooleanQueryOperatorInput>,
  featured?: Maybe<BooleanQueryOperatorInput>,
  featuredMedia?: Maybe<FileFilterInput>,
  date?: Maybe<DateQueryOperatorInput>,
  stackshareUrl?: Maybe<StringQueryOperatorInput>,
  repoUrl?: Maybe<StringQueryOperatorInput>,
  liveUrl?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  path?: Maybe<StringQueryOperatorInput>,
  hero?: Maybe<FileFilterInput>,
  subtitle?: Maybe<StringQueryOperatorInput>,
  ingredients?: Maybe<MarkdownRemarkFrontmatterIngredientsFilterListInput>,
  instructions?: Maybe<StringQueryOperatorInput>,
  servingSize?: Maybe<StringQueryOperatorInput>,
  prepTime?: Maybe<StringQueryOperatorInput>,
  cookTime?: Maybe<StringQueryOperatorInput>,
  tags?: Maybe<StringQueryOperatorInput>,
};

export type MarkdownRemarkFrontmatterIngredients = {
  name?: Maybe<Scalars['String']>,
  quantity?: Maybe<Scalars['String']>,
  unit?: Maybe<Scalars['String']>,
};

export type MarkdownRemarkFrontmatterIngredientsFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  quantity?: Maybe<StringQueryOperatorInput>,
  unit?: Maybe<StringQueryOperatorInput>,
};

export type MarkdownRemarkFrontmatterIngredientsFilterListInput = {
  elemMatch?: Maybe<MarkdownRemarkFrontmatterIngredientsFilterInput>,
};

export type MarkdownRemarkGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<MarkdownRemarkEdge>,
  nodes: Array<MarkdownRemark>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type MarkdownRemarkSortInput = {
  fields?: Maybe<Array<Maybe<MarkdownRemarkFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type MarkdownWordCount = {
  paragraphs?: Maybe<Scalars['Int']>,
  sentences?: Maybe<Scalars['Int']>,
  words?: Maybe<Scalars['Int']>,
};

export type MarkdownWordCountFilterInput = {
  paragraphs?: Maybe<IntQueryOperatorInput>,
  sentences?: Maybe<IntQueryOperatorInput>,
  words?: Maybe<IntQueryOperatorInput>,
};

/** Node Interface */
export type Node = {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
};

export type NodeFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
};

export type NodeFilterListInput = {
  elemMatch?: Maybe<NodeFilterInput>,
};

export type PageInfo = {
  currentPage: Scalars['Int'],
  hasPreviousPage: Scalars['Boolean'],
  hasNextPage: Scalars['Boolean'],
  itemCount: Scalars['Int'],
  pageCount: Scalars['Int'],
  perPage?: Maybe<Scalars['Int']>,
};

export type Potrace = {
  turnPolicy?: Maybe<PotraceTurnPolicy>,
  turdSize?: Maybe<Scalars['Float']>,
  alphaMax?: Maybe<Scalars['Float']>,
  optCurve?: Maybe<Scalars['Boolean']>,
  optTolerance?: Maybe<Scalars['Float']>,
  threshold?: Maybe<Scalars['Int']>,
  blackOnWhite?: Maybe<Scalars['Boolean']>,
  color?: Maybe<Scalars['String']>,
  background?: Maybe<Scalars['String']>,
};

export type PotraceTurnPolicy = 
  'TURNPOLICY_BLACK' |
  'TURNPOLICY_WHITE' |
  'TURNPOLICY_LEFT' |
  'TURNPOLICY_RIGHT' |
  'TURNPOLICY_MINORITY' |
  'TURNPOLICY_MAJORITY';

export type Query = {
  file?: Maybe<File>,
  allFile: FileConnection,
  directory?: Maybe<Directory>,
  allDirectory: DirectoryConnection,
  sitePage?: Maybe<SitePage>,
  allSitePage: SitePageConnection,
  site?: Maybe<Site>,
  allSite: SiteConnection,
  markdownRemark?: Maybe<MarkdownRemark>,
  allMarkdownRemark: MarkdownRemarkConnection,
  imageSharp?: Maybe<ImageSharp>,
  allImageSharp: ImageSharpConnection,
  goodreadsShelf?: Maybe<GoodreadsShelf>,
  allGoodreadsShelf: GoodreadsShelfConnection,
  goodreadsBook?: Maybe<GoodreadsBook>,
  allGoodreadsBook: GoodreadsBookConnection,
  goodreadsAuthor?: Maybe<GoodreadsAuthor>,
  allGoodreadsAuthor: GoodreadsAuthorConnection,
  goodreadsReview?: Maybe<GoodreadsReview>,
  allGoodreadsReview: GoodreadsReviewConnection,
  siteBuildMetadata?: Maybe<SiteBuildMetadata>,
  allSiteBuildMetadata: SiteBuildMetadataConnection,
  sitePlugin?: Maybe<SitePlugin>,
  allSitePlugin: SitePluginConnection,
};


export type QueryFileArgs = {
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  absolutePath?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  ino?: Maybe<FloatQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  publicURL?: Maybe<StringQueryOperatorInput>,
  childImageSharp?: Maybe<ImageSharpFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  childMarkdownRemark?: Maybe<MarkdownRemarkFilterInput>
};


export type QueryAllFileArgs = {
  filter?: Maybe<FileFilterInput>,
  sort?: Maybe<FileSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryDirectoryArgs = {
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  absolutePath?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  ino?: Maybe<FloatQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>
};


export type QueryAllDirectoryArgs = {
  filter?: Maybe<DirectoryFilterInput>,
  sort?: Maybe<DirectorySortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QuerySitePageArgs = {
  path?: Maybe<StringQueryOperatorInput>,
  component?: Maybe<StringQueryOperatorInput>,
  internalComponentName?: Maybe<StringQueryOperatorInput>,
  componentChunkName?: Maybe<StringQueryOperatorInput>,
  matchPath?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  isCreatedByStatefulCreatePages?: Maybe<BooleanQueryOperatorInput>,
  context?: Maybe<SitePageContextFilterInput>,
  pluginCreator?: Maybe<SitePluginFilterInput>,
  pluginCreatorId?: Maybe<StringQueryOperatorInput>,
  componentPath?: Maybe<StringQueryOperatorInput>
};


export type QueryAllSitePageArgs = {
  filter?: Maybe<SitePageFilterInput>,
  sort?: Maybe<SitePageSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QuerySiteArgs = {
  buildTime?: Maybe<DateQueryOperatorInput>,
  siteMetadata?: Maybe<SiteSiteMetadataFilterInput>,
  port?: Maybe<IntQueryOperatorInput>,
  host?: Maybe<StringQueryOperatorInput>,
  pathPrefix?: Maybe<StringQueryOperatorInput>,
  polyfill?: Maybe<BooleanQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>
};


export type QueryAllSiteArgs = {
  filter?: Maybe<SiteFilterInput>,
  sort?: Maybe<SiteSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryMarkdownRemarkArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  frontmatter?: Maybe<MarkdownRemarkFrontmatterFilterInput>,
  excerpt?: Maybe<StringQueryOperatorInput>,
  rawMarkdownBody?: Maybe<StringQueryOperatorInput>,
  fileAbsolutePath?: Maybe<StringQueryOperatorInput>,
  fields?: Maybe<MarkdownRemarkFieldsFilterInput>,
  html?: Maybe<StringQueryOperatorInput>,
  htmlAst?: Maybe<JsonQueryOperatorInput>,
  excerptAst?: Maybe<JsonQueryOperatorInput>,
  headings?: Maybe<MarkdownHeadingFilterListInput>,
  timeToRead?: Maybe<IntQueryOperatorInput>,
  tableOfContents?: Maybe<StringQueryOperatorInput>,
  wordCount?: Maybe<MarkdownWordCountFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>
};


export type QueryAllMarkdownRemarkArgs = {
  filter?: Maybe<MarkdownRemarkFilterInput>,
  sort?: Maybe<MarkdownRemarkSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryImageSharpArgs = {
  fixed?: Maybe<ImageSharpFixedFilterInput>,
  resolutions?: Maybe<ImageSharpResolutionsFilterInput>,
  fluid?: Maybe<ImageSharpFluidFilterInput>,
  sizes?: Maybe<ImageSharpSizesFilterInput>,
  original?: Maybe<ImageSharpOriginalFilterInput>,
  resize?: Maybe<ImageSharpResizeFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>
};


export type QueryAllImageSharpArgs = {
  filter?: Maybe<ImageSharpFilterInput>,
  sort?: Maybe<ImageSharpSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryGoodreadsShelfArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  exclusive?: Maybe<StringQueryOperatorInput>,
  review_shelf_id?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  reviews?: Maybe<GoodreadsReviewFilterListInput>,
  sortable?: Maybe<StringQueryOperatorInput>
};


export type QueryAllGoodreadsShelfArgs = {
  filter?: Maybe<GoodreadsShelfFilterInput>,
  sort?: Maybe<GoodreadsShelfSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryGoodreadsBookArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  isbn?: Maybe<StringQueryOperatorInput>,
  isbn13?: Maybe<StringQueryOperatorInput>,
  text_reviews_count?: Maybe<StringQueryOperatorInput>,
  uri?: Maybe<StringQueryOperatorInput>,
  title?: Maybe<StringQueryOperatorInput>,
  title_without_series?: Maybe<StringQueryOperatorInput>,
  image_url?: Maybe<StringQueryOperatorInput>,
  small_image_url?: Maybe<StringQueryOperatorInput>,
  large_image_url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  num_pages?: Maybe<StringQueryOperatorInput>,
  format?: Maybe<StringQueryOperatorInput>,
  edition_information?: Maybe<StringQueryOperatorInput>,
  publisher?: Maybe<StringQueryOperatorInput>,
  publication_day?: Maybe<StringQueryOperatorInput>,
  publication_year?: Maybe<DateQueryOperatorInput>,
  publication_month?: Maybe<StringQueryOperatorInput>,
  average_rating?: Maybe<StringQueryOperatorInput>,
  ratings_count?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  published?: Maybe<DateQueryOperatorInput>,
  work?: Maybe<GoodreadsBookWorkFilterInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  reviews?: Maybe<GoodreadsReviewFilterListInput>,
  authors?: Maybe<GoodreadsAuthorFilterListInput>
};


export type QueryAllGoodreadsBookArgs = {
  filter?: Maybe<GoodreadsBookFilterInput>,
  sort?: Maybe<GoodreadsBookSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryGoodreadsAuthorArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  role?: Maybe<StringQueryOperatorInput>,
  image_url?: Maybe<StringQueryOperatorInput>,
  small_image_url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  average_rating?: Maybe<StringQueryOperatorInput>,
  ratings_count?: Maybe<StringQueryOperatorInput>,
  text_reviews_count?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  books?: Maybe<GoodreadsBookFilterListInput>
};


export type QueryAllGoodreadsAuthorArgs = {
  filter?: Maybe<GoodreadsAuthorFilterInput>,
  sort?: Maybe<GoodreadsAuthorSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QueryGoodreadsReviewArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  rating?: Maybe<StringQueryOperatorInput>,
  votes?: Maybe<StringQueryOperatorInput>,
  spoiler_flag?: Maybe<StringQueryOperatorInput>,
  spoilers_state?: Maybe<StringQueryOperatorInput>,
  recommended_for?: Maybe<StringQueryOperatorInput>,
  recommended_by?: Maybe<StringQueryOperatorInput>,
  started_at?: Maybe<StringQueryOperatorInput>,
  read_at?: Maybe<StringQueryOperatorInput>,
  date_added?: Maybe<StringQueryOperatorInput>,
  date_updated?: Maybe<StringQueryOperatorInput>,
  read_count?: Maybe<StringQueryOperatorInput>,
  body?: Maybe<StringQueryOperatorInput>,
  comments_count?: Maybe<StringQueryOperatorInput>,
  url?: Maybe<StringQueryOperatorInput>,
  link?: Maybe<StringQueryOperatorInput>,
  owned?: Maybe<StringQueryOperatorInput>,
  goodreadsId?: Maybe<StringQueryOperatorInput>,
  book?: Maybe<GoodreadsBookFilterInput>
};


export type QueryAllGoodreadsReviewArgs = {
  filter?: Maybe<GoodreadsReviewFilterInput>,
  sort?: Maybe<GoodreadsReviewSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QuerySiteBuildMetadataArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  buildTime?: Maybe<DateQueryOperatorInput>
};


export type QueryAllSiteBuildMetadataArgs = {
  filter?: Maybe<SiteBuildMetadataFilterInput>,
  sort?: Maybe<SiteBuildMetadataSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};


export type QuerySitePluginArgs = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  resolve?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
  pluginOptions?: Maybe<SitePluginPluginOptionsFilterInput>,
  nodeAPIs?: Maybe<StringQueryOperatorInput>,
  browserAPIs?: Maybe<StringQueryOperatorInput>,
  ssrAPIs?: Maybe<StringQueryOperatorInput>,
  pluginFilepath?: Maybe<StringQueryOperatorInput>,
  packageJson?: Maybe<SitePluginPackageJsonFilterInput>
};


export type QueryAllSitePluginArgs = {
  filter?: Maybe<SitePluginFilterInput>,
  sort?: Maybe<SitePluginSortInput>,
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>
};

export type Site = Node & {
  buildTime?: Maybe<Scalars['Date']>,
  siteMetadata?: Maybe<SiteSiteMetadata>,
  port?: Maybe<Scalars['Int']>,
  host?: Maybe<Scalars['String']>,
  pathPrefix?: Maybe<Scalars['String']>,
  polyfill?: Maybe<Scalars['Boolean']>,
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
};


export type SiteBuildTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type SiteBuildMetadata = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  buildTime?: Maybe<Scalars['Date']>,
};


export type SiteBuildMetadataBuildTimeArgs = {
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  difference?: Maybe<Scalars['String']>,
  locale?: Maybe<Scalars['String']>
};

export type SiteBuildMetadataConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SiteBuildMetadataEdge>,
  nodes: Array<SiteBuildMetadata>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<SiteBuildMetadataGroupConnection>,
};


export type SiteBuildMetadataConnectionDistinctArgs = {
  field: SiteBuildMetadataFieldsEnum
};


export type SiteBuildMetadataConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: SiteBuildMetadataFieldsEnum
};

export type SiteBuildMetadataEdge = {
  next?: Maybe<SiteBuildMetadata>,
  node: SiteBuildMetadata,
  previous?: Maybe<SiteBuildMetadata>,
};

export type SiteBuildMetadataFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'buildTime';

export type SiteBuildMetadataFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  buildTime?: Maybe<DateQueryOperatorInput>,
};

export type SiteBuildMetadataGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SiteBuildMetadataEdge>,
  nodes: Array<SiteBuildMetadata>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type SiteBuildMetadataSortInput = {
  fields?: Maybe<Array<Maybe<SiteBuildMetadataFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SiteConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SiteEdge>,
  nodes: Array<Site>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<SiteGroupConnection>,
};


export type SiteConnectionDistinctArgs = {
  field: SiteFieldsEnum
};


export type SiteConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: SiteFieldsEnum
};

export type SiteEdge = {
  next?: Maybe<Site>,
  node: Site,
  previous?: Maybe<Site>,
};

export type SiteFieldsEnum = 
  'buildTime' |
  'siteMetadata___lang' |
  'siteMetadata___title' |
  'siteMetadata___defaultTitle' |
  'siteMetadata___titleTemplate' |
  'siteMetadata___description' |
  'siteMetadata___image' |
  'siteMetadata___facebookAppId' |
  'siteMetadata___twitterUsername' |
  'siteMetadata___siteUrl' |
  'siteMetadata___socialLinks' |
  'siteMetadata___socialLinks___name' |
  'siteMetadata___socialLinks___url' |
  'siteMetadata___socialLinks___title' |
  'siteMetadata___socialLinks___brandIcon' |
  'siteMetadata___headerMenuItems' |
  'siteMetadata___headerMenuItems___name' |
  'siteMetadata___headerMenuItems___path' |
  'port' |
  'host' |
  'pathPrefix' |
  'polyfill' |
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type';

export type SiteFilterInput = {
  buildTime?: Maybe<DateQueryOperatorInput>,
  siteMetadata?: Maybe<SiteSiteMetadataFilterInput>,
  port?: Maybe<IntQueryOperatorInput>,
  host?: Maybe<StringQueryOperatorInput>,
  pathPrefix?: Maybe<StringQueryOperatorInput>,
  polyfill?: Maybe<BooleanQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
};

export type SiteGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SiteEdge>,
  nodes: Array<Site>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type SitePage = Node & {
  path: Scalars['String'],
  component: Scalars['String'],
  internalComponentName: Scalars['String'],
  componentChunkName: Scalars['String'],
  matchPath?: Maybe<Scalars['String']>,
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  isCreatedByStatefulCreatePages?: Maybe<Scalars['Boolean']>,
  context?: Maybe<SitePageContext>,
  pluginCreator?: Maybe<SitePlugin>,
  pluginCreatorId?: Maybe<Scalars['String']>,
  componentPath?: Maybe<Scalars['String']>,
};

export type SitePageConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SitePageEdge>,
  nodes: Array<SitePage>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<SitePageGroupConnection>,
};


export type SitePageConnectionDistinctArgs = {
  field: SitePageFieldsEnum
};


export type SitePageConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: SitePageFieldsEnum
};

export type SitePageContext = {
  id?: Maybe<Scalars['String']>,
};

export type SitePageContextFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
};

export type SitePageEdge = {
  next?: Maybe<SitePage>,
  node: SitePage,
  previous?: Maybe<SitePage>,
};

export type SitePageFieldsEnum = 
  'path' |
  'component' |
  'internalComponentName' |
  'componentChunkName' |
  'matchPath' |
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'isCreatedByStatefulCreatePages' |
  'context___id' |
  'pluginCreator___id' |
  'pluginCreator___parent___id' |
  'pluginCreator___parent___parent___id' |
  'pluginCreator___parent___parent___children' |
  'pluginCreator___parent___children' |
  'pluginCreator___parent___children___id' |
  'pluginCreator___parent___children___children' |
  'pluginCreator___parent___internal___content' |
  'pluginCreator___parent___internal___contentDigest' |
  'pluginCreator___parent___internal___description' |
  'pluginCreator___parent___internal___fieldOwners' |
  'pluginCreator___parent___internal___ignoreType' |
  'pluginCreator___parent___internal___mediaType' |
  'pluginCreator___parent___internal___owner' |
  'pluginCreator___parent___internal___type' |
  'pluginCreator___children' |
  'pluginCreator___children___id' |
  'pluginCreator___children___parent___id' |
  'pluginCreator___children___parent___children' |
  'pluginCreator___children___children' |
  'pluginCreator___children___children___id' |
  'pluginCreator___children___children___children' |
  'pluginCreator___children___internal___content' |
  'pluginCreator___children___internal___contentDigest' |
  'pluginCreator___children___internal___description' |
  'pluginCreator___children___internal___fieldOwners' |
  'pluginCreator___children___internal___ignoreType' |
  'pluginCreator___children___internal___mediaType' |
  'pluginCreator___children___internal___owner' |
  'pluginCreator___children___internal___type' |
  'pluginCreator___internal___content' |
  'pluginCreator___internal___contentDigest' |
  'pluginCreator___internal___description' |
  'pluginCreator___internal___fieldOwners' |
  'pluginCreator___internal___ignoreType' |
  'pluginCreator___internal___mediaType' |
  'pluginCreator___internal___owner' |
  'pluginCreator___internal___type' |
  'pluginCreator___resolve' |
  'pluginCreator___name' |
  'pluginCreator___version' |
  'pluginCreator___pluginOptions___plugins' |
  'pluginCreator___pluginOptions___plugins___resolve' |
  'pluginCreator___pluginOptions___plugins___id' |
  'pluginCreator___pluginOptions___plugins___name' |
  'pluginCreator___pluginOptions___plugins___version' |
  'pluginCreator___pluginOptions___plugins___browserAPIs' |
  'pluginCreator___pluginOptions___plugins___ssrAPIs' |
  'pluginCreator___pluginOptions___plugins___pluginFilepath' |
  'pluginCreator___pluginOptions___path' |
  'pluginCreator___pluginOptions___name' |
  'pluginCreator___pluginOptions___key' |
  'pluginCreator___pluginOptions___id' |
  'pluginCreator___pluginOptions___className' |
  'pluginCreator___pluginOptions___maxWidth' |
  'pluginCreator___pluginOptions___active' |
  'pluginCreator___pluginOptions___size' |
  'pluginCreator___pluginOptions___target' |
  'pluginCreator___pluginOptions___rel' |
  'pluginCreator___pluginOptions___modulePath' |
  'pluginCreator___pluginOptions___output' |
  'pluginCreator___pluginOptions___exclude' |
  'pluginCreator___pluginOptions___anonymize' |
  'pluginCreator___pluginOptions___respectDNT' |
  'pluginCreator___pluginOptions___color' |
  'pluginCreator___pluginOptions___showSpinner' |
  'pluginCreator___pluginOptions___short_name' |
  'pluginCreator___pluginOptions___start_url' |
  'pluginCreator___pluginOptions___background_color' |
  'pluginCreator___pluginOptions___theme_color' |
  'pluginCreator___pluginOptions___display' |
  'pluginCreator___pluginOptions___icon' |
  'pluginCreator___pluginOptions___analyzerPort' |
  'pluginCreator___pluginOptions___production' |
  'pluginCreator___pluginOptions___disable' |
  'pluginCreator___pluginOptions___openAnalyzer' |
  'pluginCreator___pluginOptions___precachePages' |
  'pluginCreator___pluginOptions___allPageHeaders' |
  'pluginCreator___pluginOptions___pathCheck' |
  'pluginCreator___nodeAPIs' |
  'pluginCreator___browserAPIs' |
  'pluginCreator___ssrAPIs' |
  'pluginCreator___pluginFilepath' |
  'pluginCreator___packageJson___name' |
  'pluginCreator___packageJson___description' |
  'pluginCreator___packageJson___version' |
  'pluginCreator___packageJson___main' |
  'pluginCreator___packageJson___license' |
  'pluginCreator___packageJson___dependencies' |
  'pluginCreator___packageJson___dependencies___name' |
  'pluginCreator___packageJson___dependencies___version' |
  'pluginCreator___packageJson___devDependencies' |
  'pluginCreator___packageJson___devDependencies___name' |
  'pluginCreator___packageJson___devDependencies___version' |
  'pluginCreator___packageJson___peerDependencies' |
  'pluginCreator___packageJson___peerDependencies___name' |
  'pluginCreator___packageJson___peerDependencies___version' |
  'pluginCreator___packageJson___keywords' |
  'pluginCreatorId' |
  'componentPath';

export type SitePageFilterInput = {
  path?: Maybe<StringQueryOperatorInput>,
  component?: Maybe<StringQueryOperatorInput>,
  internalComponentName?: Maybe<StringQueryOperatorInput>,
  componentChunkName?: Maybe<StringQueryOperatorInput>,
  matchPath?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  isCreatedByStatefulCreatePages?: Maybe<BooleanQueryOperatorInput>,
  context?: Maybe<SitePageContextFilterInput>,
  pluginCreator?: Maybe<SitePluginFilterInput>,
  pluginCreatorId?: Maybe<StringQueryOperatorInput>,
  componentPath?: Maybe<StringQueryOperatorInput>,
};

export type SitePageGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SitePageEdge>,
  nodes: Array<SitePage>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type SitePageSortInput = {
  fields?: Maybe<Array<Maybe<SitePageFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SitePlugin = Node & {
  id: Scalars['ID'],
  parent?: Maybe<Node>,
  children: Array<Node>,
  internal: Internal,
  resolve?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
  pluginOptions?: Maybe<SitePluginPluginOptions>,
  nodeAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  browserAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  ssrAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  pluginFilepath?: Maybe<Scalars['String']>,
  packageJson?: Maybe<SitePluginPackageJson>,
};

export type SitePluginConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SitePluginEdge>,
  nodes: Array<SitePlugin>,
  pageInfo: PageInfo,
  distinct: Array<Scalars['String']>,
  group: Array<SitePluginGroupConnection>,
};


export type SitePluginConnectionDistinctArgs = {
  field: SitePluginFieldsEnum
};


export type SitePluginConnectionGroupArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  field: SitePluginFieldsEnum
};

export type SitePluginEdge = {
  next?: Maybe<SitePlugin>,
  node: SitePlugin,
  previous?: Maybe<SitePlugin>,
};

export type SitePluginFieldsEnum = 
  'id' |
  'parent___id' |
  'parent___parent___id' |
  'parent___parent___parent___id' |
  'parent___parent___parent___children' |
  'parent___parent___children' |
  'parent___parent___children___id' |
  'parent___parent___children___children' |
  'parent___parent___internal___content' |
  'parent___parent___internal___contentDigest' |
  'parent___parent___internal___description' |
  'parent___parent___internal___fieldOwners' |
  'parent___parent___internal___ignoreType' |
  'parent___parent___internal___mediaType' |
  'parent___parent___internal___owner' |
  'parent___parent___internal___type' |
  'parent___children' |
  'parent___children___id' |
  'parent___children___parent___id' |
  'parent___children___parent___children' |
  'parent___children___children' |
  'parent___children___children___id' |
  'parent___children___children___children' |
  'parent___children___internal___content' |
  'parent___children___internal___contentDigest' |
  'parent___children___internal___description' |
  'parent___children___internal___fieldOwners' |
  'parent___children___internal___ignoreType' |
  'parent___children___internal___mediaType' |
  'parent___children___internal___owner' |
  'parent___children___internal___type' |
  'parent___internal___content' |
  'parent___internal___contentDigest' |
  'parent___internal___description' |
  'parent___internal___fieldOwners' |
  'parent___internal___ignoreType' |
  'parent___internal___mediaType' |
  'parent___internal___owner' |
  'parent___internal___type' |
  'children' |
  'children___id' |
  'children___parent___id' |
  'children___parent___parent___id' |
  'children___parent___parent___children' |
  'children___parent___children' |
  'children___parent___children___id' |
  'children___parent___children___children' |
  'children___parent___internal___content' |
  'children___parent___internal___contentDigest' |
  'children___parent___internal___description' |
  'children___parent___internal___fieldOwners' |
  'children___parent___internal___ignoreType' |
  'children___parent___internal___mediaType' |
  'children___parent___internal___owner' |
  'children___parent___internal___type' |
  'children___children' |
  'children___children___id' |
  'children___children___parent___id' |
  'children___children___parent___children' |
  'children___children___children' |
  'children___children___children___id' |
  'children___children___children___children' |
  'children___children___internal___content' |
  'children___children___internal___contentDigest' |
  'children___children___internal___description' |
  'children___children___internal___fieldOwners' |
  'children___children___internal___ignoreType' |
  'children___children___internal___mediaType' |
  'children___children___internal___owner' |
  'children___children___internal___type' |
  'children___internal___content' |
  'children___internal___contentDigest' |
  'children___internal___description' |
  'children___internal___fieldOwners' |
  'children___internal___ignoreType' |
  'children___internal___mediaType' |
  'children___internal___owner' |
  'children___internal___type' |
  'internal___content' |
  'internal___contentDigest' |
  'internal___description' |
  'internal___fieldOwners' |
  'internal___ignoreType' |
  'internal___mediaType' |
  'internal___owner' |
  'internal___type' |
  'resolve' |
  'name' |
  'version' |
  'pluginOptions___plugins' |
  'pluginOptions___plugins___resolve' |
  'pluginOptions___plugins___id' |
  'pluginOptions___plugins___name' |
  'pluginOptions___plugins___version' |
  'pluginOptions___plugins___pluginOptions___className' |
  'pluginOptions___plugins___pluginOptions___maxWidth' |
  'pluginOptions___plugins___pluginOptions___active' |
  'pluginOptions___plugins___pluginOptions___size' |
  'pluginOptions___plugins___pluginOptions___target' |
  'pluginOptions___plugins___pluginOptions___rel' |
  'pluginOptions___plugins___browserAPIs' |
  'pluginOptions___plugins___ssrAPIs' |
  'pluginOptions___plugins___pluginFilepath' |
  'pluginOptions___path' |
  'pluginOptions___name' |
  'pluginOptions___key' |
  'pluginOptions___id' |
  'pluginOptions___className' |
  'pluginOptions___maxWidth' |
  'pluginOptions___active' |
  'pluginOptions___size' |
  'pluginOptions___target' |
  'pluginOptions___rel' |
  'pluginOptions___modulePath' |
  'pluginOptions___output' |
  'pluginOptions___exclude' |
  'pluginOptions___env___production___policy' |
  'pluginOptions___env___branch_deploy___policy' |
  'pluginOptions___env___deploy_preview___policy' |
  'pluginOptions___anonymize' |
  'pluginOptions___respectDNT' |
  'pluginOptions___color' |
  'pluginOptions___showSpinner' |
  'pluginOptions___short_name' |
  'pluginOptions___start_url' |
  'pluginOptions___background_color' |
  'pluginOptions___theme_color' |
  'pluginOptions___display' |
  'pluginOptions___icon' |
  'pluginOptions___analyzerPort' |
  'pluginOptions___production' |
  'pluginOptions___disable' |
  'pluginOptions___openAnalyzer' |
  'pluginOptions___precachePages' |
  'pluginOptions___allPageHeaders' |
  'pluginOptions___pathCheck' |
  'nodeAPIs' |
  'browserAPIs' |
  'ssrAPIs' |
  'pluginFilepath' |
  'packageJson___name' |
  'packageJson___description' |
  'packageJson___version' |
  'packageJson___main' |
  'packageJson___license' |
  'packageJson___dependencies' |
  'packageJson___dependencies___name' |
  'packageJson___dependencies___version' |
  'packageJson___devDependencies' |
  'packageJson___devDependencies___name' |
  'packageJson___devDependencies___version' |
  'packageJson___peerDependencies' |
  'packageJson___peerDependencies___name' |
  'packageJson___peerDependencies___version' |
  'packageJson___keywords';

export type SitePluginFilterInput = {
  id?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  internal?: Maybe<InternalFilterInput>,
  resolve?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
  pluginOptions?: Maybe<SitePluginPluginOptionsFilterInput>,
  nodeAPIs?: Maybe<StringQueryOperatorInput>,
  browserAPIs?: Maybe<StringQueryOperatorInput>,
  ssrAPIs?: Maybe<StringQueryOperatorInput>,
  pluginFilepath?: Maybe<StringQueryOperatorInput>,
  packageJson?: Maybe<SitePluginPackageJsonFilterInput>,
};

export type SitePluginGroupConnection = {
  totalCount: Scalars['Int'],
  edges: Array<SitePluginEdge>,
  nodes: Array<SitePlugin>,
  pageInfo: PageInfo,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJson = {
  name?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
  main?: Maybe<Scalars['String']>,
  license?: Maybe<Scalars['String']>,
  dependencies?: Maybe<Array<Maybe<SitePluginPackageJsonDependencies>>>,
  devDependencies?: Maybe<Array<Maybe<SitePluginPackageJsonDevDependencies>>>,
  peerDependencies?: Maybe<Array<Maybe<SitePluginPackageJsonPeerDependencies>>>,
  keywords?: Maybe<Array<Maybe<Scalars['String']>>>,
};

export type SitePluginPackageJsonDependencies = {
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonDependenciesFilterInput>,
};

export type SitePluginPackageJsonDevDependencies = {
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonDevDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonDevDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonDevDependenciesFilterInput>,
};

export type SitePluginPackageJsonFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
  main?: Maybe<StringQueryOperatorInput>,
  license?: Maybe<StringQueryOperatorInput>,
  dependencies?: Maybe<SitePluginPackageJsonDependenciesFilterListInput>,
  devDependencies?: Maybe<SitePluginPackageJsonDevDependenciesFilterListInput>,
  peerDependencies?: Maybe<SitePluginPackageJsonPeerDependenciesFilterListInput>,
  keywords?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonPeerDependencies = {
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonPeerDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonPeerDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonPeerDependenciesFilterInput>,
};

export type SitePluginPluginOptions = {
  plugins?: Maybe<Array<Maybe<SitePluginPluginOptionsPlugins>>>,
  path?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  key?: Maybe<Scalars['String']>,
  id?: Maybe<Scalars['String']>,
  className?: Maybe<Scalars['String']>,
  maxWidth?: Maybe<Scalars['Int']>,
  active?: Maybe<Scalars['Boolean']>,
  size?: Maybe<Scalars['Int']>,
  target?: Maybe<Scalars['String']>,
  rel?: Maybe<Scalars['String']>,
  modulePath?: Maybe<Scalars['String']>,
  output?: Maybe<Scalars['String']>,
  exclude?: Maybe<Array<Maybe<Scalars['String']>>>,
  env?: Maybe<SitePluginPluginOptionsEnv>,
  anonymize?: Maybe<Scalars['Boolean']>,
  respectDNT?: Maybe<Scalars['Boolean']>,
  color?: Maybe<Scalars['String']>,
  showSpinner?: Maybe<Scalars['Boolean']>,
  short_name?: Maybe<Scalars['String']>,
  start_url?: Maybe<Scalars['String']>,
  background_color?: Maybe<Scalars['String']>,
  theme_color?: Maybe<Scalars['String']>,
  display?: Maybe<Scalars['String']>,
  icon?: Maybe<Scalars['String']>,
  analyzerPort?: Maybe<Scalars['Int']>,
  production?: Maybe<Scalars['Boolean']>,
  disable?: Maybe<Scalars['Boolean']>,
  openAnalyzer?: Maybe<Scalars['Boolean']>,
  precachePages?: Maybe<Array<Maybe<Scalars['String']>>>,
  allPageHeaders?: Maybe<Array<Maybe<Scalars['String']>>>,
  pathCheck?: Maybe<Scalars['Boolean']>,
};

export type SitePluginPluginOptionsEnv = {
  production?: Maybe<SitePluginPluginOptionsEnvProduction>,
  branch_deploy?: Maybe<SitePluginPluginOptionsEnvBranch_Deploy>,
  deploy_preview?: Maybe<SitePluginPluginOptionsEnvDeploy_Preview>,
};

export type SitePluginPluginOptionsEnvBranch_Deploy = {
  policy?: Maybe<Array<Maybe<SitePluginPluginOptionsEnvBranch_DeployPolicy>>>,
};

export type SitePluginPluginOptionsEnvBranch_DeployFilterInput = {
  policy?: Maybe<SitePluginPluginOptionsEnvBranch_DeployPolicyFilterListInput>,
};

export type SitePluginPluginOptionsEnvBranch_DeployPolicy = {
  userAgent?: Maybe<Scalars['String']>,
  disallow?: Maybe<Array<Maybe<Scalars['String']>>>,
};

export type SitePluginPluginOptionsEnvBranch_DeployPolicyFilterInput = {
  userAgent?: Maybe<StringQueryOperatorInput>,
  disallow?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsEnvBranch_DeployPolicyFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsEnvBranch_DeployPolicyFilterInput>,
};

export type SitePluginPluginOptionsEnvDeploy_Preview = {
  policy?: Maybe<Array<Maybe<SitePluginPluginOptionsEnvDeploy_PreviewPolicy>>>,
};

export type SitePluginPluginOptionsEnvDeploy_PreviewFilterInput = {
  policy?: Maybe<SitePluginPluginOptionsEnvDeploy_PreviewPolicyFilterListInput>,
};

export type SitePluginPluginOptionsEnvDeploy_PreviewPolicy = {
  userAgent?: Maybe<Scalars['String']>,
  disallow?: Maybe<Array<Maybe<Scalars['String']>>>,
};

export type SitePluginPluginOptionsEnvDeploy_PreviewPolicyFilterInput = {
  userAgent?: Maybe<StringQueryOperatorInput>,
  disallow?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsEnvDeploy_PreviewPolicyFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsEnvDeploy_PreviewPolicyFilterInput>,
};

export type SitePluginPluginOptionsEnvFilterInput = {
  production?: Maybe<SitePluginPluginOptionsEnvProductionFilterInput>,
  branch_deploy?: Maybe<SitePluginPluginOptionsEnvBranch_DeployFilterInput>,
  deploy_preview?: Maybe<SitePluginPluginOptionsEnvDeploy_PreviewFilterInput>,
};

export type SitePluginPluginOptionsEnvProduction = {
  policy?: Maybe<Array<Maybe<SitePluginPluginOptionsEnvProductionPolicy>>>,
};

export type SitePluginPluginOptionsEnvProductionFilterInput = {
  policy?: Maybe<SitePluginPluginOptionsEnvProductionPolicyFilterListInput>,
};

export type SitePluginPluginOptionsEnvProductionPolicy = {
  userAgent?: Maybe<Scalars['String']>,
};

export type SitePluginPluginOptionsEnvProductionPolicyFilterInput = {
  userAgent?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsEnvProductionPolicyFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsEnvProductionPolicyFilterInput>,
};

export type SitePluginPluginOptionsFilterInput = {
  plugins?: Maybe<SitePluginPluginOptionsPluginsFilterListInput>,
  path?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  key?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  className?: Maybe<StringQueryOperatorInput>,
  maxWidth?: Maybe<IntQueryOperatorInput>,
  active?: Maybe<BooleanQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  target?: Maybe<StringQueryOperatorInput>,
  rel?: Maybe<StringQueryOperatorInput>,
  modulePath?: Maybe<StringQueryOperatorInput>,
  output?: Maybe<StringQueryOperatorInput>,
  exclude?: Maybe<StringQueryOperatorInput>,
  env?: Maybe<SitePluginPluginOptionsEnvFilterInput>,
  anonymize?: Maybe<BooleanQueryOperatorInput>,
  respectDNT?: Maybe<BooleanQueryOperatorInput>,
  color?: Maybe<StringQueryOperatorInput>,
  showSpinner?: Maybe<BooleanQueryOperatorInput>,
  short_name?: Maybe<StringQueryOperatorInput>,
  start_url?: Maybe<StringQueryOperatorInput>,
  background_color?: Maybe<StringQueryOperatorInput>,
  theme_color?: Maybe<StringQueryOperatorInput>,
  display?: Maybe<StringQueryOperatorInput>,
  icon?: Maybe<StringQueryOperatorInput>,
  analyzerPort?: Maybe<IntQueryOperatorInput>,
  production?: Maybe<BooleanQueryOperatorInput>,
  disable?: Maybe<BooleanQueryOperatorInput>,
  openAnalyzer?: Maybe<BooleanQueryOperatorInput>,
  precachePages?: Maybe<StringQueryOperatorInput>,
  allPageHeaders?: Maybe<StringQueryOperatorInput>,
  pathCheck?: Maybe<BooleanQueryOperatorInput>,
};

export type SitePluginPluginOptionsPlugins = {
  resolve?: Maybe<Scalars['String']>,
  id?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
  pluginOptions?: Maybe<SitePluginPluginOptionsPluginsPluginOptions>,
  browserAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  ssrAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  pluginFilepath?: Maybe<Scalars['String']>,
};

export type SitePluginPluginOptionsPluginsFilterInput = {
  resolve?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
  pluginOptions?: Maybe<SitePluginPluginOptionsPluginsPluginOptionsFilterInput>,
  browserAPIs?: Maybe<StringQueryOperatorInput>,
  ssrAPIs?: Maybe<StringQueryOperatorInput>,
  pluginFilepath?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsPluginsFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsPluginsFilterInput>,
};

export type SitePluginPluginOptionsPluginsPluginOptions = {
  className?: Maybe<Scalars['String']>,
  maxWidth?: Maybe<Scalars['Int']>,
  active?: Maybe<Scalars['Boolean']>,
  size?: Maybe<Scalars['Int']>,
  target?: Maybe<Scalars['String']>,
  rel?: Maybe<Scalars['String']>,
};

export type SitePluginPluginOptionsPluginsPluginOptionsFilterInput = {
  className?: Maybe<StringQueryOperatorInput>,
  maxWidth?: Maybe<IntQueryOperatorInput>,
  active?: Maybe<BooleanQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  target?: Maybe<StringQueryOperatorInput>,
  rel?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginSortInput = {
  fields?: Maybe<Array<Maybe<SitePluginFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SiteSiteMetadata = {
  lang?: Maybe<Scalars['String']>,
  title?: Maybe<Scalars['String']>,
  defaultTitle?: Maybe<Scalars['String']>,
  titleTemplate?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  image?: Maybe<Scalars['String']>,
  facebookAppId?: Maybe<Scalars['String']>,
  twitterUsername?: Maybe<Scalars['String']>,
  siteUrl?: Maybe<Scalars['String']>,
  socialLinks?: Maybe<Array<Maybe<SiteSiteMetadataSocialLinks>>>,
  headerMenuItems?: Maybe<Array<Maybe<SiteSiteMetadataHeaderMenuItems>>>,
};

export type SiteSiteMetadataFilterInput = {
  lang?: Maybe<StringQueryOperatorInput>,
  title?: Maybe<StringQueryOperatorInput>,
  defaultTitle?: Maybe<StringQueryOperatorInput>,
  titleTemplate?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  image?: Maybe<StringQueryOperatorInput>,
  facebookAppId?: Maybe<StringQueryOperatorInput>,
  twitterUsername?: Maybe<StringQueryOperatorInput>,
  siteUrl?: Maybe<StringQueryOperatorInput>,
  socialLinks?: Maybe<SiteSiteMetadataSocialLinksFilterListInput>,
  headerMenuItems?: Maybe<SiteSiteMetadataHeaderMenuItemsFilterListInput>,
};

export type SiteSiteMetadataHeaderMenuItems = {
  name?: Maybe<Scalars['String']>,
  path?: Maybe<Scalars['String']>,
};

export type SiteSiteMetadataHeaderMenuItemsFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  path?: Maybe<StringQueryOperatorInput>,
};

export type SiteSiteMetadataHeaderMenuItemsFilterListInput = {
  elemMatch?: Maybe<SiteSiteMetadataHeaderMenuItemsFilterInput>,
};

export type SiteSiteMetadataSocialLinks = {
  name?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  title?: Maybe<Scalars['String']>,
  brandIcon?: Maybe<Scalars['String']>,
};

export type SiteSiteMetadataSocialLinksFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  url?: Maybe<StringQueryOperatorInput>,
  title?: Maybe<StringQueryOperatorInput>,
  brandIcon?: Maybe<StringQueryOperatorInput>,
};

export type SiteSiteMetadataSocialLinksFilterListInput = {
  elemMatch?: Maybe<SiteSiteMetadataSocialLinksFilterInput>,
};

export type SiteSortInput = {
  fields?: Maybe<Array<Maybe<SiteFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SortOrderEnum = 
  'ASC' |
  'DESC';

export type StringQueryOperatorInput = {
  eq?: Maybe<Scalars['String']>,
  ne?: Maybe<Scalars['String']>,
  in?: Maybe<Array<Maybe<Scalars['String']>>>,
  nin?: Maybe<Array<Maybe<Scalars['String']>>>,
  regex?: Maybe<Scalars['String']>,
  glob?: Maybe<Scalars['String']>,
};

export type HeroImageFragment = { childImageSharp: Maybe<{ fluid: Maybe<GatsbyImageSharpFluid_WithWebp_TracedSvgFragment> }> };

export type IndexPageQueryVariables = {};


export type IndexPageQuery = { hero: Maybe<{ childImageSharp: Maybe<{ fluid: Maybe<GatsbyImageSharpFluid_WithWebp_TracedSvgFragment> }> }>, allMarkdownRemark: { edges: Array<{ node: (
        Pick<MarkdownRemark, 'excerpt' | 'id' | 'timeToRead'>
        & { fields: Maybe<Pick<MarkdownRemarkFields, 'slug'>>, frontmatter: Maybe<(
          Pick<MarkdownRemarkFrontmatter, 'title' | 'templateKey' | 'date'>
          & { featuredMedia: Maybe<{ childImageSharp: Maybe<{ fluid: Maybe<GatsbyImageSharpFluid_WithWebpFragment> }> }> }
        )> }
      ) }> } };

export type GatsbyImageSharpFixedFragment = Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpFixed_TracedSvgFragment = Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpFixed_WithWebpFragment = Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpFixed_WithWebp_TracedSvgFragment = Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpFixed_NoBase64Fragment = Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpFixed_WithWebp_NoBase64Fragment = Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpFluidFragment = Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpFluid_TracedSvgFragment = Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpFluid_WithWebpFragment = Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

export type GatsbyImageSharpFluid_WithWebp_TracedSvgFragment = Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

export type GatsbyImageSharpFluid_NoBase64Fragment = Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpFluid_WithWebp_NoBase64Fragment = Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

export type GatsbyImageSharpResolutionsFragment = Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpResolutions_TracedSvgFragment = Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpResolutions_WithWebpFragment = Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpResolutions_WithWebp_TracedSvgFragment = Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpResolutions_NoBase64Fragment = Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet'>;

export type GatsbyImageSharpResolutions_WithWebp_NoBase64Fragment = Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>;

export type GatsbyImageSharpSizesFragment = Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpSizes_TracedSvgFragment = Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpSizes_WithWebpFragment = Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

export type GatsbyImageSharpSizes_WithWebp_TracedSvgFragment = Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;

export type GatsbyImageSharpSizes_NoBase64Fragment = Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>;

export type GatsbyImageSharpSizes_WithWebp_NoBase64Fragment = Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>;
