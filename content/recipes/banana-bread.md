---
templateKey: recipe
title: Banana Bread
featuredMedia: /img/baked-blur-bread-830894.jpg
date: 2018-12-22T13:50:27.445Z
ingredients:
  - name: Plain Flour
    quantity: '360'
    unit: g
  - name: Large Ripe Bananas
    quantity: '2'
instructions: |-
  * Cream butter and sugar
  * Add eggs
  * Fold in flour
  * Fold in bananas
  * Bake for 50 minutes or until blah blah blah
servingSize: '8'
prepTime: 20 minutes
cookTime: 1 hour
tags:
  - banana
  - bread
  - breakfast
---

