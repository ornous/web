---
templateKey: recipe
title: Lemon Drizzle Cake
featuredMedia: /img/sweet-dish-food-produce-breakfast-baking-1075890-pxhere.com.jpg
date: 2018-12-22T14:37:50.050Z
ingredients:
  - name: Lemon
    quantity: '2'
instructions: Bake at 160C
servingSize: '8'
prepTime: 15 minutes
cookTime: 35-40 minutes
tags:
  - Lemon
  - cake
  - summer
---

