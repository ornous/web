---
templateKey: recipe
title: Brownie
featuredMedia: /img/baked-baking-blurred-background-1277202.jpg
date: 2018-12-22T14:07:07.774Z
ingredients:
  - name: Butter
    quantity: '10000'
    unit: g
instructions: Mix it up and bake. = Brownies
servingSize: '1'
prepTime: 20 minutes
cookTime: 40 minutes
tags:
  - brownie
  - chocolate
---

