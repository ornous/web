---
templateKey: recipe
title: Chocolate Chip Cookies
featuredMedia: /img/baked-goods-chocolate-chocolate-chip-cookies-890577.jpg
date: 2018-12-22T14:19:32.514Z
ingredients:
  - name: Flour
    quantity: '200'
    unit: g
  - name: Butter
    quantity: '150'
    unit: g
instructions: >-
  Start with cold butter\

  Cream butter and sugar for up to 10 minutes. There is a lot of butter in this
  recipe so structure is important or it'll spread far too quickly


  Fold in the flour, salt, baking soda\

  etc.
servingSize: '9'
prepTime: 30 minutes
cookTime: 15 minutes
tags:
  - cookies
  - chocolate
  - treat
---

