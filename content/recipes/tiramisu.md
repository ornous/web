---
templateKey: recipe
title: Tiramisu
featuredMedia: /img/cake-chocolate-chocolate-cake-373066.jpg
date: 2018-12-22T14:40:32.291Z
ingredients:
  - name: Lady Fingers
    quantity: '12'
  - name: Mascarpone
    quantity: '250'
    unit: ml
  - name: Double Cream
    quantity: '250'
    unit: ml
  - name: Egg Yolk
    quantity: '1'
instructions: |-
  Mix creams, flavourings\
  Dip savoiardi in espresso\
  Layer in a dish\
  Layer the cream mixture\
  Dust with cocoa powder

  Leave in the fridge to set
servingSize: '4'
prepTime: 20 minutes
cookTime: 1 hour
tags:
  - tiramisu
  - dessert
  - italian
---

