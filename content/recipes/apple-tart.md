---
templateKey: recipe
title: Apple Tart
featuredMedia: /img/tarte-aux-pommes.jpg
date: 2018-12-22T14:23:04.918Z
ingredients:
  - name: Sweet Shortcrust Pastry
    quantity: '1'
  - name: Compote Apples
    quantity: '4'
  - name: Topping Apples
    quantity: '6'
instructions: >-
  Make the dough\

  Make the compote (butter, sugar, apples, lemon juice, etc.)\

  Blind bake\

  Layer of compote, layer of apples\

  top with 1/4" butter cubes (helps the apples cook and brown properly)\

  Bake\

  Blow torch apple tips to get that nice contrast-y look\

  Warm up ~40ml apricot jam, 2tbsp lemon juice and 1 table spoon water to brush
  the tart


  Let cool for an hour or so to get out of that soft state


  \

  Whip out the ice cream tub...


  MANGER!
servingSize: '12'
prepTime: 1 hour
cookTime: 45 minutes
tags:
  - apple
  - tart
  - dessert
---

