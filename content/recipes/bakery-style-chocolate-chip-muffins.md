---
templateKey: recipe
title: Bakery-style Chocolate Chip Muffins
featuredMedia: /img/img_20181130_152331.jpg
date: 2018-12-10T20:40:27.749Z
ingredients:
  - name: Plain Flour
    quantity: '360'
    unit: g
  - name: Granulated Sugar
    quantity: '180'
    unit: g
  - name: Baking Powder
    quantity: '3.5'
    unit: tsp
  - name: Salt
    quantity: '0.5'
    unit: tsp
  - name: Eggs
    quantity: '2'
    unit: unit
  - name: Milk
    quantity: '240'
    unit: ml
  - name: Neutral Oil
    quantity: '100'
    unit: ml
  - name: Chocolate Chips
    quantity: '260'
    unit: g
  - name: Vanilla Extract
    quantity: '1'
    unit: tsp
instructions: |-
  Mix it all up :p



  220 for oven spring 190 for bake
servingSize: 12 Muffins
prepTime: 10 minutes
cookTime: 20 minutes
tags:
  - baking
  - chocolate
  - muffins
  - breakfast
---

