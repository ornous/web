---
templateKey: blog-post
title: Learning new languages for Busy People
draft: false
featured: true
featuredMedia: /img/idiomas.jpeg
date: 2019-05-25T11:48:00.000Z
description: >-
  Italian, Spanish, Vietnamese, Arabic are the languages I am practicing this
  year. This is how I approach it
tags:
  - learning
  - languages
---
I live in London and it is astounding how many people tell me about wanting to learn languages but not being able to because, somehow, it is too late. Presumably because they are too old or don’t have the time to spare?

I don’t have time either so I thought I’d share some tips about how I approach this.

## My journey to loving languages

My family lives in France and hails from Sénégal and we are descendants of the Fulani, a nomadic herding people with Peul as it’s lingua franca. This meant growing up with two cultures and languages, speaking Peul at home and French pretty much everywhere else.

When I was 23, I made a leap and moved to London with the aim to learn English, which I have been doing since.

This has greatly influenced the way I approach language acquisition, specifically, as an adult.

I think of it as a journey, with its ups and downs and I think of it as an integral part of how I experience the world rather than a separate time slot set aside for study.

## Forming the Habit

I’ve failed at getting into the habit several times and noticed that I would get into the same pattern every time. I would be super motivated and start at a grandiose, unsustainable, pace spending hours a day studying and practising only to fall off a few days/weeks later when life got busier and that initial burst of motivation started fading. Getting back into it also felt super difficult since it entailed putting in so much time.

I now spend the first month creating the habit of studying 5 minutes a day, every day and no longer.

At this pace, it would probably take me a couple of decades to achieve my goals yet every session would leave me wanting more. I am also usually impressed with how much I’ve improved by that point, which only serves to bolster up my motivation and stifles any kind of doubt I initially had about my ability to learn.

Once that first month has passed, I am okay missing the odd session here and there, going longer and what not. The habit is there and going back to it isn’t as much of an issue anymore.

## Comprehensible Input

I’ve tried grammar and vocabulary focused learning and didn’t find it effective.

What works for me is exposure to compelling content. (When I started learning English, I watched Scrubs and The IT Crowd and read Harry Potter books.)

The less you understand that piece of content, the more potential there is for learning. On the flip side, it is much more difficult to stay focused and actually understand what’s going on, which is not fun or effective.

Your mileage may vary but the sweet spot for me is about 65% comprehension (including any subtitles).

I take note of words that hinder my comprehension for spaced repetition and move on.

To begin with, for languages that use an alphabet I understand, I use subtitles in the target language, otherwise I’ll just pick French or English. Once my comprehension is high enough without subtitles, I drop them. It’s a fun milestone and I learn much faster once I’ve done this.

To begin with your understanding might be close to 0% though.

What I do here is pick something very small and get through it as many times as it takes to fully understand.

With Italian, I started with the golden rule of reciprocity “Non fare agli altri quello que non vorresti fosse fato a te”.

Picking a subject that I am very familiar with also takes a lot of pressure off of aural comprehension.

## Meaningful and Familiar Content

This is the integration bit and probably why vocab and grammar binging doesn’t work for me. Learning how to say that I am buying plane tickets or that you cannot camp somewhere isn’t really meaningful or familiar to me. Sure, there are benefits to learning to say those things but I’m unlikely to remember that as it’s not really engaging.

My favourite thing to do is to bring those languages to my passions. I love reading, cooking, baking, playing music. Watching a lesson on how to play Asturias (Leyenda) in Spanish, or how to make pasta in Italian is much more likely to keep me engaged and help me remember the language. It also happens to be information that I wouldn’t have been able to get from the horse’s proverbial mouth without my commitment to learning that language and that is incredibly rewarding.

It also means that I do not need to spend extra time on language learning because I am integrating it into my hobbies.

## Spaced Repetition

Although I do not focus on vocabulary, I do want to commit some words to longer term memory and I find spaced repetition to be a great low cost way to do this.

Rather than cram through a list of words out of context, I pick words from the content that I consume and make flash cards out of them.

If you haven’t heard of it, the principle is based on the spacing effect: in order to learn, you must first forget (which sounds like something Mr Myagi would say). The way I understand it, it would be extremely costly for us to just remember everything we are exposed to. Forgetting and re-encoding a piece of information signals the brain that we need to remember this longer term. Also, there’s only so much new information that we can process in a day and trying to push beyond our limits leads to poorer retention rates.

Below is the Lietner System. You study items on the leftmost box every day and review less and less frequently as you move right.

![The Lietner System](/img/leitner_system_alternative.svg.png "The Lietner System")

The process essentially shows you what you get wrong often and what you know less and less frequently.

I’ve been using [Anki](https://apps.ankiweb.net/) for this (although I am currently building my own). 

## Motivation

I’d like to end on a note about motivation. I believe in language learning as a life altering journey.

Regardless of your age and even if you only have 5 minutes a day to spare, I believe that anyone can learn languages. Sure, it’ll take more effort than when you picked up your mother tongue but it’s never too late.

Thanks for sticking with me.

I picked up Arabic and Vietnamese this year and hope to start learning some form of sign language.

What languages are/will you be learning next? Let me know in the comments!
