---
templateKey: blog-post
title: Why I Read 60 Books a Year
draft: true
featured: false
featuredMedia: /img/books-books.jpg
date: 2018-09-14T07:20:00.000Z
description: >-
  3 Years ago, I caught the reading bug. The transformative effects have been so
  powerful that I have read 207 books since. There are some questions that I get
  often but rarely ever feel like I take enough time to do them justice. I'll
  try to address them in this post.
tags:
  - books
  - reading
  - non-fiction
---
## How do you find the time?

The same way I tackle most of my goals. Break it down into tiny steps, then walk.

60 books per year is also: 1 book every ~6 days, 1/6 of a book every day. 1/12 per half day and so on. How many pages in a book will depend on what you read. I think mine's around 600 pages so if I read a 100 pages in a day, I should be good!

I aim to read 40 pages as part of my morning and evening routines, the rest I like to be somewhat spontaneous about. Reading while walking to the station, in the tube, waiting for a friend or listen to an audiobook while baking.

Speaking of Audiobooks, lets get to the next bit.

## You must read really quickly, then?

Actually, not really. Ive always been jealous of those that can sit somewhere for a couple of hours and read a back from cover to cover.

I actually think Im quite slow with a book, especially if I am engaged. I practice speed reading also but generally not if I want to engage deeply with the material.

You said we were gonna talk about Audiobooks?

Ah, yeah! I absolutely love Audiobooks and read about 1:1 paper to audio. I also find that I can listen to Audiobooks at higher speeds and still have decent information retention. I mostly listen at 2X speeds and will go to 3X if I am fully engaged (and using headphones!)

## How do you maximise information retention?

Some people I know write summaries and use highlighters to promote retention. I believe these can help tremendously but if I don"t enjoy something I" probably not going to keep at it for very long.

I find that my ability to recall is mostly determined by how engaged I when experiencing the content. I unapologetically put a book down if I find that I am not fully engaged.

I learn with the aim to teach. And I test myself all the time by jumping back a few steps and seeing if I am gathering insights.

Mainly, though, I don"t see books as parcels of paper from which we must extract as much essence as possible before it starts its dust collecting duties. I like to read slow and multiple times. I get joy from discovering a chapter that I could never really remember reading.

## What do you read?

I probably should have led with the fact that I only read non-fiction! A melting pot of psychology, self-help, technology, business, design, culture. Whatever I can get my hands on. This is on my shelf this month:

* An Ecology of Mind
* Small Arcs of Larger Circles
* Special Relativity and Classical Field Theory (The Theoretical Minimum)
* Speaking, actually
* Introduction to Cybernetics
* Braving the Wilderness
* Why We Sleep
* Value Stream Mapping
* and How to be a Brilliant Mentor.

I love recommendations (even for fiction :p)
