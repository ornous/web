---
templateKey: blog-post
title: 'Kaizen: Continuous Improvement'
draft: false
featured: true
featuredMedia: /img/kaizen-1.svg.png
date: 2019-01-22T18:43:43.825Z
description: >-
  Kaizen, is the Japanese word for "improvement". In business, kaizen refers to
  activities that continuously improve all functions and involve all employees
  from the CEO to the assembly line workers. It also applies to processes, such
  as purchasing and logistics, that cross organisational boundaries into the
  supply chain.
tags:
  - kaizen
  - continuous improvement
  - toyota
---
Kaizen, is the Japanese word for "improvement". In business, kaizen refers to activities that continuously improve all functions and involve all employees from the CEO to the assembly line workers. It also applies to processes, such as purchasing and logistics, that cross organizational boundaries into the supply chain.



Improvement Kata, Kaizen, Toyota
