---
templateKey: blog-post
title: Hello World
draft: false
featured: true
featuredMedia: /img/oz.jpg
date: 2019-04-08T14:10:35.288Z
description: >-
  Why I am blogging, Who I am Blogging For, What I will blog about, Who my
  audience is
tags:
  - introduction
---
## Who Are You?

* Software Engineer with 10+ years professional experience (15 years total).
* I have a fascination for how teams can be highly successful and how to get there.
* Am a rugby player and a martial artist (Judo, Vietnamese Martial Arts).
* I read quite a bit, juggle, play music (guitar, bass and djembe).
* Love cooking, baking and bread making.
* I often throw around words such as Systems Thinking, Resilience, Sustainability, Learning, Continuous Delivery, Testing.

## Why Are You Blogging?

I didn't study computer science in school and my path has been gnarly, learning as I go along. I have had to figure out ways to acquire understanding and figuring out how to execute on problems that have at time felt foreign to me.\
I want to help share systems and tips that I have picked up along the way that help me navigate the complex and fast moving industry that Software Engineering and Web Development specifically.

## What Are You Going To Be Blogging About?

Web Development, Process Improvement, Learning, Continuous Delivery, Container Orchestration, Automation, Architecture and peripheral topics.

## Who Are You Writing For?

* Software Engineers, Avid Learners
* To crystallise my thoughts

## How Can They Get Involved?

Book Recommendations

Work

Comments, Questions and queries

\>> Follow/DM on Social Media

\>>> Use the Contact Form

##
