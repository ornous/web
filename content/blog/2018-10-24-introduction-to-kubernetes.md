---
templateKey: blog-post
title: What I love about Kubernetes
draft: true
featured: false
featuredMedia: /img/boats-cargo-cargo-container-753331.jpg
date: 2018-10-24T15:23:19.513Z
description: Kubernetes deconstructed
tags:
  - kubernetes
  - devops
  - microservices
---
I came across Kubernetes in 2015 as part of my continuous delivery efforts. I was staggered by how relevant container orchestration was.

In this post I'd like to highlight some of the reasons I think it's an amazing project.

Some of the subjects I plan on writing separate posts about:

* Getting started with Kubernetes
* Running Microservices Architectures on Kubernetes
* Running serverless applications on Kubernetes

Kubernetes (k8s) is an open-source system for automating deployment, scaling, and management of containerized applications.

It groups containers that make up an application into logical units for easy management and discovery. Kubernetes builds upon 15 years of experience of running production workloads at Google, combined with best-of-breed ideas and practices from the community.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec dolor ultrices, vehicula nibh nec, fringilla massa. Donec cursus felis ac luctus consequat. Aliquam erat volutpat. Etiam et diam in leo porttitor consectetur ac eu sem. Nunc convallis quam volutpat ante dignissim rhoncus. Donec ut arcu nibh. Duis rutrum rhoncus turpis, eget ullamcorper orci egestas a. Phasellus posuere risus blandit nunc dapibus, dapibus ultrices elit condimentum. Ut mattis dui tellus. Duis vel maximus quam. Mauris vel auctor justo, eget consequat nisi.

In hac habitasse platea dictumst. Cras vulputate non elit et condimentum. Nullam lobortis elit sit amet metus facilisis, quis egestas purus maximus. Maecenas non consectetur mauris. Suspendisse turpis mauris, suscipit ac eleifend quis, rhoncus eget leo. Nam lorem neque, semper in molestie nec, tempus id lorem. Maecenas ipsum tellus, fringilla vel convallis ac, tincidunt id sem. Nullam commodo nibh elit, a elementum leo molestie convallis. In aliquam diam eget nibh sollicitudin, a commodo nunc convallis. Praesent non rhoncus velit. Nulla maximus purus sit amet purus convallis, sed vestibulum ipsum laoreet. Morbi placerat libero non mi pretium finibus. Fusce iaculis, sem eget pulvinar maximus, eros nunc volutpat nisl, non tempus tellus justo vitae ligula. Sed porttitor tristique tempus. Nunc malesuada molestie purus nec ultricies.

Cras euismod sed odio nec molestie. Sed ac justo mollis, congue leo id, molestie nunc. In pretium purus ac venenatis hendrerit. Nam varius eget felis non accumsan. Proin accumsan augue in lectus ornare dignissim vel nec purus. In varius augue vel sapien sagittis vehicula. Aenean arcu justo, iaculis id ipsum non, ultrices auctor dolor. Maecenas at quam pretium, auctor neque ut, maximus elit. Aliquam erat volutpat. Etiam ac pharetra urna. Donec sit amet quam nisl. Proin commodo sit amet felis sed mattis. Cras vel lectus sed elit bibendum commodo.
