---
templateKey: blog-post
title: Getting Started With Microservices
draft: true
featured: false
featuredMedia: /img/builders-building-construction-159306.jpg
date: 2018-10-24T15:39:22.016Z
description: Microservices and aspects
tags:
  - microservices
  - testing
  - continuous delivery
---
The term "Microservice Architecture" has sprung up over the last few years to describe a particular way of designing software applications as suites of independently deployable services. While there is no precise definition of this architectural style, there are certain common characteristics around organization around business capability, automated deployment, intelligence in the endpoints, and decentralized control of languages and data.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec dolor ultrices, vehicula nibh nec, fringilla massa. Donec cursus felis ac luctus consequat. Aliquam erat volutpat. Etiam et diam in leo porttitor consectetur ac eu sem. Nunc convallis quam volutpat ante dignissim rhoncus. Donec ut arcu nibh. Duis rutrum rhoncus turpis, eget ullamcorper orci egestas a. Phasellus posuere risus blandit nunc dapibus, dapibus ultrices elit condimentum. Ut mattis dui tellus. Duis vel maximus quam. Mauris vel auctor justo, eget consequat nisi.

In hac habitasse platea dictumst. Cras vulputate non elit et condimentum. Nullam lobortis elit sit amet metus facilisis, quis egestas purus maximus. Maecenas non consectetur mauris. Suspendisse turpis mauris, suscipit ac eleifend quis, rhoncus eget leo. Nam lorem neque, semper in molestie nec, tempus id lorem. Maecenas ipsum tellus, fringilla vel convallis ac, tincidunt id sem. Nullam commodo nibh elit, a elementum leo molestie convallis. In aliquam diam eget nibh sollicitudin, a commodo nunc convallis. Praesent non rhoncus velit. Nulla maximus purus sit amet purus convallis, sed vestibulum ipsum laoreet. Morbi placerat libero non mi pretium finibus. Fusce iaculis, sem eget pulvinar maximus, eros nunc volutpat nisl, non tempus tellus justo vitae ligula. Sed porttitor tristique tempus. Nunc malesuada molestie purus nec ultricies.

Cras euismod sed odio nec molestie. Sed ac justo mollis, congue leo id, molestie nunc. In pretium purus ac venenatis hendrerit. Nam varius eget felis non accumsan. Proin accumsan augue in lectus ornare dignissim vel nec purus. In varius augue vel sapien sagittis vehicula. Aenean arcu justo, iaculis id ipsum non, ultrices auctor dolor. Maecenas at quam pretium, auctor neque ut, maximus elit. Aliquam erat volutpat. Etiam ac pharetra urna. Donec sit amet quam nisl. Proin commodo sit amet felis sed mattis. Cras vel lectus sed elit bibendum commodo.
