---
templateKey: blog-post
title: How I built this blog
draft: false
featured: false
featuredMedia: /img/codes-coding-programming-270488.jpg
date: 2019-06-24T20:08:51.691Z
description: >-
  How and why I use Gitlab, Netlify, Netlify CMS and Gatsby, Airtable, React and
  GraphQL to build this website and what the roadmap looks like so far.
tags:
  - ''
---
How and why I use Gitlab, Netlify, Netlify CMS and Gatsby, Airtable, React and GraphQL to build this website and what the roadmap looks like so far.
