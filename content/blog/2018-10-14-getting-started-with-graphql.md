---
templateKey: blog-post
title: Getting Started With GraphQL
draft: false
featured: false
featuredMedia: /img/arrows-box-business-533189.jpg
date: 2018-10-14T12:52:14.498Z
description: 'Intro to GraphQL: A Query Language for APIs'
tags:
  - graphql
  - rest
---
GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools.

# The Big Heading
## A Query Language for your APIs

## Rest Refresher

### Resources

Entities served by the API

### Verbs

Mapping, HTTP...

PUT: Replace

GET: Retrieve a collection or entity

etc...

### Hateoas

Hypermedia As The Engine Of Application State

## Richardson's Maturity Model for Rest APIs

![Level 0: The Swamp of POX, Level 1: Resources, Level 2: HTTP Verbs, Level 3: Hypermedia Controls](/img/richardson.png)

## Some GraphQL Code
```javascript:title=GraphQL Query
{
  file(relativePath: { eq: "hero.jpg" }) {
    url
  }
  users {
    id
    name
    age
    posts {
      id
      excerpt
    }
  }
}
```

## Emoji Support
:100: :+1: :confetti_ball: :boom: :fire: :tada:

# Tweet Cards
This is an example of embedding a single tweet card.
Add any markdown as you normally do, and then insert a valid
Tweet link anywhere to automatically transform it into an
embed card.

https://twitter.com/gatsbyjs/status/1055939617646465024

You can embed several tweets

https://twitter.com/wesbos/status/1068597847237541888

https://twitter.com/dan_abramov/status/1068884262273933312

[I'm an inline-style link](https://www.google.com)

## Look, A mermaid
```mermaid
  sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!
```

# Look at this Video:

`youtube:https://www.youtube.com/embed/2Xc9gXyf2G4`
