---
templateKey: blog-post
title: My 5 Favourite Books in 2019
draft: false
featured: false
featuredMedia: /img/blur-bookcase-books-298660.jpg
date: 2019-06-24T19:03:23.160Z
description: >-
  2019 has been my favourite year of reading. In this publication, I talk about
  the books that impacted me most and why.
tags:
  - books
  - reading
  - top 5
---
In the past few years, I mostly read self help and although that's what I needed then, I started feeling as though I was getting diminishing returns from them so I set my goal this year was to broaden my reading leading to my most fascinating year of reading so far. 

Another goal of mine was to inject some diversity in terms of authors, having noticed that most of my read books had been written by the typical cis-gendered white male.

Philosophy Marcus Aurelius' Meditations, The Discourses of Epictetus, Seneca, ...

Memoires Michelle Obama - Becoming, Educated - Tara Westover, Can't Hurt Me - David Goggins

Most importantly, I've read my very first fiction book (yes, at 32 years of age :laugh:)

It was 1984 by George Orwell. Very interesting book, yet It was a pretty novel experience and sort of awkward at times because of how I'm used to processing books.

\[Include graphs]

Without further ado, here's my top 5

## 5. Invisible Women - Caroline Criado Perez

In Book X, Author Y explores Subject Z.

I learned that

...

## 4. The Science of Mindfulness

In Book X, Author Y explores Subject Z.

I learned that

...

## 3. Out of Your Mind

In Book X, Author Y explores Subject Z.

I learned that

...

## 2. Exploring Metaphysics - David Kyle Johnson

In Book X, Author Y explores Subject Z.

I learned that

...

## 1. Range: Why Generalists Triumph in a Specialized World - David Epstein

In Book X, Author Y explores Subject Z.

I learned that

...

Honourable Mentions:

* Spiral Dynamics
* Meditations - Marcus Aurelius
* Can't Hurt Me - David Goggins
* Work Clean
* 

What are you favourite books this year?
