---
templateKey: blog-post
title: Consumer Driven Contracts
draft: true
featured: false
featuredMedia: /img/contractlaw.jpg
date: 2018-09-19T16:12:54.742Z
description: >-
  An approach to creating and evolving APIs that allows for testing
  independently
tags:
  - api
  - testing
  - contracts
  - microservices
  - continuous delivery
---
An approach to creating and evolving APIs that allows for independent delivery lines.

...

This left us with two options that turned out to be unsatisfactory (spoiler alert).

## The integration pipeline

Create an integration job that would run every time any of its upstream pipelines ran successfully, effectively integrating those services and testing the system as a whole.

## The independent pipeline

No integration, the pipeline only cares for that service and we'll use versioning to manage our consumers.

Fortunately, at the time I was reading the Microservices book where Sam Newman presented this problem along with a solution

## Consumer Driven Contracts

Blah Blah Blah

##
