## Tasks

### Clear Blockers
- [ ] Fix PropTypes Warnings
- [ ] Fix Cypress Tests
- [ ] In page blog images not getting resized

### Improvement
- [ ] Migrate to typescript from prop types
- [ ] Graphql codegen for query/data types
- [ ] Implement Architecture Decision Records
- [ ] Add fallbacks for css custom variables
- [ ] CSS Grid Review and improvements (min/max-content, fitcontent, auto-rows...)
- [ ] Create component library with form components
- [ ] Restore Table Of Contents

#### Testing
- [ ] Contact Form
- [ ] Contact Lambda
- [ ] Hamburger menu with react-testing-library

#### Styling
- [ ] contact form
- [ ] mobile menu
- [ ] Logo hover treatment

#### Interaction
- [ ] Ensure opening and closure is done through area-expanded API
- [ ] Screen reader audit
  > ensure correct sections are skipped so content is
  > served rather than navlinks being repeated at the beginning of every page

## Experiments
### frontend
- [ ] contextual overriding of css custom variables
- [ ] Scroll indicators
- [ ] Back to top
- [ ] back to category
- [ ] Breadcrumbs
- [ ] Outline/headings summary (table of contents)

#### UI
- [ ] Play with shapes, page transitions
- [ ] Try shadow colorisation based on extracted image color + bg color
- [ ] Experiment with a mobile tapbar
- [ ] Play with sidebars and animations

#### Testing
- [ ] Test ~a~ menu component using react-testing-library
