module.exports = {
  "root": true,
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2019,
    "ecmaFeatures": {
      "jsx": true,
    },
  },
  "plugins": ["emotion", "jest"],
  "extends": [
    "plugin:jest/recommended",
    "plugin:jest/style",
    "airbnb",
  ],
  "settings": {
    "import/core-modules": ["gatsby"],
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx"]
      }
    }
  },
  "globals": {
    "document": true,
    "graphql": true
  },
  "env": {
    "jest/globals": true
  },
  "rules": {
    "no-unused-expressions": ["error", { "allowTaggedTemplates": true }],
     "import/extensions": [
        "error",
        "ignorePackages",
        {
          "js": "never",
          "jsx": "never",
          "ts": "never",
          "tsx": "never"
        }
     ],
     "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": [
          ".storybook/**",
          "**/*.stories.js",
          "**/*.test.js",
          "src/**/__tests__/*.js",
          "test/**/*.js"
        ]
      }
    ],
    "react/jsx-filename-extension": [
      1,
      { "extensions": [".js", ".jsx", ".tsx"] }
    ],
    "jsx-a11y/anchor-is-valid": [
      "error",
      {
        "components": ["Link"],
        "specialLink": ["to"],
        "aspects": ["noHref", "invalidHref", "preferButton"]
      }
    ],
    "emotion/jsx-import": "error",
    "emotion/no-vanilla": "error",
    "emotion/import-from-emotion": "error",
    "emotion/styled-import": "error",
    "react/prop-types": "off",
    "react/jsx-props-no-spreading": "off", // TODO Re-enable
  },
  "overrides": [
    {
      "files": ["src/**/__tests__/*.js"],
      "rules": {
        "react/jsx-props-no-spreading": "off"
      }
    },
    {
      "files": ["**/*.tsx"],
      "parser": "@typescript-eslint/parser",
      "parserOptions": {
        "sourceType": "module",
        "project": ["./tsconfig.json"],
      },
      "extends": [
        "airbnb-typescript",
        "prettier/@typescript-eslint",
      ],
      "rules": {
        "react/prop-types": "off",
        "@typescript-eslint/member-delimiter-style": [
          "error",
          {
            multiline: {
              delimiter: "none",
            },
          },
        ],
        "@typescript-eslint/explicit-function-return-type": "error",
        "@typescript-eslint/indent": ["error", 2],
        "@typescript-eslint/camelcase": ["error", { "properties": "never" }],
      }
    }
  ]
}
