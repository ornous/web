## Personal Website: www.ornous.com

My personal blog, about me and contact

## Motivation

A place for people to learn about me and reach out.
A platform for me to practice my writing and web development skills

## Code style

This project uses prettier and eslint to maintain the standard js code style -

## Tech/framework used

<b>Built with</b>

-   [React](https://www.reactjs.org/)
-   [Gatsby](https://www.gatsbyjs.org/)
-   [Storybook](https://storybook.js.org/)
-   [Emotion](https://emotion.sh/)
-   [Netlify](https://www.netlify.com/docs/)
-   [Netlify CMS](https://www.netlifycms.org/)
-   [GraphQL (Apollo Client)](https://www.apollographql.com/docs/react/)
-   [Cypress](https://cypress.io/)

## Features

-   Offline-First App
-   Lambda Functions

## Installation

-   `yarn install`
-   `yarn start`

## Structure

-   src/
    -   _components_: Composable and reusable building blocks.
    -   _pages_: Compose components into pages served based on filename.
    -   _templates_: Used for dynamically generated routes.
    -   _lambda_: Lambda Functions.
    -   _themes_: Themes define color schemes, typography and icons

## Tests

-   Unit
    -   Jest
    -   Enzyme
    -   React-testing-library (trialing)
    -   Lamda-tester
-   Integration
    -   Cypress
    -   Jest
-   UAT
    -   Cypress
-   Performance
    -   Sitespeed.io
-   Visual Regression
    -   Percy.io
    -
