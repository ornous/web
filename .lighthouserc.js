{
  "ci": {
    "collect": {
      "numberOfRuns": 3,
      "staticDistDir": "public/",
      "settings": {
        "chromeFlags": "--disable-gpu --no-sandbox"
      },
    },
    "assert": {
      "preset": "lighthouse:recommended",
    },
    "upload": {
      "target": "temporary-public-storage",
    },
  },
}
