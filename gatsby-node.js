const _ = require('lodash')
const path = require('path')
const fs = require('fs')
const { createFilePath } = require('gatsby-source-filesystem')
const { fmImagesToRelative } = require('gatsby-remark-relative-images')

// TODO Extract this function if keeping after transitional phase
const findTemplateByKey = (key) => {
  let templateFile
  for (const ext of ['.js', '.tsx']) {
    templateFile = path.format({
      root: 'src/templates/',
      name: String(key),
      ext
    })

    if (fs.existsSync(templateFile)) {
      return path.resolve(templateFile)
    }
  }

  throw new Error(`Could not find template matching key: ${key}`)
}

exports.createPages = ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  return graphql(`
    {
      allMarkdownRemark(limit: 1000) {
        edges {
          node {
            id
            fields {
              slug
            }
            frontmatter {
              templateKey
            }
          }
        }
      }
    }
    `).then(({
      data: { allMarkdownRemark: { edges: posts } },
      errors,
    }) => {
    if (errors) {
      errors.forEach(e => console.error(e.toString()))
      return Promise.reject(errors)
    }

    posts.forEach(({
      node: {
        id,
        fields: { slug },
        frontmatter: { templateKey },
      },
    }) => {
      try {
        createPage({
          path: slug,
          component: findTemplateByKey(templateKey),
          context: { id },
        })
      } catch(e) {
        reporter.error(`Error creating page: ${slug}`, e)
      }
    })

    // Tag pages:
    let tags = []
    // Iterate through each post, putting all found tags into `tags`
    posts.forEach(edge => {
      if (_.get(edge, `node.frontmatter.tags`)) {
        tags = tags.concat(edge.node.frontmatter.tags)
      }
    })
    // Eliminate duplicate tags
    tags = _.uniq(tags)

    // Make tag pages
    tags.forEach(tag => {
      const tagPath = `/tags/${_.kebabCase(tag)}/`

      createPage({
        path: tagPath,
        component: path.resolve(`src/templates/tags.js`),
        context: {
          tag,
          slug: tagPath,
        },
      })
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  fmImagesToRelative(node)
  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
