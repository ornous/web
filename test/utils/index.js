import glob from 'fast-glob'
import { useStaticQuery } from 'gatsby'

// TODO Great candidate for TS conversion

/**
 * Sets defaults for static queries
 * TODO Improve default data
 * TODO Leverage fixtures
 */
const stubStaticQueries = () => {
  useStaticQuery.mockReturnValue({
    site: {
      siteMetadata: {
        headerMenuItems: [],
        socialLinks: [],
      },
    },
    image: { childImageSharp: { social: { src: '' } } },
  })
}

/**
 * Loads a fixture by name
 * @param string name The name of the file without extension
 * @return The contents of the fixture
 */
const loadFixture = async (name) => {
  const [file] = await glob(`**/__tests__/fixtures/${name}.js`)
  const handle = await import('../../' + file)
  return handle.default
}

export { loadFixture, stubStaticQueries }
