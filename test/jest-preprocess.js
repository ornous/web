const babelOptions = {
  presets: [
    ['@babel/preset-env', {targets: {node: 'current'}}],
    '@babel/preset-typescript',
    'gatsby'
  ],
  plugins: ['emotion', '@babel/plugin-proposal-optional-chaining'],
}

module.exports = require('babel-jest').createTransformer(babelOptions)
