module.exports = {
  moduleDirectories: ['node_modules', 'src', 'test'],
  moduleFileExtensions: ["js", "json", "jsx", "node", "ts", "tsx"],
  roots: ['src'],
  testEnvironment: 'node',
  verbose: false,
  setupFilesAfterEnv: ['<rootDir>/test/setupJest.js'],
  setupFiles: ['<rootDir>/test/loadershim.js'],
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.js',
    '!src/**/*.stories.js',
    '!src/cms/cms.js',
    '!**/__tests__/fixtures/*',
  ],
  testRegex: '/.*(__tests__\\/.*\\.js)$|(.*\\.(test|spec))\\.js$',
  testPathIgnorePatterns: ['node_modules', '.cache', 'fixtures'],
  transform: {
    '^.+\\.(js|ts|tsx)$': '<rootDir>/test/jest-preprocess.js',
  },
  transformIgnorePatterns: ['node_modules/(?!(gatsby)/)'],
  moduleNameMapper: {
    '.+\\.css$': 'identity-obj-proxy',
    '.+\\.(jpg|jpeg|png|gif|svg)$': '<rootDir>/__mocks__/fileMock.js',
  },
  snapshotSerializers: ['enzyme-to-json/serializer', 'jest-emotion'],
  globals: {
    __PATH_PREFIX__: '',
  },
}
