import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faGithub,
  faGitlab,
  faGoodreads,
  faLinkedin,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

library.add(
  faBars,
  faTimes,
  faGithub,
  faGitlab,
  faGoodreads,
  faLinkedin,
  faTwitter
)
