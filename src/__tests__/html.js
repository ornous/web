import React from 'react'
import { shallow } from 'enzyme'
import HtmlWrapper from '../html'

const createWrapper = props => shallow(<HtmlWrapper {...props} />)
const createTestProps = (props = {}) => ({
  data: {},
  ...props,
})

describe('HTML', () => {
  let wrapper
  beforeEach(() => {
    wrapper = createWrapper(createTestProps())
  })
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
