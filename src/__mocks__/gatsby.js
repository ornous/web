const gatsby = jest.requireActual('gatsby')

const useStaticQuery = jest.fn()
useStaticQuery.mockReturnValue({
  site: {
    siteMetadata: {
      headerMenuItems: [],
      socialLinks: [],
    },
  },
  image: { childImageSharp: { social: { src: '' } } },
})

module.exports = {
  ...gatsby,
  graphql: jest.fn(),
  Link: 'a',
  useStaticQuery: useStaticQuery,
}
