import React from 'react'

/* eslint-disable-next-line react/prop-types */
export function FontAwesomeIcon({ icon }) {
  return <i className={`fa ${icon}`} />
}
