import React from 'react'
import { shallow } from 'enzyme'

import BlogPostPreview from '../BlogPostPreview'

describe('BlogPostPreview', () => {
  let wrapper
  const createWrapper = props => shallow(<BlogPostPreview {...props} />)

  const dataBag = {
    title: 'My New Blog Post',
    tags: ['new', 'fresh'],
    description: `
    It's fresh off the press and set to impress. And cliche, too.
    `,
    featuredMedia: {},
  }

  beforeEach(() => {
    // TODO Convert to jest mock and test behaviour
    wrapper = createWrapper({
      widgetFor: () => 'body',
      entry: {
        getIn: ([, field]) => dataBag[field],
      },
    })
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
