import { createMuiTheme } from '@material-ui/core/styles'

import theme from './warm-antique'

const muiTheme = createMuiTheme({
  palette: {
    primary: {
      main: theme.colors.primary,
      contrastText: theme.colors.onPrimary,
    },
    secondary: {
      main: theme.colors.secondary,
      contrastText: theme.colors.onSecondary,
    }
  }
})

export { theme as default, muiTheme }
