import 'typeface-lato'
import 'typeface-montserrat'

const theme = {
  name: 'Warm Antique',

  colors: {
    background: '#ffffff',
    onBackground: '#4e2025',

    primary: '#873e4c',
    onPrimary: '#ffffff',

    secondary: '#e2b091',
    secondaryVariant: '#f7dfd4',
    onSecondary: '#ffffff',

    error: '#b00020',
    onError: '#ffffff',

    experimental: ['#e2b091', '#eabcac', '#ffefd6'],
  },

  // TODO set up default typefaces pre swap
  typefaces: {
    heading: 'Montserrat',
    legibleText: 'Lato',
  },
}

export default theme
