import Airtable from 'airtable'
import Mailgun from 'mailgun-js'
import validationSchema from '../components/ContactForm/validation-schema'

require('dotenv').config()
const {
  AIRTABLE_API_KEY,
  AIRTABLE_BASE_ID,
  MAILGUN_API_KEY,
  MAILGUN_DOMAIN,
} = process.env

exports.handler = async (event, context, callback) => {
  let record
  if (event.httpMethod !== 'POST') {
    return { statusCode: 405, body: 'Method Not Allowed' }
  }

  const { name, email, message } = JSON.parse(event.body)

  try {
    await validationSchema.validate({ name, email, message })
  } catch (e) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        error: {
          name: e.name,
          message: e.message,
          errors: e.errors,
        },
      }),
    }
  }

  const base = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(AIRTABLE_BASE_ID)
  try {
    record = await base('Contact').create({
      Name: name,
      Email: email,
      Message: message,
    })
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error }),
    }
  }

  const mailgun = Mailgun({ apiKey: MAILGUN_API_KEY, domain: MAILGUN_DOMAIN })
  try {
    const result = await mailgun.messages().send({
      from: 'Ousmane Ndiaye <no-reply@ornous.com>',
      to: email,
      subject: 'Thank you for reaching out',
      text: "I'll get back to you asap!",
    })
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({ error }),
    }
  }

  return {
    statusCode: 200,
    body: JSON.stringify({ recordId: record.get('id') }),
  }
}
