import React from 'react'
import { render } from 'enzyme'

import { stubStaticQueries } from '../../../test/utils'
import BooksPage from '../books'
import BooksData from './fixtures/books'

const createTestProps = (props = {}) => ({
  data: BooksData,
  location: { pathname: '/books' },
  ...props,
})

describe('BooksPage', () => {
  beforeEach(stubStaticQueries)

  it('renders correctly', () => {
    const tree = render(<BooksPage {...createTestProps()} />)

    expect(tree).toMatchSnapshot()
  })
})
