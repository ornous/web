import React from 'react'
import { shallow } from 'enzyme'
import BlogIndex from '../blog'
import blogData from './fixtures/blog'

const createTestProps = (props = {}) => ({
  data: blogData,
  location: { pathname: '/blog' },
  ...props,
})

describe('BlogIndex', () => {
  it('renders correctly', () => {
    const tree = shallow(<BlogIndex {...createTestProps()} />)

    expect(tree).toMatchSnapshot()
  })
})
