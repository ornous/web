const homePageData = {
  hero: {
    childImageSharp: {
      fluid: {
        aspectRatio: 1,
        sizes: '',
        src: '',
        srcSet: '',
      },
    },
  },
  allMarkdownRemark: {
    edges: [
      {
        node: {
          id: '1',
          excerpt: 'Some Text',
          timeToRead: 1,
          frontmatter: {
            date: '18-01-2003',
            title: 'Blog Post',
            featuredMedia: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 1,
                  sizes: '',
                  src: '',
                  srcSet: '',
                },
              },
            },
          },
          fields: { slug: 'blog-post' },
        },
      },
    ],
  },
}

export default homePageData
