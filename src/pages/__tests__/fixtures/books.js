const booksPageData = {
  hero: {
    childImageSharp: {
      fluid: {
        aspectRatio: 1,
        sizes: '',
        src: '',
        srcSet: '',
      },
    },
  },
  allGoodreadsShelf: {
    edges: [
      {
        node: {
          id: '5f194581-079b-5b33-8576-558a37389c60f',
          name: 'read',
          reviews: [
            {
              dateUpdated: '20/12/2019',
              book: {
                id: '',
                isbn: '',
                title: '',
                imageUrl: 'test.jpg',
                authors: [{ name: 'Ozzy' }],
              },
            },
          ],
        },
      },
      {
        node: {
          id: '5f194581-079b-5b33-8576-558a37389c60f',
          name: 'to-read',
          reviews: [
            {
              dateUpdated: '20/12/2019',
              book: {
                id: '',
                isbn: '',
                title: '',
                imageUrl: 'test.jpg',
                authors: [{ name: 'Ozzy' }],
              },
            },
          ],
        },
      },
      {
        node: {
          id: '5f194581-079b-5b33-8576-558a37389c60f',
          name: 'currently-reading',
          reviews: [
            {
              dateUpdated: '20/12/2019',
              book: {
                id: '',
                isbn: '',
                title: '',
                imageUrl: 'test.jpg',
                authors: [{ name: 'Ozzy' }],
              },
            },
          ],
        },
      },
    ],
  },
}

export default booksPageData
