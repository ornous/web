const recipesPageData = {
  site: {
    siteMetadata: {
      title: '',
    },
  },
  hero: {
    childImageSharp: {
      fluid: {
        src: '',
        srcSet: '',
        aspectRatio: 1,
        sizes: '',
      },
    },
  },
  allMarkdownRemark: {
    tags: [
      {
        fieldValue: 'front-end',
        totalCount: 1,
      },
    ],
    recipes: [
      {
        node: {
          id: '1',
          frontmatter: {
            title: 'Delicious Recipe',
            featuredMedia: {
              childImageSharp: {
                fluid: {
                  src: '',
                  srcSet: '',
                  aspectRatio: 1,
                  sizes: '',
                },
              },
            },
          },
          fields: { slug: 'recipes' },
        },
      },
    ],
  },
}

export default recipesPageData
