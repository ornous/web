const blogPageData = {
  site: {
    siteMetadata: {
      title: '',
    },
  },
  hero: {
    childImageSharp: {
      fluid: {
        src: '',
        sizes: {
          aspectRatio: 1,
        },
      },
    },
  },
  allMarkdownRemark: {
    tags: [
      {
        fieldValue: 'things',
        totalCount: 42,
      },
    ],
    posts: [
      {
        node: {
          id: '1',
          timeToRead: 2,
          excerpt: 'Some Text',
          frontmatter: {
            date: '18-01-2018',
            title: 'Blog Post',
            featuredMedia: {
              childImageSharp: {
                fluid: {
                  sizes: {
                    aspectRatio: 1,
                  },
                },
              },
            },
          },
          fields: { slug: 'blog-post' },
        },
      },
    ],
  },
}

export default blogPageData
