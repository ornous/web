import React from 'react'
import { render } from 'enzyme'

import { stubStaticQueries } from '../../../test/utils'
import ContactPage from '../contact'

describe('ContactPage', () => {
  beforeEach(stubStaticQueries)

  it('renders correctly', () => {
    const tree = render(<ContactPage />)

    expect(tree).toMatchSnapshot()
  })
})
