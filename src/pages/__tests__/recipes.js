import React from 'react'
import { render } from 'enzyme'

import { stubStaticQueries } from '../../../test/utils'
import RecipesIndex from '../recipes'
import recipesData from './fixtures/recipes'

const createTestProps = (props = {}) => ({
  data: recipesData,
  location: { pathname: '/recipes' },
  ...props,
})

describe('RecipesIndex', () => {
  beforeEach(stubStaticQueries)

  it('renders correctly', () => {
    const tree = render(<RecipesIndex {...createTestProps()} />)

    expect(tree).toMatchSnapshot()
  })
})
