import React from 'react'
import { render } from 'enzyme'

import HomePage from '../index'
import { loadFixture } from '../../../test/utils'

describe('HomePage', () => {
  let defaultProps

  beforeEach(async () => {
    const homeData = await loadFixture('home');

    defaultProps = {
      data: homeData,
      location: { pathname: '/' },
    }
  })

  it('renders correctly', async () => {
    const tree = render(<HomePage {...defaultProps} />)
    expect(tree).toMatchSnapshot()
  })
})
