import React from 'react'
import { shallow } from 'enzyme'
import NotFoundPage from '../404'

describe('NotFoundPage', () => {
  it('renders correctly', () => {
    const tree = shallow(<NotFoundPage />)

    expect(tree).toMatchSnapshot()
  })
})
