import React, { FC } from 'react'
import { graphql } from 'gatsby'
import { FluidObject } from 'gatsby-image'

import { IndexPageQuery } from '../../graphql-types'
import Layout from '../components/Layout'
import Content from '../components/Content'
import Hero from '../components/Hero'
import BlogRoll from '../components/blog/Roll'

interface IndexProps {
  data: IndexPageQuery
  location: Location
}

const IndexPage: FC<IndexProps> = ({
  data: {
    allMarkdownRemark: {
      edges: featuredPosts,
    },
    hero: heroImage
  },
}) => {
  // TODO Sort out the copy below :dagger: :hankey:
  const hero = (
    <Hero
      alt="A desk in an office"
      heading="Ousmane Ndiaye"
      subHeading="Software Engineering with a Passion for Learning"
      imageData={heroImage?.childImageSharp?.fluid as FluidObject}
    />
  )

  return (
    <Layout hero={hero}>
      <Content>
        <BlogRoll heading="Featured Articles" posts={featuredPosts} />
      </Content>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPage {
    hero: file(relativePath: { eq: "hero-mac.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1400, maxHeight: 450) {
          ...GatsbyImageSharpFluid_withWebp_tracedSVG
        }
      }
    }
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: {
        frontmatter: {
          templateKey: { eq: "blog-post" }
          draft: { eq: false }
          featured: { eq: true }
        }
      }
    ) {
      edges {
        node {
          excerpt(pruneLength: 144)
          id
          timeToRead
          fields {
            slug
          }
          frontmatter {
            title
            featuredMedia {
              childImageSharp {
                fluid(maxWidth: 500, maxHeight: 300) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            templateKey
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`
