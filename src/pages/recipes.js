import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import styled from '@emotion/styled'

import SEO from '../components/SEO'
import Tag from '../components/Tag'
import Hero from '../components/Hero'
import Teaser from '../components/recipes/Teaser'
import Layout from '../components/Layout'
import Content from '../components/Content'

const RecipeList = styled.div`
  display: grid;
  grid-gap: 0 3vw;
  align-items: center;
  justify-content: space-around;
  margin: 0 auto;
  grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));

  /*
    @media (min-width: 555px) {
    padding: 0 2rem;
  }
  */
`

const TagList = styled.ul`
  display: flex;
  list-style-type: none;
  padding: 0;
  font-size: 1.2rem;
  font-family: Lato;
  font-weight: 500;

  ${Tag} {
    padding: 1.2 1.4em;
    border: none;
    margin-right: 0.85em;
  }
`

const RecipesPage = ({
  data: {
    allMarkdownRemark: { recipes },
    hero: {
      childImageSharp: {
        fluid: heroImageData,
      }
    },
  },
}) => {
  const tagList = (
    <TagList>
      <Tag>
        <a href="recipes/baking">Baking</a>
      </Tag>
      <Tag>
        <a href="recipes/dinner">Dinner</a>
      </Tag>
      <Tag>
        <a href="recipes/lunch">Lunch</a>
      </Tag>
    </TagList>
  )

  const hero = (
    <Hero
      heading="Ozzy's Recipes"
      alt="A pancake stack"
      subHeading={tagList}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <SEO title="Ozzy's recipes" />
      <Content>
        <RecipeList>
          {recipes.map(({ node: { id, frontmatter, fields } }) => (
            <Teaser
              key={id}
              id={id}
              frontmatter={frontmatter}
              fields={fields}
            />
          ))}
        </RecipeList>
      </Content>
    </Layout>
  )
}

export default RecipesPage

RecipesPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      posts: PropTypes.arrayOf(PropTypes.shape({}).isRequired),
    }).isRequired,
    hero: PropTypes.shape({}).isRequired,
  }).isRequired,
}

export const recipesQuery = graphql`
  query Recipes {
    hero: file(
      relativePath: { eq: "close-up-delicious-depth-of-field-1503320.jpg" }
    ) {
      ...HeroImage
    }
    allMarkdownRemark(
      limit: 100
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: { frontmatter: { templateKey: { eq: "recipe" } } }
    ) {
      recipes: edges {
        ...RecipeTeaser
      }
    }
  }
`
