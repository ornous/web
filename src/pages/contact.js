import React from 'react'
import styled from '@emotion/styled'

import Layout from '../components/Layout'
import BaseContent from '../components/Content'
import ContactForm from '../components/ContactForm'
import { RevealModal } from '../components/Modal'

const Content = styled(BaseContent)`
  & h2 {
    padding-left: 2rem;
  }
`

const ContactPage = () => (
  <Layout>
    <Content>
      <RevealModal ctaLabel="Contact Me">
        <h2>Get in Touch</h2>
        <ContactForm />
      </RevealModal>
    </Content>
  </Layout>
)

export default ContactPage
