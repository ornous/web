import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import styled from '@emotion/styled'

import SEO from '../components/SEO'
import Tag from '../components/Tag'
import Hero from '../components/Hero'
import Layout from '../components/Layout'
import Content from '../components/Content'
import PostPreview from '../components/blog/PostPreview'
import PostList from '../components/blog/PostList'

const TagList = styled.ul`
  display: flex;
  list-style-type: none;
  padding: 0;
  font-size: 1.2rem;
  font-family: Lato;
  font-weight: 500;

  ${Tag} {
    padding: 1.2 1.4em;
    border: none;
    margin-right: 0.85em;
  }
`

const BlogIndexPage = ({
  data: {
    allMarkdownRemark: { posts },
    hero: {
      childImageSharp: {
        fluid: heroImageData,
      },
    },
  },
}) => {
  const tagList = (
    <TagList>
      <Tag>
        <a href="frontend">Frontend</a>
      </Tag>
      <Tag>
        <a href="backend">Backend</a>
      </Tag>
      <Tag>
        <a href="learning">Learning</a>
      </Tag>
    </TagList>
  )

  const hero = (
    <Hero
      heading="Latest Articles"
      alt="A book with sun shining through its fanned out pages"
      subHeading={tagList}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <SEO title='Blog Home' />
      <Content>
        <PostList>
          {posts.map(({ node }) => (
            <PostPreview key={node.id} {...node} />
          ))}
        </PostList>
      </Content>
    </Layout>
  )
}

BlogIndexPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      posts: PropTypes.arrayOf(PropTypes.shape({}).isRequired),
    }).isRequired,
    hero: PropTypes.shape({}).isRequired,
  }).isRequired,
}

export default BlogIndexPage

export const blogIndexQuery = graphql`
  query BlogIndexQuery {
    hero: file(relativePath: { eq: "books-books.jpg" }) {
      ...HeroImage
    }
    allMarkdownRemark(
      limit: 1000
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: {
        frontmatter: { templateKey: { eq: "blog-post" }, draft: { eq: false } }
      }
    ) {
      posts: edges {
        node {
          ...PostPreview
        }
      }
    }
  }
`
