import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import Layout from '../components/Layout'
import Content from '../components/Content'
import Hero from '../components/Hero'
import BookShelf from '../components/BookShelf'

const BooksPage = ({
  data: {
    hero: {
      childImageSharp: { fluid: heroImageData },
    },
    allGoodreadsShelf: { edges },
  },
}) => {
  const shelves = edges.map(({ node }) => node)
  const shelfByName = desiredName => ({ name }) => name === desiredName

  const read = shelves.find(shelfByName('read'))
  const currentlyReading = shelves.find(shelfByName('currently-reading'))

  const currentYear = new Date().getFullYear()
  const readThisYear = read.reviews.filter(({ dateUpdated }) =>
    dateUpdated.includes(currentYear)
  )
  const subHeading = `
    ${currentYear} Reading Challenge (${readThisYear.length}/60)
  `

  const hero = (
    <Hero
      heading="What's on my bookshelf"
      alt="A pile of books"
      subHeading={subHeading}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <Content>
        <BookShelf
          name="Currently Reading"
          reviews={currentlyReading.reviews}
        />
        <BookShelf
          name="Read"
          reviews={readThisYear}
          />
      </Content>
    </Layout>
  )
}

BooksPage.propTypes = {
  data: PropTypes.shape({
    allGoodreadsShelf: PropTypes.shape({
      edges: PropTypes.array.isRequired,
    }).isRequired,
  }).isRequired,
}

export default BooksPage

export const pageQuery = graphql`
  query BooksQuery {
    hero: file(relativePath: { eq: "may-books.jpg" }) {
      ...HeroImage
    }
    allGoodreadsShelf(
      limit: 3
      filter: { name: { in: ["currently-reading", "read"] } }
      sort: { fields: name, order: ASC }
    ) {
      edges {
        node {
          id
          name
          reviews {
            ...Book
          }
        }
      }
    }
  }
`

