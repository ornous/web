import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import SEO from '../components/SEO'
import Hero from '../components/Hero'
import Layout from '../components/Layout'
import Content, { HTMLContent } from '../components/Content'

export const ProjectTemplate = ({
  content,
  contentComponent,
}) => {
  const PostContent = contentComponent

  return (
    <Content>
      <PostContent content={content} />
    </Content>
  )
}

ProjectTemplate.propTypes = {
  content: PropTypes.node.isRequired,
  contentComponent: PropTypes.func.isRequired,
}

const Project = ({
  data: {
    project: {
      html,
      fields: { slug },
      frontmatter: {
        title,
        description,
        featuredMedia,
      },
    },
  },
}) => {
  const heroImageData = featuredMedia.childImageSharp.fluid
  const hero = (
    <Hero
      heading={title}
      subHeading={description}
      alt=""
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <ProjectTemplate
        content={html}
        contentComponent={HTMLContent}
        featuredMedia={featuredMedia}
      />
      <SEO
        type="article"
        pathname={slug}
        description={description}
        image={featuredMedia.childImageSharp.social.src}
        title={title}
      />
    </Layout>
  )
}

Project.propTypes = {
  data: PropTypes.shape({
    project: PropTypes.shape({}).isRequired,
  }).isRequired,
}

export default Project

export const pageQuery = graphql`
  query ProjectsByID($id: String!) {
    project: markdownRemark(id: { eq: $id }) {
      html
      fields {
        slug
      }
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        featuredMedia {
          ...HeroImage
          ...SeoImage
        }
        description
      }
    }
  }
`
