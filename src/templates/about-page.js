import React from 'react'
import { graphql } from 'gatsby'
import styled from '@emotion/styled'

import { HTMLContent } from '../components/Content'
import ContactForm from '../components/ContactForm'
import Layout from '../components/Layout'
import { RevealModal } from '../components/Modal'
import Hero from '../components/Hero'
import SEO from '../components/SEO'

const Content = styled.article`
  padding: 0 3vw;
  max-width: 80ch;
  margin: 0 auto;
  letter-spacing: 0.028em;

  h1 {
    line-height: 1.4;
    margin-bottom: 0;
  }

  p {
    line-height: 1.55;
  }
`

export const AboutPageTemplate = ({
  content,
}) => {
  return (
    <Content>
      <HTMLContent content={content} />
    </Content>
  )
}

const AboutPage = ({
  data: {
    markdownRemark: pageData,
  },
}) => {

  const {
    html,
    fields: {
      slug,
    },
    frontmatter: {
      title,
      subtitle,
      description,
      hero: {
        childImageSharp: {
          fluid: heroImageData,
          social: {
            src: seoImageUrl,
          },
        },
      },
    },
  } = pageData

  const hero = (
    <Hero
      heading={title}
      alt="Ozzy in front of the Atomium, in Brussels"
      subHeading={subtitle}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <SEO
        type="article"
        pathname={slug}
        description={description}
        image={seoImageUrl}
        title={`${title} - ${subtitle}`}
      />
      <AboutPageTemplate content={html} />
      <RevealModal ctaLabel="Contact Me">
        <ContactForm />
      </RevealModal>
    </Layout>
  )
}

export default AboutPage

export const aboutPageQuery = graphql`
  query AboutPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        subtitle
        description
        hero {
          ...HeroImage
          ...SeoImage
        }
      }
    }
  }
`
