import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'
import { graphql } from 'gatsby'

import SEO from '../components/SEO'
import Hero from '../components/Hero'
import Layout from '../components/Layout'
import Content, { HTMLContent } from '../components/Content'

const Article = styled.article`
  padding: 0 3vw;
  width: 80ch;
  max-width: 90vw;
  margin: 0 auto;

  h1 {
    line-height: 1.4;
    margin-bottom: 0;
  }

  p {
    line-height: 1.5;
  }

  .gatsby-resp-image-wrapper {
    z-index: 0 !important;
  }
`

export const RecipeTemplate = ({
  contentComponent: PostContent,
  ingredients,
  instructions,
  servingSize,
  prepTime,
  cookTime,
}) => (
  <Article>
    <Content>
      <p>{`Yields: ${servingSize}`}</p>
      <p>{`Prep Time: ${prepTime}`}</p>
      <p>{`Cook Time: ${cookTime}`}</p>
      <h3>Ingredients</h3>
      <ul>
        {ingredients.map(({ name, quantity, unit }) => (
          <li key={name}>{`${quantity}${unit || ''} ${name}`}</li>
        ))}
      </ul>
      <h3>Method</h3>
      <PostContent content={instructions} />
    </Content>
  </Article>
)

RecipeTemplate.propTypes = {
  servingSize: PropTypes.string.isRequired,
  cookTime: PropTypes.string.isRequired,
  prepTime: PropTypes.string.isRequired,
  ingredients: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      quantity: PropTypes.number.isRequired,
      unit: PropTypes.string.isRequired,
    })
  ).isRequired,
  instructions: PropTypes.string.isRequired,
  contentComponent: PropTypes.func,
}

RecipeTemplate.defaultProps = {
  contentComponent: null,
}

const Recipe = ({ data }) => {
  const { markdownRemark: post } = data
  const heroImageData = post.frontmatter.featuredMedia.childImageSharp.fluid

  const hero = (
    <Hero
      alt={post.frontmatter.featuredMediaAlt}
      heading={post.frontmatter.title}
      subHeading={post.frontmatter.description}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <RecipeTemplate
        contentComponent={HTMLContent}
        description={post.frontmatter.description}
        ingredients={post.frontmatter.ingredients}
        instructions={post.frontmatter.instructions}
        servingSize={post.frontmatter.servingSize}
        prepTime={post.frontmatter.prepTime}
        cookTime={post.frontmatter.cookTime}
        featuredMedia={post.frontmatter.featuredMedia}
        tags={post.frontmatter.tags}
        title={post.frontmatter.title}
      />
      <SEO
        type="recipe"
        pathname={post.fields.slug}
        description={post.frontmatter.description}
        image={post.frontmatter.featuredMedia.childImageSharp.social.src}
        title={post.frontmatter.title}
      />
    </Layout>
  )
}

Recipe.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
  }).isRequired,
}

export default Recipe

export const pageQuery = graphql`
  query recipeByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      fields {
        slug
      }
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        servingSize
        prepTime
        cookTime
        instructions
        ingredients {
          name
          quantity
          unit
        }
        featuredMedia {
          ...HeroImage
          ...SeoImage
        }
        description
        tags
      }
    }
  }
`
