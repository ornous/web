import React from 'react'
import { graphql } from 'gatsby'

import Content, { HTMLContent } from '../components/Content'
import ProjectPreview from '../components/projects/Preview'
import ProjectList from '../components/blog/PostList'
import Layout from '../components/Layout'
import Hero from '../components/Hero'
import SEO from '../components/SEO'

const ProjectsPage = ({
  data: {
    markdownRemark: pageData,
    allMarkdownRemark: {
      projects,
    },
  },
}) => {
  const {
    html,
    fields: {
      slug,
    },
    frontmatter: {
      title,
      subtitle,
      description,
      hero: {
        childImageSharp: {
          fluid: heroImageData,
          social: {
            src: seoImageUrl,
          },
        },
      },
    },
  } = pageData

  const hero = (
    <Hero
      heading={title}
      alt="Ozzy in front of the Atomium, in Brussels"
      subHeading={subtitle}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <SEO
        type="article"
        pathname={slug}
        description={description}
        image={seoImageUrl}
        title={`${title} - ${subtitle}`}
      />
      <Content legible>
        <HTMLContent content={html} />
      </Content>
      <Content legible>
        <ProjectList>
          {projects.map(({ node }) => (
            <ProjectPreview key={node.id} {...node} />
          ))}
        </ProjectList>
      </Content>
    </Layout>
  )
}

export default ProjectsPage

export const aboutPageQuery = graphql`
  query ProjectsPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        subtitle
        description
        hero {
          ...HeroImage
          ...SeoImage
        }
      }
    }
    allMarkdownRemark(
      limit: 100
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: {
        frontmatter: { templateKey: { eq: "project" } }
      }
    ) {
      projects: edges {
        node {
          ...ProjectPreview
        }
      }
    }
  }
`
