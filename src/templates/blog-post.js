import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'
import { graphql, Link } from 'gatsby'

import SEO from '../components/SEO'
import Hero from '../components/Hero'
import Layout from '../components/Layout'
import BlogRoll from '../components/blog/Roll'
import Content, { HTMLContent } from '../components/Content'

const Article = styled.article`
  padding: 0 3vw;
  width: 80ch;
  max-width: 90vw;
  margin: 0 auto;
  letter-spacing: 0.028em;

  h1 {
    line-height: 1.4;
    margin-bottom: 0;
  }

  p {
    line-height: 1.55;
  }

  .gatsby-resp-image-wrapper {
    z-index: 0 !important;
  }
`

const Breadcrumbs = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: row;
  row-gap: 1rem;

  & li {
    list-style: none;
    margin-left: 0.2rem;
    text-transform: lowercase;

    &:before {
      content: '» ';
    }

    &:first-of-type:before {
      content: '';
    }
  }
`

export const BlogPostTemplate = ({
  content,
  tableOfContents,
  contentComponent,
  title,
}) => {
  const PostContent = contentComponent

  return (
    <Article>
      <Breadcrumbs>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/blog">Blog</Link>
        </li>
        <li>{title}</li>
      </Breadcrumbs>
      {tableOfContents && (
        <>
          <h3>Sections</h3>
          <PostContent content={tableOfContents} />
        </>
      )}
      <PostContent content={content} legible />
    </Article>
  )
}

BlogPostTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired,
  contentComponent: PropTypes.func.isRequired,
  tableOfContents: PropTypes.string.isRequired,
  featuredMedia: PropTypes.shape({}).isRequired,
}

const BlogPost = ({
  data: {
    post: {
      html,
      tableOfContents,
      fields: { slug },
      frontmatter: {
        title,
        description,
        featuredMedia,
        featuredMediaAlt
      },
    },
    similarArticles,
  },
}) => {
  const heroImageData = featuredMedia.childImageSharp.fluid
  const hero = (
    <Hero
      heading={title}
      subHeading={description}
      alt={featuredMediaAlt}
      imageData={heroImageData}
    />
  )

  return (
    <Layout hero={hero}>
      <BlogPostTemplate
        content={html}
        tableOfContents={tableOfContents}
        contentComponent={HTMLContent}
        title={title}
        featuredMedia={featuredMedia}
      />
      <Content>
        <BlogRoll
          heading="Popular Articles"
          justifyHeading="center"
          posts={similarArticles.edges}
        />
      </Content>
      <SEO
        type="article"
        pathname={slug}
        description={description}
        image={featuredMedia.childImageSharp.social.src}
        title={title}
      />
    </Layout>
  )
}

BlogPost.propTypes = {
  data: PropTypes.shape({
    post: PropTypes.shape({}).isRequired,
  }).isRequired,
}

export default BlogPost

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    post: markdownRemark(id: { eq: $id }) {
      html
      tableOfContents
      fields {
        slug
      }
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        featuredMedia {
          ...HeroImage
          ...SeoImage
        }
        description
      }
    }
    similarArticles: allMarkdownRemark(
      limit: 3
      sort: { order: DESC, fields: [frontmatter___date] }
      filter: {
        id: { ne: $id }
        frontmatter: {
          templateKey: { eq: "blog-post" }
          draft: { eq: false }
          featured: { eq: true }
        }
      }
    ) {
      edges {
        node {
          ...PostPreview
        }
      }
    }
  }
`
