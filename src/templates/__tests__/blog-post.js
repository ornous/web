import React from 'react'
import { render } from 'enzyme'

import { BlogPostTemplate } from '../blog-post'
import { HTMLContent } from '../../components/Content'

describe('BlogPostTemplate', () => {
  let wrapper
  const createWrapper = props => render(<BlogPostTemplate {...props} />)

  beforeEach(() => {
    wrapper = createWrapper({
      html: '',
      title: 'Article Name',
      content: 'Blog Post Contents',
      contentComponent: HTMLContent,
      tableOfContents: 'Table of Contents',
      featuredMedia: {
        childImageSharp: {
          fluid: {},
          social: {},
        },
      },
      tags: [1, 1],
    })
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
