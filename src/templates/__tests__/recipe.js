import React from 'react'
import { shallow } from 'enzyme'

import { RecipeTemplate } from '../recipe'
import { HTMLContent } from '../../components/Content'

describe('RecipeTemplate', () => {
  const createWrapper = (props = {}) => shallow(<RecipeTemplate {...props} />)

  it('renders correctly', () => {
    const wrapper = createWrapper({
      contentComponent: HTMLContent,
      servingSize: '5',
      prepTime: '45 minutes',
      cookTime: '15 minutes',
      ingredients: [
        {
          name: 'eggs',
          unit: '',
          quantity: 2,
        },
      ],
      instructions: '<div color="red">Do it now!</div>',
    })
    expect(wrapper).toMatchSnapshot()
  })
})
