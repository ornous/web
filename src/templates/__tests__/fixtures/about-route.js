export default {
  markdownRemark: {
    fields: { slug: 'slug' },
    html: '',
    frontmatter: {
      title: 'title',
      subtitle: 'subtitle',
      description: 'subscription',
      hero: {
        childImageSharp: {
          fluid: {
            src: '',
            sizes: {
              aspectRatio: 1,
            },
          },
          social: {
            src: '',
          },
        },
      },
    },
  },
}
