export default {
  markdownRemark: {
    fields: {
      slug: 'recipe',
    },
    frontmatter: {
      title: 'A scrumptious recipe name',
      instructions: 'Get it done!',
      ingredients: [
        {
          name: 'Flour',
          quantity: 100,
          unit: 'g',
        },
      ],
      servingSize: '15',
      prepTime: '15 minutes',
      cookTime: '25 minutes',
      featuredMediaAlt: 'An appetising description of the photo',
      featuredMedia: {
        childImageSharp: {
          fluid: {
            src: '',
          },
          social: {
            src: 'img.png',
          },
        },
      },
      tags: [],
    },
  },
}
