import React from 'react'
import { shallow } from 'enzyme'

import { AboutPageTemplate } from '../about-page'
import { HTMLContent } from '../../components/Content'

const defaultProps = {
  title: 'title',
  subtitle: 'subtitle',
  content: '<div>content</div>',
  contentComponent: HTMLContent,
}

describe('AboutPage', () => {
  it('renders correctly', () => {
    const tree = shallow(<AboutPageTemplate {...defaultProps} />)

    expect(tree).toMatchSnapshot()
  })
})
