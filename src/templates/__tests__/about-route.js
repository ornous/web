import React from 'react'
import { render } from 'enzyme'

import { stubStaticQueries } from '../../../test/utils'
import AboutPageRoute from '../about-page'
import aboutRouteData from './fixtures/about-route'

describe('AboutPageRoute', () => {
  beforeEach(stubStaticQueries)

  it('renders correctly', () => {
    const props = { data: aboutRouteData }
    const wrapper = render(<AboutPageRoute {...props} />)

    expect(wrapper).toMatchSnapshot()
  })
})
