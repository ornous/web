import React from 'react'
import { shallow } from 'enzyme'

import BlogPostRoute from '../blog-post'

describe('BlogPostRoute', () => {
  let wrapper
  const createWrapper = props => shallow(<BlogPostRoute {...props} />)

  beforeEach(() => {
    wrapper = createWrapper({
      data: {
        post: {
          fields: { slug: 'slug' },
          html: '',
          tableOfContents: '',
          frontmatter: {
            title: 'Article Name',
            featuredMediaAlt: 'Blog Post Featured Media Alt',
            featuredMedia: {
              childImageSharp: {
                fluid: {
                  src: '',
                },
                social: {},
              },
            },
          },
        },
        similarArticles: {
          edges: [],
        },
      },
    })
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
