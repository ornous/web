import React from 'react'
import { render } from 'enzyme'

import RecipeRoute from '../recipe'
import RecipeRouteData from './fixtures/recipe-route'
import { stubStaticQueries } from '../../../test/utils'

describe('RecipeRoute', () => {
  beforeEach(stubStaticQueries)

  it('renders correctly', () => {
    const props = {
      data: RecipeRouteData,
    }
    const wrapper = render(<RecipeRoute {...props} />)

    expect(wrapper).toMatchSnapshot()
  })
})
