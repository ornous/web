import styled from '@emotion/styled'

export default styled.button`
  padding: .1em .5em;
  color: #4285f4;
  background: #e2edff;
  border: 1px solid #bad2fa;
  border-radius: .5em;
  cursor: pointer;
  transition: background .15s ease-out;

  &:hover {
    background: #cbddfb;
  }

  &:focus {
    outline: 0;
    box-shadow: 0 0 4px currentColor;
  }
`
