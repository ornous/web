import React from 'react'
import { Global, css } from '@emotion/core'
import { useTheme } from 'emotion-theming'
import { normalize, darken, lighten } from 'polished'

import '../icons-library'
import mq from '../utils/mq'

const experimentalColors = colors => {
  return colors.experimental.map(
    (color, index) => `--candidate-color-${index}: ${color};`
  )
}

const GlobalStyles = () => {
  const theme = useTheme()

  return (
    <Global
      styles={css`
        ${normalize()}
        :root {
          --primary-color: ${theme.colors.primary};
          --primary-color-variant: ${lighten(0.1, theme.colors.primary)};
          --on-primary-color: ${theme.colors.onPrimary};

          --secondary-color: ${theme.colors.secondary};
          --secondary-color-variant: ${theme.colors.secondaryVariant};

          --on-secondary-color: ${theme.colors.onSecondary};

          --background-color: ${theme.colors.background};

          --on-background-color: ${theme.colors.onBackground};

          --error-color: ${theme.colors.error};
          --on-error-color: ${theme.colors.onError};

          ${experimentalColors(theme.colors)}
        }

        html {
          ${mq({ fontSize: ['100%', '112.5%'] })}
        }

        h1 {
          color: var(--primary-color);
          font-size: 1.4rem;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          font-family: ${theme.typefaces.heading};
        }

        body {
          font-family: ${theme.typefaces.legibleText};
        }

        a {
          color: var(--primary-color);
          text-decoration: none;

          &:hover,
          &:focus {
            outline: 0;
            color: ${darken(0.1, theme.colors.primary)};
          }

          &:visited {
            color: ${lighten(0.1, theme.colors.primary)};
          }
        }

        .remark-code-title {
          margin-bottom: -0.6rem;
          padding: 0.5em 1em;
          font-family: Consolas, 'Andale Mono WT', 'Andale Mono', 'Lucida Console',
            'Lucida Sans Typewriter', 'DejaVu Sans Mono',
            'Bitstream Vera Sans Mono', 'Liberation Mono', 'Nimbus Mono L', Monaco,
            'Courier New', 'Courier', monospace;

          background-color: black;
          color: white;
          z-index: 0;

          border-top-left-radius: 0.3em;
          border-top-right-radius: 0.3em;
        }
      `}
    />
  )
}

export default GlobalStyles
