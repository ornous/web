import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

const Form = ({
  values: { name, email, message },
  errors,
  touched,
  handleChange,
  handleSubmit,
  isValid,
}) => (
  <form onSubmit={handleSubmit}>
    <TextField
      id="name"
      name="name"
      helperText={touched.name ? errors.name : ''}
      error={touched.name && Boolean(errors.name)}
      label="Name*"
      value={name}
      onChange={handleChange}
      fullWidth
    />
    <TextField
      id="email"
      name="email"
      helperText={touched.email ? errors.email : ''}
      error={touched.email && Boolean(errors.email)}
      label="Email*"
      fullWidth
      value={email}
      onChange={handleChange}
    />
    <TextField
      id="message"
      name="message"
      helperText={touched.message ? errors.message : ''}
      error={touched.message && Boolean(errors.message)}
      label="message*"
      value={message}
      multiline
      onChange={handleChange}
      rows={10}
      fullWidth
    />
    <Button
      type="submit"
      fullWidth
      variant="contained"
      disabled={!isValid}
      color="primary"
    >
      Submit
    </Button>
  </form>
)

Form.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
  }).isRequired,
  touched: PropTypes.shape({
    name: PropTypes.bool,
    email: PropTypes.bool,
    message: PropTypes.bool,
  }).isRequired,
  errors: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    message: PropTypes.string,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isValid: PropTypes.bool.isRequired,
}

export default Form
