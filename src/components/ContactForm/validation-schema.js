import * as Yup from 'yup'

const validationSchema = Yup.object({
  name: Yup.string('Enter a name').required('Name is required'),
  email: Yup.string('Enter your email')
    .email('Enter a valid email')
    .required('Email is required'),
  message: Yup.string('Enter your message').required('Message is required'),
})

export default validationSchema
