import React, { useState } from 'react'
import { Formik } from 'formik'
import styled from '@emotion/styled'
import fetch from 'isomorphic-fetch'

import Form from './Form'
import validationSchema from './validation-schema'

const Container = styled.div`
  max-width: 400px;

  & h1 {
    text-align: center;
  }
`

// eslint-disable react/jsx-props-no-spreading
const InputForm = () => {
  const [success, setSuccess] = useState(false)
  return (
    <>
      <Container>
        {(!success && (
          <>
            <h1>Get in Touch</h1>
            <Formik
              validationSchema={validationSchema}
              initialValues={{ name: '', email: '', message: '' }}
              onSubmit={(values, actions) => {
                fetch('/.netlify/functions/contact', {
                  method: 'post',
                  body: JSON.stringify(values),
                }).then(({ ok }) => {
                  actions.setSubmitting(false)
                  setSuccess(ok)
                  // TODO what happens when lambda invocation fails
                })
              }}
          >
            {props => <Form {...props} />}
          </Formik>
          </>
        )) || <div>Thank you for reaching out</div>}
      </Container>
    </>
  )
}

export default InputForm
