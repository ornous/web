import React from 'react'
import { render, fireEvent } from '@testing-library/react'
// import { getByText as domGetByText } from '@testing-library/dom'

import { RevealModal } from '..'

describe('RevealModal', () => {
  beforeEach(() => {
    const modalsPortal = document.createElement('div')
    modalsPortal.setAttribute('id', 'modals-portal')
    document.body.appendChild(modalsPortal)
  })

  it('shows modal content on click', () => {
    const children = ["Tom", "Meena", "Abdoul", "Takako"].join(', ')

    const { getByText } = render(
      <RevealModal ctaLabel="See Children">{children}</RevealModal>
    )

    expect(getByText('See Children')).toBeVisible()
    // (domGetByText(document, children)).toBeNotVisible()

    fireEvent.click(getByText('See Children'))
    expect(getByText(children)).toBeVisible()
  })

  describe('an open modal', () => {
    let wrapper
    const kids = "They are alright"

    beforeEach(() => {
      wrapper = render(
        <RevealModal ctaLabel="How are the kids">{kids}</RevealModal>
      )
      fireEvent.click(wrapper.getByText("How are the kids"))
    })

    it('can be closed', () => {
      const { getByText } = wrapper
      expect(getByText(kids)).toBeVisible()
    })
  })
})
