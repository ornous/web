import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'

import PostPreview from './PostPreview'
import PostList from './PostList'

const Heading = styled.h2`
  text-align: ${props => props.justifyHeading};
`

const BlogRoll = ({ heading, posts, justifyHeading }) => (
  <>
    <Heading justifyHeading={justifyHeading}>{heading}</Heading>
    <PostList>
      {posts.map(({ node: { id, timeToRead, frontmatter, fields } }) => (
        <PostPreview
          key={id}
          id={id}
          timeToRead={timeToRead}
          frontmatter={frontmatter}
          fields={fields}
        />
      ))}
    </PostList>
  </>
)

BlogRoll.propTypes = {
  heading: PropTypes.string.isRequired,
  justifyHeading: PropTypes.oneOf(['left', 'center']),
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      node: PropTypes.shape({
        id: PropTypes.string.isRequired,
      }).isRequired,
    })
  ).isRequired,
}

BlogRoll.defaultProps = {
  justifyHeading: 'left',
}

export { BlogRoll as default, Heading }
