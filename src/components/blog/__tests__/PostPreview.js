import React from 'react'
import { shallow } from 'enzyme'
import merge from 'deepmerge'

import PostPreview from '../PostPreview'

const defaultTestProps = {
  id: '1',
  timeToRead: 1,
  fields: {
    slug: 'how-to-train-your-chihuahua',
  },
  frontmatter: {
    date: '18-01-2018',
    title: 'How to train your chihuahua',
    featuredMedia: {
      childImageSharp: {
        fluid: {
          sizes: '',
          src: '',
          srcSet: '',
          aspectRatio: 1,
        },
      },
    },
  },
}

const createTestProps = (props = {}) => merge(defaultTestProps, props)

describe('PostPreview', () => {
  it('should be defined', () => {
    expect(PostPreview).toBeDefined()
  })

  it('should render correctly', () => {
    const testProps = createTestProps()
    const wrapper = shallow(<PostPreview {...testProps}>Label</PostPreview>)

    expect(wrapper).toMatchSnapshot()
  })
})
