import React from 'react'
import { render, shallow } from 'enzyme'
import { ThemeProvider } from 'emotion-theming'
import merge from 'deepmerge'

import Roll, { Heading } from '../Roll'

const defaultTestProps = {
  heading: 'Blog Posts',
  posts: [
    {
      node: {
        id: '1',
        timeToRead: 1,
        excerpt: `
          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
          praesentium voluptatum deleniti atque corrupti quos dolores et quas mol
        `,
        fields: {
          slug: 'title-based',
        },
        frontmatter: {
          title: 'How to train your chihuahua',
          date: '18-01-2018',
          featuredMedia: {
            childImageSharp: {
              fluid: {
                sizes: '',
                src: '',
                srcSet: '',
                aspectRatio: 1,
              },
            },
          },
        },
      },
    },
    {
      node: {
        id: '2',
        timeToRead: 2,
        excerpt: `
          At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
          praesentium voluptatum deleniti atque corrupti quos dolores et quas mol
        `,
        fields: {
          slug: 'another-title',
        },
        frontmatter: {
          date: '18-01-2018',
          title: 'How to train your parrot',
          featuredMedia: {
            childImageSharp: {
              fluid: {
                sizes: '',
                src: '',
                srcSet: '',
                aspectRatio: 1,
              },
            },
          },
        },
      },
    },
  ],
}

const createTestProps = (props = {}) => merge(defaultTestProps, props)
// const withTheme = (theme) => ({ children }) => (
//   <ThemeProvider theme={theme}>{children}</ThemeProvider>
// )

describe('Roll', () => {
  it('should be defined', () => {
    expect(Roll).toBeDefined()
  })

  it('should render correctly', () => {
    const testProps = createTestProps()
    const theme = { colors: { primary: 'red' } }
    const tree = render(
      <ThemeProvider theme={theme}>
        <Roll {...testProps} />
      </ThemeProvider>
    )

    expect(tree).toMatchSnapshot()
  })

  it('Allows headings justification', () => {
    const testProps = createTestProps({ justifyHeadings: 'left' })
    const tree = shallow(<Roll {...testProps} />)

    expect(tree.find(Heading).props().justifyHeading).toBe('left')
  })

  describe('Heading', () => {
    it('should be defined', () => {
      expect(Heading).toBeDefined()
    })

    it('should render correctly', () => {
      const tree = render(<Heading>heading</Heading>)

      expect(tree).toMatchSnapshot()
    })
  })
})
