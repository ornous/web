import styled from '@emotion/styled'

export default styled.div`
  display: grid;
  grid-gap: 0 3vw;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
`
