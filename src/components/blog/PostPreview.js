import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import { Card, CardCover, CardImg, CardHeadings, CardLink } from '../Card'

const PostPreview = ({
  id,
  timeToRead,
  fields: {
    slug,
  },
  frontmatter: {
    date,
    title,
    featuredMedia,
  }
}) => (
  <Card key={id}>
    <CardLink to={slug}>
      <CardCover>
        <CardImg
          alt={title}
          fluid={featuredMedia.childImageSharp.fluid}
        />
      </CardCover>
      <CardHeadings justifyHeadings="left">
        <h6>
          {`${timeToRead} minute${timeToRead > 1 ? 's' : ''} read `}
          &middot;&nbsp;
          {date}
        </h6>
        <h4>{title}</h4>
      </CardHeadings>
    </CardLink>
  </Card>
)

PostPreview.propTypes = {
  id: PropTypes.string.isRequired,
  timeToRead: PropTypes.number.isRequired,
  fields: PropTypes.shape({
    slug: PropTypes.string.isRequired,
  }).isRequired,
  frontmatter: PropTypes.shape({
    date: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    featuredMedia: PropTypes.shape({}),
  }).isRequired,
}

export const query = graphql`
  fragment PostPreview on MarkdownRemark {
    id
    timeToRead
    fields {
      slug
    }
    frontmatter {
      title
      description
      date(formatString: "MMMM DD, YYYY")
      featuredMedia {
        childImageSharp {
          fluid(maxWidth: 500, maxHeight: 300) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  }
`

export default PostPreview
