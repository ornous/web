import styled from '@emotion/styled'

const Wrapper = styled.footer`
  grid-area: footer;
  padding: 0 1rem;
  background-color: var(--primary-color);
  color: var(--on-primary-color);
  margin: 0;
  font-size: 0.8rem;

  a,
  a:visited,
  a:hover,
  a:focus,
  a:active {
    color: var(--on-primary-color);
  }

  a:hover,
  a:focus {
    outline: 0;
    filter: drop-shadow(0px 0px 0.8px hsla(0, 0%, 100%, 0.8));
  }
`

export default Wrapper
