import React from 'react'
import PropTypes from 'prop-types'
import Heading from './Heading'
import List from './List'

const Nav = ({ children, heading }) => (
  <>
    {heading && <Heading>{heading}</Heading>}
    <List>{children}</List>
  </>
)

Nav.propTypes = {
  children: PropTypes.node.isRequired,
  heading: PropTypes.string,
}

Nav.defaultProps = {
  heading: null,
}

export default Nav
