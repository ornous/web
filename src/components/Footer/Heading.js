import styled from '@emotion/styled'

const Heading = styled.h3`
  color: var(--on-primary-color);
  text-align: center;
`

export default Heading
