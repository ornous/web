import React from 'react'
import { render } from 'enzyme'

import Wrapper from '../Wrapper'

describe('Wrapper', () => {
  it('should be defined', () => {
    expect(Wrapper).toBeDefined()
  })

  it('should render correctly', () => {
    const wrapper = render(<Wrapper>Label</Wrapper>)

    expect(wrapper).toMatchSnapshot()
  })
})
