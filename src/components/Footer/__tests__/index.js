import React from 'react'
import { shallow } from 'enzyme'
import { useStaticQuery } from 'gatsby'

import Footer, { Copyright, Nav } from '..'
import OutboundLink from '../../OutboundLink'
import BrandIcon from '../../BrandIcon'

describe('Footer', () => {
  beforeEach(() => {
    useStaticQuery.mockReturnValueOnce({
      site: {
        siteMetadata: {
          socialLinks: [
            {
              name: 'social',
              url: 'http://social.link',
              brandIcon: 'social',
              title: 'Engage with this social thing',
            },
          ],
        },
      },
    })
  })
  it('should be defined', () => {
    expect(Footer).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<Footer heading="Stay in Touch" />)

    const link = tree.find(OutboundLink)
    expect(link.props().href).toEqual('http://social.link')
    expect(link.props().title).toEqual('Engage with this social thing')

    expect(link.contains(<BrandIcon icon="social" />)).toBe(true)
    expect(link.contains('social')).toBe(true)
  })

  describe('Nav', () => {
    it('should be defined', () => {
      expect(Nav).toBeDefined()
    })

    it('should render correctly', () => {
      const tree = shallow(<Nav heading="Quick Links">Label</Nav>)

      expect(tree).toMatchSnapshot()
    })
  })

  describe('Copyright', () => {
    it('should be defined', () => {
      expect(Copyright).toBeDefined()
    })

    it('should render the current year', () => {
      const tree = shallow(<Copyright />)

      expect(tree).toMatchSnapshot()
    })
  })
})
