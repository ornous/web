import React from 'react'
import { render } from 'enzyme'

import Heading from '../Heading'

describe('Heading', () => {
  it('should be defined', () => {
    expect(Heading).toBeDefined()
  })

  it('should render correctly', () => {
    const wrapper = render(<Heading>Label</Heading>)

    expect(wrapper).toMatchSnapshot()
  })
})
