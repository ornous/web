import React from 'react'
import { render } from 'enzyme'

import List from '../List'

describe('List', () => {
  it('should be defined', () => {
    expect(List).toBeDefined()
  })

  it('should render correctly', () => {
    const wrapper = render(<List>Label</List>)

    expect(wrapper).toMatchSnapshot()
  })
})
