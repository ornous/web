import styled from '@emotion/styled'

const List = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(2rem, 1fr));
  column-gap: 1rem;
  list-style-type: none;
  max-width: 500px;
  margin: 0 auto;
  padding: 0;
  font-size: 0.85rem;

  li {
    text-align: center;
  }

  a {
    display: inline-flex;
    color: var(--on-primary-color);
    text-transform: lowercase;
    margin-bottom: 0.26em;
    transition: transform ease 200ms;
    letter-spacing: 0.045em;
    align-items: center;
    justify-content: space-around;
    flex-flow: column;

    &:visited {
      color: var(--on-primary-color);
    }

    &:hover,
    &:focus {
      color: var(--on-primary-color);
      filter: drop-shadow(0px 0px 0.8px hsla(0, 0%, 100%, 0.3));
      transform: scale(1.12) translateY(-3px);
    }
  }
`

export default List
