import React from 'react'

import useSocialLinks from '../../hooks/useSocialLinks'
import OutboundLink from '../OutboundLink'
import BrandIcon from '../BrandIcon'
import Copyright from './Copyright'
import Wrapper from './Wrapper'
import Nav from './Nav'

const Footer = () => {
  const socialLinks = useSocialLinks()

  return (
    <Wrapper>
      <Nav heading="Stay in Touch">
        {socialLinks.map(({ name, url, title, brandIcon }) => (
          <li key={name}>
            <OutboundLink href={url} title={title}>
              <BrandIcon icon={brandIcon} />
              {name}
            </OutboundLink>
          </li>
        ))}
      </Nav>
      <Copyright />
    </Wrapper>
  )
}

export { Footer as default, Copyright, Nav }
