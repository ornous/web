import React from 'react'
import { Link } from 'gatsby'
import styled from '@emotion/styled'

const DimText = styled.div`
  font-size: 0.7rem;
  margin: 1em 0;
  text-align: center;

  a {
    text-decoration-skip-ink: auto;
    text-decoration: dotted underline;
  }
`

const Copyright = () => (
  <DimText>
    Copyright ©&nbsp;
    {new Date().getFullYear()}
    &nbsp;
    <Link to="/">Ousmane Ndiaye</Link>
  </DimText>
)

export default Copyright
