import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import {
  Card,
  CardCover,
  CardImg,
  CardHeadings,
  CardLink,
  CardContent,
} from '../Card'

const ProjectPreview = ({
  id,
  fields: {
    slug,
  },
  frontmatter: {
    title,
    // description,
    repoUrl,
    liveUrl,
    stackshareUrl,
    featuredMedia: {
      childImageSharp: {
        fluid,
      },
    },
  },
}) => (
  <Card key={id}>
    <CardLink to={slug}>
      <CardCover>
        <CardImg alt={title} fluid={fluid} />
      </CardCover>
      <CardHeadings justifyHeadings="left">
        <h4>{title}</h4>
      </CardHeadings>
    </CardLink>
    <CardContent>
      <ul>
        <li>
          <a href={liveUrl}>Live</a>
        </li>
        <li>
          <a href={repoUrl}>Gitlab</a>
        </li>
        <li>
          <a href={stackshareUrl}>StackShare</a>
        </li>
      </ul>
    </CardContent>
  </Card>
)

ProjectPreview.propTypes = {
  id: PropTypes.string.isRequired,
  fields: PropTypes.shape({
    slug: PropTypes.string.isRequired,
  }).isRequired,
  frontmatter: PropTypes.shape({
    title: PropTypes.string.isRequired,
    liveUrl: PropTypes.string.isRequired,
    repoUrl: PropTypes.string.isRequired,
    stackshareUrl: PropTypes.string.isRequired,
    featuredMedia: PropTypes.shape({}),
  }).isRequired,
}

export const query = graphql`
  fragment ProjectPreview on MarkdownRemark {
    id
    fields {
      slug
    }
    frontmatter {
      title
      description
      liveUrl
      repoUrl
      stackshareUrl
      featuredMedia {
        childImageSharp {
          fluid(maxWidth: 500, maxHeight: 300) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  }
`

export default ProjectPreview
