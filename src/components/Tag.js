import styled from '@emotion/styled'

const Tag = styled.li`
  padding: 0.4em 0.8em;
  font-size: 0.9rem;
  font-weight: 400;
  background-color: var(--secondary-color);
  border: 1px solid var(--secondary-color);
  border-radius: 0.25em;
  margin-right: 0.5rem;
`

export default Tag
