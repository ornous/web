import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const BrandIcon = ({ icon }) => (
  <FontAwesomeIcon icon={['fab', icon]} size="3x" />
)

BrandIcon.propTypes = {
  icon: PropTypes.string.isRequired,
}

export default BrandIcon
