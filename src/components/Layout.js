import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'emotion-theming'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'

import { Grid, GridContainer, MainArea, Header } from './Grid'
import theme, { muiTheme } from '../themes'
import GlobalStyles from './GlobalStyles'
import Footer from './Footer'
import Menu from './Menu'
import SEO from './SEO'

const Layout = ({ children, hero }) => (
  <>
    <SEO />
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <MuiThemeProvider theme={muiTheme}>
        <GridLayout Hero={hero}>{children}</GridLayout>
      </MuiThemeProvider>
    </ThemeProvider>
  </>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  hero: PropTypes.element,
}

Layout.defaultProps = {
  hero: null,
}

// Should hero be capitalised?
const GridLayout = ({ children, Hero }) => (
  <GridContainer>
    <Grid>
      <Header>
        {Hero !== null && Hero}
        <Menu />
      </Header>
      <MainArea>{children}</MainArea>
      <Footer />
    </Grid>
  </GridContainer>
)

GridLayout.propTypes = {
  children: PropTypes.node.isRequired,
  Hero: PropTypes.element,
}

GridLayout.defaultProps = {
  Hero: null,
}

export { Layout as default, GridLayout }
