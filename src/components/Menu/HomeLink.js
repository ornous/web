import { Link } from 'gatsby'
import styled from '@emotion/styled'

export default styled(Link)`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-items: center;
  height: inherit;

  &:visited {
    color: var(--primary-color);
  }
`
