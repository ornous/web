import styled from '@emotion/styled'
import { css } from '@emotion/core'

const List = styled.ul`
  grid-area: menu;
  list-style-type: none;
  text-indent: 0;
  text-align: center;
  margin: 0;
  padding: 0;
  font-family: lato;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  transition: transform 200ms ease-in-out;
  background-color: white;
  transition: transform 200ms ease-in-out;
  margin-left: 100vw;
  width: 70vw;

  li > a[href='#close'] {
    text-align: right;
  }

  ${({ open }) => !!open && css`transform: translateX(-70vw); `}

  /* TODO How to trigger the target state from js? */
  &:target {
    transform: translateX(-70vw);
  }

  @media (min-width: 700px) {
    background: none;
    margin: 0;
    flex-flow: row;
    width: auto;

    li > a[href='#close'] {
      display: none;
      text-align: right;
    }  }

  @media (max-width: 699px) {
    position: absolute;

    li {
      flex: 1 1 100%;

      a {
      }
    }
  }
`

export default List
