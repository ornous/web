import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import useMenu from '../../hooks/useMenu'
import Logo from '../Logo'
import Menu from './Menu'
import Hamburger from './Hamburger'
import HomeLink from './HomeLink'
import Heading from './Heading'
import NavLink from './NavLink'
import MenuList from './List'

export default () => {
  const { menuItems, menuOpen, toggle } = useMenu()

  return (
    <Menu>
      <Heading>
        <HomeLink to="/">
          <Logo />
          ornous
        </HomeLink>
      </Heading>
      {/* TODO reconcile cypress and react-testing library's test ids*/}
      <MenuList data-testid="menu" id="menu" open={menuOpen}>
        <li>
          <a
            href="#close"
            aria-label="Close Menu"
            data-test="close-menu"
            onClick={toggle}
          >
            <FontAwesomeIcon icon="times" />
          </a>
        </li>
        {menuItems.map(({ name, path }) => (
          <NavLink to={path} key={path}>{name}</NavLink>
        ))}
      </MenuList>
      <Hamburger toggle={toggle} />
    </Menu>
  )
}
