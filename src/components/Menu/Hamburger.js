import React from 'react'
import styled from '@emotion/styled'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Container = styled.a`
  grid-area: hamburger;
  font-family: lato;
  color: var(--primary-color);
  font-size: 2em;

  @media (min-width: 700px) {
    display: none;
  }

  &:visited {
    color: var(--primary-color);
  }
`

const Hamburger = ({ toggle}) => (
  <div role="dialog" style={{ display: 'contents' }}>
    <Container
      href="#menu"
      aria-label="Open Menu"
      data-test="open-menu"
      data-testid="open-menu"
      onClick={toggle}
    >
      <FontAwesomeIcon icon="bars" />
    </Container>
  </div>
)

export default Hamburger
