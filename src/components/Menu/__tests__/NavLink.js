import React from 'react'
import { render } from 'enzyme'

import NavLink from '../NavLink'

describe('NavLink', () => {
  it('renders correctly', () => {
    const tree = render(<NavLink to="https://www.ornous.com">Label</NavLink>)

    expect(tree).toMatchSnapshot()
  })
})
