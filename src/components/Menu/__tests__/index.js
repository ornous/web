import React from 'react'
import { useStaticQuery } from 'gatsby'
import { ThemeProvider } from 'emotion-theming'
import { render, fireEvent } from '@testing-library/react'
// TODO Move to test setUpBeforeFiles
import '@testing-library/jest-dom/extend-expect'

import Menu from '..'

describe('Menu', () => {
  beforeEach(() => {
    useStaticQuery.mockReturnValue({
      site: {
        siteMetadata: {
          headerMenuItems: [
            { path: '/', name: 'Home' },
          ],
        },
      },
    })
  })

  it('renders correctly', () => {

    const { asFragment } = render(
      <ThemeProvider theme={{ colors: { secondary: 'green' } }}>
        <Menu>Children</Menu>
      </ThemeProvider>
    )

    expect(asFragment()).toMatchSnapshot()
  })

  it('opens the menu with a click', () => {
    const { getByTestId, getByText } = render(
      <ThemeProvider theme={{ colors: { secondary: 'green' } }}>
        <Menu>Children</Menu>
      </ThemeProvider>
    )

    expect(getByText('Home')).toBeVisible()

    fireEvent.click(getByTestId('open-menu'))
    expect(getByText('Home')).toBeVisible()
  })
})
