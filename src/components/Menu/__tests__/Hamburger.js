import React from 'react'
import { render } from 'enzyme'

import Hamburger from '../Hamburger'

describe('Hamburger', () => {
  it('renders correctly', () => {
    const tree = render(<Hamburger />)

    expect(tree).toMatchSnapshot()
  })
})
