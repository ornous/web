import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'
import { Link as GatsbyLink } from 'gatsby'

const Wrapper = styled.li`
  margin-bottom: 0;
  display: flex;
  flex-basis: auto;
  height: 100%;
`

const Link = styled(GatsbyLink)`
  display: inline-block;
  position: relative;
  padding: 0 1.3em;
  opacity: 0.9;
  color: var(--primary-color);
  text-decoration: none;
  letter-spacing: 0.16rem;
  text-transform: uppercase;
  font-size: 1rem;
  font-weight: bold;
  transition: background-color 0.2s ease-in-out, opacity 0.1s ease-in;

  &:after {
    content: '';
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    background-color: var(--primary-color);
    height: 4px;
    width: 100%;
    transform: scaleX(0);
  }

  &:hover,
  &:focus {
    outline: 0;
    opacity: 1;

    &:after {
      transition: transform ease 250ms;
      transform: scaleX(1);
    }
  }

  &.active {
    box-shadow: inset 0px -4px 0px var(--primary-color-variant);
    background-color: hsla(0, 0%, 30%, 0.1);
  }

  &:visited {
    color: var(--primary-color);
  }
`

const NavLink = ({ children, to }) => (
  <Wrapper>
    <Link to={to} activeClassName="active">
      {children}
    </Link>
  </Wrapper>
)

NavLink.propTypes = {
  children: PropTypes.node.isRequired,
  to: PropTypes.string.isRequired,
}

export default NavLink
