import styled from '@emotion/styled'
import { transparentize } from 'polished'

export default styled.nav`
  display: grid;
  /* Place fixed declaration inside feature query */
  position: fixed;
  z-index: 1;
  grid-template-areas:
    'heading spacer hamburger'
    'menu menu menu';
  grid-template-columns: 190px auto 40px;
  width: 100%;
  height: 3rem;
  line-height: 3rem;
  background-color: ${({ theme }) =>
    transparentize(0.15, theme.colors.secondary)};
  box-shadow: 0 3px 6px hsla(0, 0%, 0%, 0.4);
  grid-area: hero;

  @media (min-width: 700px) {
    grid-template-areas: 'heading menu';
    grid-template-columns: 190px minmax(300px, 1fr);
  }

  @media (min-width: 840px) {
    grid-template-columns: 190px minmax(300px, 1fr) 190px;
  }
`
