import styled from '@emotion/styled'

const Heading = styled.h3`
  grid-area: heading;
  text-transform: uppercase;
  letter-spacing: 0.1rem;
  color: var(--primary-color);
  font-size: 1.1rem;
  margin: 0;
  height: inherit;

  a {
    text-decoration: none;
    color: inherit;
  }
`

export default Heading
