import React from 'react'
import { shallow } from 'enzyme'

import Teaser from '../Teaser'

const data = {
  id: 'id',
  fields: {
    slug: 'slug',
  },
  frontmatter: {
    title: 'I am a tease',
    featuredMedia: {
      childImageSharp: { fluid: {} },
    },
  },
}

describe('Teaser', () => {
  it('should be defined', () => {
    expect(Teaser).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<Teaser {...data}>Label</Teaser>)

    expect(tree).toMatchSnapshot()
  })
})
