import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'
import styled from '@emotion/styled'
import { transparentize } from 'polished'

const Wrapper = styled.div`
  padding-bottom: 1rem;
  margin-bottom: 1rem;
  font-size: 0.9rem;
  min-width: 200px;
  position: relative;
  border-radius: 1.3rem;
`

const FeaturedMediaWrapper = styled.div`
  grid-area: featuredMedia;
  box-shadow: 1px 2px 4px #8c8c8c;
  overflow: hidden;
`

const FeaturedMedia = styled(Img)`
  transition: transform 0.25s ease, filter 0.25s ease;
  filter: sepia(20%);
`

const RecipeLink = styled(Link)`
  font-weight: 400;
  text-decoration: none;

  &:hover,
  &:focus {
    outline: 0;

    ${FeaturedMedia} {
      transform: scale(1.12);
      filter: sepia(0);
    }
  }
`

const Title = styled.h4`
  position: absolute;
  top: 17%;
  left: 0;
  right: 0;
  /* TODO color integration*/
  background-color: ${({ theme }) =>
    transparentize(0.35, theme.colors.experimental[2])};
  color: var(--primary-color);
  margin: 0;
  font-size: 1.1rem;
  padding: 0.8em;
  overflow: hidden;
  max-width: 100%;
`

const Teaser = ({ id, frontmatter, fields }) => {
  const imgData = frontmatter.featuredMedia.childImageSharp.fluid

  return (
    <Wrapper key={id}>
      <RecipeLink title={frontmatter.title} to={fields.slug}>
        <FeaturedMediaWrapper>
          <FeaturedMedia alt={frontmatter.title} fluid={imgData} />
          <Title>
            {frontmatter.title}
            <br />
            <small>{frontmatter.date}</small>
          </Title>
        </FeaturedMediaWrapper>
      </RecipeLink>
    </Wrapper>
  )
}

Teaser.propTypes = {
  id: PropTypes.string.isRequired,
  fields: PropTypes.shape({
    slug: PropTypes.string.isRequired,
  }).isRequired,
  frontmatter: PropTypes.shape({
    title: PropTypes.string.isRequired,
    featuredMedia: PropTypes.shape({}),
  }).isRequired,
}

export default Teaser

export const query = graphql`
  fragment RecipeTeaser on MarkdownRemarkEdge {
    node {
      id
      fields {
        slug
      }
      frontmatter {
        title
        featuredMedia {
          childImageSharp {
            fluid(maxWidth: 500, maxHeight: 300) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        date(formatString: "MMMM DD, YYYY")
      }
    }
  }
`
