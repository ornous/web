import React from 'react'
import { render } from 'enzyme'
import { ThemeProvider } from 'emotion-theming'

import theme from '../../themes'
import GlobalStyles from '../GlobalStyles'

describe('GlobalStyles', () => {
  it('should be defined', () => {
    expect(GlobalStyles).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = render(
      <ThemeProvider theme={theme}>
        <GlobalStyles>Label</GlobalStyles>
      </ThemeProvider>
    )

    expect(tree).toMatchSnapshot()
  })
})
