import React from 'react'
import { shallow } from 'enzyme'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import BrandIcon from '../BrandIcon'

describe('BrandIcon', () => {
  it('should be defined', () => {
    expect(BrandIcon).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<BrandIcon icon="hellow" />)
    expect(
      tree.matchesElement(
        <FontAwesomeIcon icon={['fab', 'hellow']} size="3x" />
      )
    ).toBe(true)
  })
})
