import React from 'react'
import { render, shallow } from 'enzyme'

import Hero from '../Hero'
const imageData = {
  src: 'img.jpg',
  srcSet: '',
  aspectRatio: 1.2,
  sizes: '',
}

describe('Hero', () => {
  it('renders correctly', () => {
    const tree = render(
      <Hero
        heading="There goes my hero!"
        alt="A photo of Dave Grohl"
        imageData={imageData}
      />
    )

    expect(tree).toMatchSnapshot()
  })

  it('Displays Subtitle when provided', () => {
    const tree = shallow(
      <Hero
        heading="There goes my hero!"
        subHeading="He's ordinary"
        alt="A photo of Dave Grohl"
        imageData={imageData}
      />
    )

    expect(tree.contains("He's ordinary")).toBe(true)
  })
})
