import React from 'react'
import { shallow } from 'enzyme'

import { Grid } from '../Grid'

describe('Grid', () => {
  let wrapper
  const createWrapper = props => shallow(<Grid {...props} />)

  beforeEach(() => {
    wrapper = createWrapper()
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
