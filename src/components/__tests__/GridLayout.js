import React from 'react'
import { shallow } from 'enzyme'

import { GridLayout } from '../Layout'
import Hero from '../Hero'

describe('GridLayout', () => {
  let wrapper
  const createWrapper = props =>
    shallow(<GridLayout {...props}>Children</GridLayout>)

  beforeEach(() => {
    wrapper = createWrapper({
      Hero: (
        <Hero
          alt="Hero Description"
          heading="Hero Title"
          imageData={{ src: '' }}
        />
      ),
    })
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
