import React from 'react'
import { shallow } from 'enzyme'

import Layout from '../Layout'

describe('Layout', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(<Layout>Children</Layout>)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
