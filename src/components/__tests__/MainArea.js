import React from 'react'
import { shallow } from 'enzyme'

import { MainArea } from '../Grid'

describe('MainArea', () => {
  let wrapper
  const createWrapper = props => shallow(<MainArea {...props} />)
  beforeEach(() => {
    wrapper = createWrapper()
  })
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
