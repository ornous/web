import 'jsdom-global/register'
import React from 'react'
import { shallow } from 'enzyme'

import Content from '../Content'

describe('Content', () => {
  it('should be defined', () => {
    expect(Content).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<Content heading="">Label</Content>)

    expect(tree).toMatchSnapshot()
  })
})
