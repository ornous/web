import React from 'react'
import { render } from 'enzyme'

import { HTMLContent } from '../Content'

describe('HTMLContent', () => {
  it('should be defined', () => {
    expect(HTMLContent).toBeDefined()
  })

  it('should render correctly', () => {
    const sampleHtml = `
      <h1>Hello</h1>
      <ul>
        <li style={{ color: 'red' }}><b>item</b></li>
        <li>item</li>
      </ul>
    `

    const tree = render(<HTMLContent content={sampleHtml}>Label</HTMLContent>)

    expect(tree).toMatchSnapshot()
  })
})
