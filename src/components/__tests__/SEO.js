import React from 'react'
import { render } from 'enzyme'
import { useStaticQuery } from 'gatsby'
import Helmet from 'react-helmet'
import SEO from '../SEO'

const getHelmetMetaTagContent = tagName =>
  Helmet.peek().metaTags.find(
    ({ name, property }) => property === tagName || name === tagName
  ).content

describe('SEO', () => {
  it('Displays Twitter Username', () => {
    useStaticQuery.mockReturnValue({
      site: { siteMetadata: { twitterUsername: '@me' } },
      image: { childImageSharp: { social: { image: '' } } },
    })

    render(<SEO />)
    expect(getHelmetMetaTagContent('twitter:creator')).toEqual('@me')
  })

  it('Displays Facebook App Id', () => {
    useStaticQuery.mockReturnValueOnce({
      site: { siteMetadata: { facebookAppId: 'E56$idc,ez' } },
      image: { childImageSharp: { social: { image: '' } } },
    })

    render(<SEO />)
    expect(getHelmetMetaTagContent('fb:app_id')).toEqual('E56$idc,ez')
  })

  describe('Title', () => {
    beforeEach(() => {
      useStaticQuery.mockReturnValueOnce({
        site: { siteMetadata: { title: 'Default Title' } },
        image: { childImageSharp: { social: { image: '' } } },
      })
    })

    describe('Not Provided', () => {
      it('should render default title', () => {
        render(<SEO />)

        expect(Helmet.peek().title).toEqual('Default Title')
      })
    })

    describe('Provided', () => {
      it('should update page title', () => {
        render(<SEO title="Title" />)

        expect(Helmet.peek().title).toEqual('Title')
        expect(getHelmetMetaTagContent('og:title')).toEqual('Title')
        expect(getHelmetMetaTagContent('twitter:title')).toEqual('Title')
      })
    })
  })

  describe('Description', () => {
    beforeEach(() => {
      useStaticQuery.mockReturnValueOnce({
        site: {
          siteMetadata: { description: 'Default Description' },
        },
        image: { childImageSharp: { social: { image: '' } } },
      })
    })

    describe('Not Provided', () => {
      it('should render default description', () => {
        render(<SEO />)

        expect(getHelmetMetaTagContent('description')).toEqual(
          'Default Description'
        )
      })
    })

    describe('Provided', () => {
      it('should update page description', () => {
        render(<SEO description="Description" />)

        expect(getHelmetMetaTagContent('description')).toEqual('Description')
        expect(getHelmetMetaTagContent('og:description')).toEqual('Description')
        expect(getHelmetMetaTagContent('twitter:description')).toEqual(
          'Description'
        )
      })
    })
  })
})
