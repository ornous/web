import React from 'react'
import { shallow } from 'enzyme'

import { Header } from '../Grid'

describe('Header', () => {
  let wrapper
  const createWrapper = props => shallow(<Header {...props} />)
  beforeEach(() => {
    wrapper = createWrapper()
  })
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
