import React from 'react'
import { shallow } from 'enzyme'

import Logo from '../Logo'

describe('Logo', () => {
  let wrapper
  const createWrapper = props => shallow(<Logo {...props} />)
  beforeEach(() => {
    wrapper = createWrapper()
  })
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
