import React from 'react'
import { shallow } from 'enzyme'

import OutboundLink from '../OutboundLink'

describe('OutboundLink', () => {
  it('should be defined', () => {
    expect(OutboundLink).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<OutboundLink href="http://li.nk">Label</OutboundLink>)
    expect(
      tree.matchesElement(
        <a target="_blank" rel="noopener noreferrer" href="http://li.nk">
          Label
        </a>
      )
    ).toBe(true)
  })
})
