import React from 'react'
import { Helmet } from 'react-helmet'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import useSeoMetadata from '../hooks/useSeoMetadata'

const SEO = ({ url, ...props }) => {
  const {
    lang,
    type,
    title,
    titleTemplate,
    image,
    description,
    facebookAppId,
    twitterUsername,
  } = useSeoMetadata(props)

  return (
    <>
      <Helmet title={title} titleTemplate={titleTemplate}>
        <html lang={lang} />
        <meta name="description" content={description} />
        <meta name="image" content={image} />
        {url && <meta property="og:url" content={url} />}
        <meta property="og:type" content={type} />
        {title && <meta property="og:title" content={title} />}
        {description && (
          <meta property="og:description" content={description} />
        )}
        {image && <meta property="og:image" content={image} />}
        <meta property="fb:app_id" content={facebookAppId} />

        <meta name="twitter:card" content="summary_large_image" />
        {twitterUsername && (
          <meta name="twitter:creator" content={twitterUsername} />
        )}
        {title && <meta name="twitter:title" content={title} />}
        {description && (
          <meta name="twitter:description" content={description} />
        )}
        {image && <meta name="twitter:image" content={image} />}
      </Helmet>
    </>
  )
}

export const query = graphql`
  fragment SeoImage on File {
    childImageSharp {
      social: resize(width: 1200, height: 627) {
        src
      }
    }
  }
`

export default SEO

SEO.propTypes = {
  title: PropTypes.string,
  type: PropTypes.oneOf(['website', 'article', 'recipe']),
  description: PropTypes.string,
  image: PropTypes.string,
  pathname: PropTypes.string,
}

SEO.defaultProps = {
  title: null,
  type: 'website',
  description: null,
  image: null,
  pathname: null,
}
