import React from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'

import Book from './Book'

const ShelfTitle = styled.h2`
  font-size: 1.3rem;
  text-align: center;
`

const BookList = styled.ul`
  display: grid;
  grid-gap: 2rem;
  grid-template-columns: repeat(auto-fit, minmax(16rem, 1fr));
  list-style-type: none;
  justify-content: start;
  align-items: left;
  padding: 3vw 3vw;
`

const Container = styled.div``

const BookShelf = ({ name, reviews }) => (
  <Container key={name}>
    <ShelfTitle>{`${name} (${reviews.length})`}</ShelfTitle>
    <BookList>
      {reviews.map(({ book: { id, title, isbn, imageUrl, authors } }) => (
        <Book
          key={id}
          title={title}
          isbn={isbn}
          imageUrl={imageUrl}
          authors={authors}
        />
      ))}
    </BookList>
  </Container>
)

BookShelf.propTypes = {
  name: PropTypes.string.isRequired,
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      book: PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        imageUrl: PropTypes.string.isRequired,
        authors: PropTypes.arrayOf(
          PropTypes.shape({
            name: PropTypes.string.isRequired,
          })
        ).isRequired,
      }).isRequired,
    })
  ).isRequired,
}

export default BookShelf
