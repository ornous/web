import React from 'react'
import PropTypes from 'prop-types'

const OutboundLink = ({ children, href, title }) => (
  <a target="_blank" rel="noopener noreferrer" href={href} title={title}>
    {children}
  </a>
)

OutboundLink.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string.isRequired,
  title: PropTypes.string,
}

OutboundLink.defaultProps = {
  title: null,
}

export default OutboundLink
