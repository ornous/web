import styled from '@emotion/styled'

export const bookCoverUrl = (goodreadsImageUrl, isbn) => {
  if (!/nophoto/.test(goodreadsImageUrl)) {
    return goodreadsImageUrl
  } else if (isbn) {
    return `https://covers.openlibrary.org/b/isbn/${isbn}-M.jpg?default=false`
  }

  return null
}

const BookCover = styled.img`
  grid-area: image;
  height: 151px;
  object-fit: fill;
  box-shadow: 3px 3px 2px 0px hsla(0, 0%, 0%, 0.09);
  border-radius: 0.3rem;
`

export default BookCover
