import React from 'react'
import { graphql } from 'gatsby'
import PropTypes from 'prop-types'
import styled from '@emotion/styled'

import Cover, { bookCoverUrl } from './Cover'

const BookCard = styled.li`
  display: flex;
  font-size: 0.81rem;
  padding: 0.7em;
  background-color: var(--secondary-color);
  box-shadow: 8px 8px 5px 0px hsla(0, 0%, 0%, 0.09);
  border-radius: 0.3rem;

  h4,
  h5 {
    color: var(--primary-color);
    padding: 0;
    margin: 0.5em 0.5rem;
  }
`

const Details = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
`

const Book = ({ isbn, title, authors, imageUrl }) => {
  const coverImageUrl = bookCoverUrl(imageUrl, isbn)

  // TODO add oubound link
  return (
    <BookCard>
      {coverImageUrl ? <Cover alt={title} src={coverImageUrl} /> : null}
      <Details>
        <h4>{title}</h4>
        <h5>{authors.map(({ name }) => name).join(', ')}</h5>
      </Details>
    </BookCard>
  )
}

Book.propTypes = {
  title: PropTypes.string.isRequired,
  isbn: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  authors: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
}

export default Book

export const query = graphql`
  fragment Book on GoodreadsReview {
      dateUpdated: date_updated
      book {
        id
        isbn
        title
        imageUrl: image_url
        authors {
          name
        }
      }
  }
`
