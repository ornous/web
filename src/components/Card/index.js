import styled from '@emotion/styled'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

const Card = styled.div`
  display: grid;
  grid-template-areas:
    'cover'
    'headings'
    'content';
  grid-template-rows: min-content 4rem auto;
  margin-bottom: 3vw;
  font-size: 0.9rem;
  min-width: 200px;
`

const CardHeadings = styled.div`
  grid-area: headings;
  padding: 0.9rem;
  font-size: 1rem;
  font-weight: 500;
  height: 240px;

  &:hover,
  &:focus {
    outline: 0;
    text-decoration: underline;
  }

  h4,
  h6 {
    text-align: ${props => props.justifyHeadings};
    margin: 0;
  }

  h4 {
    line-height: 1.25;
    color: ${({ theme }) => theme.colors.primary};
  }
`

const CardCover = styled.div`
  grid-area: cover;
  box-shadow: 8px 8px 5px 1px hsla(0, 0%, 0%, 0.09);
  border-radius: 0.3em;
  overflow: hidden;
`

const CardImg = styled(Img)`
  transition: transform 0.25s ease, filter 0.25s ease;
  filter: sepia(20%);
`

const CardContent = styled.div`
  grid-area: content;
  padding: 0 0.9rem;

  p {
    margin-bottom: 0.4rem;
  }
`

const CardLink = styled(Link)`
  display: contents;
  font-weight: 400;
  text-decoration: none;

  &:hover,
  &:focus {
    outline: 0;

    ${CardImg} {
      transform: scale(1.12);
      filter: sepia(0);
    }
  }
`

export { Card, CardHeadings, CardCover, CardImg, CardContent, CardLink }
