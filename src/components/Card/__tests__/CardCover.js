import React from 'react'
import { shallow } from 'enzyme'

import { CardCover } from '..'

describe('CardCover', () => {
  it('should be defined', () => {
    expect(CardCover).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<CardCover>Label</CardCover>)

    expect(tree).toMatchSnapshot()
  })
})
