import React from 'react'
import { render } from 'enzyme'

import { CardHeadings } from '..'

describe('CardHeadings', () => {
  it('should be defined', () => {
    expect(CardHeadings).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = render(
      <CardHeadings
        theme={{ colors: { primary: 'red' } }}
        justifyHeadings="left"
      >
        <h4>Label</h4>
      </CardHeadings>
    )

    expect(tree).toMatchSnapshot()
  })
})
