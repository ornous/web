import React from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'

export const HTMLContent = ({ content }) => (
  /* eslint-disable-next-line react/no-danger */
  <div dangerouslySetInnerHTML={{ __html: content }} />
)

const Content = styled.article`
  padding: 0 3vw;
  max-width: ${({ legible }) => legible ?  '80ch' : '93vw'};
  margin: 0 auto;
  letter-spacing: 0.028em;

  h1 {
    line-height: 1.4;
    margin-bottom: 0;
  }

  p {
    line-height: 1.55;
  }
`

HTMLContent.propTypes = {
  content: PropTypes.node.isRequired,
}

export default Content
