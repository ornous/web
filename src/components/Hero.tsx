import React, { FC } from 'react'
import { graphql } from 'gatsby'
import Img, { FluidObject } from 'gatsby-image'
import styled from '@emotion/styled'

const HeroImage = styled(Img)`
  grid-area: hero;
  width: 100%;
  height: 100%;
  max-height: 550px;
  z-index: -1;
`

const Content = styled.div`
  width: 100%;
  max-width: 80ch;
  margin: 0 auto;
  padding: 1rem;
`

const Overlay = styled.div`
  grid-area: hero;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-end;
  background-color: hsla(0, 0%, 0%, 0.5);
  margin-top: 3rem;
  padding: 1rem;
`

const Heading = styled.h1`
  filter: drop-shadow(0px 1px 1px hsla(0, 0%, 19%, 1));
  letter-spacing: 0.125rem;
  font-size: 2rem;
  color: white;
`

const SubHeading = styled.h2`
  filter: drop-shadow(0px 1px 1px hsla(0, 0%, 19%, 1));
  letter-spacing: 0.09rem;
  font-size: 1.4rem;
  margin: 0;
  color: white;
`

interface Props {
  alt: string
  heading: string
  subHeading?: string
  imageData?: FluidObject | FluidObject[]
}

const Hero: FC<Props> = ({
  alt,
  heading,
  subHeading = null,
  imageData
}) => (
  <>
    <HeroImage fluid={imageData} alt={alt} />
    <Overlay>
      <Content>
        <Heading>{heading}</Heading>
        {subHeading && <SubHeading>{subHeading}</SubHeading>}
      </Content>
    </Overlay>
  </>
)

export default Hero

export const query = graphql`
  fragment HeroImage on File {
      childImageSharp {
        fluid(maxWidth: 1400, maxHeight: 550, cropFocus: CENTER) {
          ...GatsbyImageSharpFluid_withWebp_tracedSVG
        }
      }
  }
`
