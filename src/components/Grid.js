import styled from '@emotion/styled'

export const GridContainer = styled.div`
  height: 100vh;
`

export const Grid = styled.div`
  height: 100%;
  min-height: 100vh;
  display: grid;
  row-gap: 3vw;
  grid-template-areas: 'header' 'main' 'footer';
  grid-template-rows: min-content auto max-content;
  margin: 0;
  padding: 0;
`

export const MainArea = styled.section`
  grid-area: main;
  background-color: var(--background-color);
  color: var(--on-background-color);
  width: 100%;

  a {
    color: var(--primary-color);

    &:visited {
      color: var(--primary-color-variant);
    }
  }
`

export const Header = styled.header`
  grid-area: header;
  position: relative;
  margin: 0;
  display: grid;
  grid-template-areas: 'hero';
  max-height: 550px;
  overflow: hidden;
  box-shadow: 0px 8px 5px 1px hsla(0, 0%, 0%, 0.09);
`
