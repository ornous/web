import { useEffect, useRef } from 'react'

const usePortal = (containerSelector) => {
  if (typeof window === `undefined`) {
    return null
  }

  const domNodeRef = useRef(document.createElement('div'))

  useEffect(() => {
    document.querySelector(containerSelector).appendChild(domNodeRef.current)
    const currentRef = domNodeRef.current
    return () => { currentRef.remove() }
  })

  return domNodeRef.current
}

export default usePortal
