import { graphql, useStaticQuery } from 'gatsby'

const useHeaderMenuItems = () => {
  const { site } = useStaticQuery(graphql`
    query HeaderMenuItems {
      site {
        siteMetadata {
          headerMenuItems {
            name
            path
          }
        }
      }
    }
  `)

  return site.siteMetadata.headerMenuItems
}

export default useHeaderMenuItems
