import { useState } from 'react'

import usePortal from './usePortal'

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false)

  return {
    isShowing,
    toggle: () => { setIsShowing(!isShowing) },
    target: usePortal('#modals-portal'),
  }
}

export default useModal
