import { graphql, useStaticQuery } from 'gatsby'

/* TODO extract/convert to ts/document */
const pluckNullProps = (obj) =>  Object.fromEntries(
  Object.entries(obj).filter(([, value]) => value !== null)
)

const useSeoMetadata = (overrides) => {
  const {
    site: {
      siteMetadata
    },
    image: {
      childImageSharp: {
        social: {
          src: image
        }
      }
    }
  } = useStaticQuery(
    graphql`
      query SeoMetadata {
        site {
          siteMetadata {
            lang
            title
            titleTemplate
            description
            twitterUsername
            facebookAppId
          }
        }
        image: file(relativePath: { eq: "oz.jpg" }) {
          ...SeoImage
        }
      }
    `
  )

  const filteredOverrides = pluckNullProps(overrides)

  return { ...siteMetadata, image, ...filteredOverrides, }
}

export default useSeoMetadata
