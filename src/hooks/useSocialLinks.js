import { graphql, useStaticQuery } from 'gatsby'

const useSocialLinks = () => {
  const { site: { siteMetadata } } = useStaticQuery(graphql`
      query SocialLinks {
        site {
          siteMetadata {
            socialLinks {
              name
              url
              title
              brandIcon
            }
          }
        }
      }
    `
  )

  return siteMetadata.socialLinks
}

export default useSocialLinks
