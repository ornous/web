import { useState } from 'react'

import useHeaderMenuItems from './useHeaderMenuItems'

export default () => {
  const menuItems = useHeaderMenuItems()
  const [menuOpen, setMenuOpen] = useState(false)

  const toggle = (event) => {
    event.preventDefault()

    setMenuOpen(!menuOpen)
  }

  return { menuItems, menuOpen, toggle }
}
