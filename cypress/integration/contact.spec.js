describe('Contact Form', () => {
  it('Can send a message', () => {
    cy.server({ enable: true })
    // Ensure mocks are adequately set up
    cy.route('POST', '/.netlify/functions/contact', {})
    cy.visit('/contact')

    cy.contains('Contact Me').click()

    cy.fixture('contact-entry').then(([{ name, email, message }]) => {
      cy.get('#name').type(name)
      cy.get('#email').type(email)
      cy.get('#message').type(message)

      cy.contains('Submit').click()
      cy.contains('Thank you for reaching out')
    })
  })
})
