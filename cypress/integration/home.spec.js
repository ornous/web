describe('The Home Page', () => {
  context('SEO', () => {
    before(() => {
      cy.visit('/')
    })

    it('Title is correct', () => {
      cy.get('title').should(
        'contain',
        'Ousmane Ndiaye, Software Engineer | ornous.com'
      )
    })

    it("Description is set and isn't too long", () => {
      cy.get('meta[name="description"]')
        .should('have.prop', 'content')
        .and('have.length.of.at.most', 158)
    })

    it('Has one level one heading', () => {
      cy.get('h1').should('have.length.of', 1)
    })

    it('Has at least one level two heading', () => {
      cy.get('h2').should('have.length.of.at.least', 1)
    })
  })

  context('Latest Blog Articles', () => {
    before(() => {
      cy.visit('/')
    })

    it('Has a heading', () => {
      cy.contains('Featured Articles')
    })

    it('Shows three article teasers max', () => {
      cy.contains('Featured Articles')
        .siblings('div')
        .children('div')
        .should('have.length.of.at.most', 3)
        .each(teaser => {
          expect(teaser.find('h4').text()).to.be.a('string')

          // TODO: Make this suck less
          const pubDate = teaser.find('h6')
            .text()
            .split('·')
            .pop()
            .trim()
          expect(
            Cypress.moment(pubDate, 'MMMM DD, YYYY').isValid()
          ).to.equal(true)

          expect(teaser.find('a'))
            .to.have.prop('href')
            .and.to.contain('/blog/')
        })
    })
  })
})
