describe('Footer', () => {
  context('Social Links', () => {
    const socialLinks = [
      { label: 'LinkedIn', url: 'https://www.linkedin.com/in/ornous/' },
      { label: 'Twitter', url: 'https://twitter.com/@ornous' },
      { label: 'GitLab', url: 'https://gitlab.com/ornous' },
      { label: 'GitHub', url: 'https://github.com/ornous' },
    ]

    before(() => {
      cy.visit('/')
    })

    it('Greets with Stay in Touch', () => {
      cy.get('footer').contains('Stay in Touch')
    })

    socialLinks.forEach(({ label, url }) => {
      it(`Links to ${label}`, () => {
        cy.get('footer')
          .contains(label)
          .should('have.prop', 'href', url)
      })
    })
  })

  context('Copyright', () => {
    before(() => {
      cy.visit('/blog')
    })

    it('Displays the current year', () => {
      const currentYear = Cypress.moment().format('YYYY')

      cy.get('footer > div').should('contain', currentYear)
    })

    it('Links to the homepage', () => {
      cy.get('footer > div a').click()
      cy.location('pathname').should('equal', '/')
    })
  })
})
