describe('Header', () => {
  const navLinks = [
    { label: 'Home', path: '/' },
    { label: 'Blog', path: '/blog' },
    { label: 'Reading', path: '/books' },
    { label: 'About', path: '/about' },
  ]

  describe('Nav Menus', () => {
    before(() => {
      cy.visit('/')
    })

    context('720p resolution', () => {
      before(() => {
        cy.viewport(1280, 720)
      })

      it('Displays full header', () => {
        cy.get('nav #menu').should('be.visible')
        cy.get('[data-test="open-menu"]').should('not.be.visible')
      })

      context('Nav Links', () => {
        navLinks.forEach(({ label, path }) => {
          it(`Links to ${label}`, () => {
            cy.get('nav')
              .contains(label)
              .click()

            cy.location('pathname').should('equal', path)
          })
        })
      })
    })

    context('Iphone-5 resolution', () => {
      before(() => {
        cy.viewport('iphone-5')
      })

      it('Toggles mobile menu visibility on or off', () => {
        cy.get('nav #menu').should('not.be.visible')

        cy.get('[data-test=open-menu]')
          .should('be.visible')
          .find('svg.fa-bars')
          .click()

        cy.get('nav #menu')
          .should('be.visible')
          .find('[data-test="close-menu"]')
          .click()

        cy.get('nav #menu').should('not.be.visible')
      })

      context('Nav Links', () => {
        before(() => {
          cy.viewport('iphone-5')
          cy.get('[data-test="open-menu"]')
            .find('svg.fa-bars')
            .click()
        })

        navLinks.forEach(({ label, path }) => {
          it(`Links to ${label}`, () => {
            cy.get('nav')
              .contains(label)
              .click()

            cy.location('pathname').should('equal', path)
          })
        })
      })
    })
  })
})
