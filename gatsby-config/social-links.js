const socialLinks = [
  {
    name: 'LinkedIn',
    url: 'https://www.linkedin.com/in/ornous/',
    title: 'More about me on LinkedIn',
    brandIcon: 'linkedin',
  },
  {
    name: 'Twitter',
    url: 'https://twitter.com/@ornous',
    title: 'Follow me on Twitter',
    brandIcon: 'twitter',
  },
  {
    name: 'GitLab',
    url: 'https://gitlab.com/ornous',
    title: 'Checkout my GitLab projects',
    brandIcon: 'gitlab',
  },
  {
    name: 'GitHub',
    url: 'https://github.com/ornous',
    title: 'My GitHub Profile',
    brandIcon: 'github',
  },
  {
    name: 'GoodReads',
    url: 'https://goodreads.com/ornous',
    title: "See what I'm reading",
    brandIcon: 'goodreads',
  },
]

module.exports = socialLinks
