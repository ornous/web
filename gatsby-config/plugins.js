require('dotenv').config()
const path = require('path')
const fs = require('fs')

const {
  NODE_ENV,
  OFFLINE = false,
  ANALYSE_WEBPACK_BUNDLE = false,
  GOODREADS_API_KEY,
  GOODREADS_USER_ID,
  GA_TRACKING_ID,
  URL: NETLIFY_SITE_URL = 'https://www.ornous.com',
  DEPLOY_PRIME_URL: NETLIFY_DEPLOY_URL = NETLIFY_SITE_URL,
  CONTEXT: NETLIFY_ENV = NODE_ENV,
} = process.env

const sourcePath = path.join(__dirname, '..')

const online = !OFFLINE

// TODO Make it so file can be imported
const theme = fs.readFileSync('./src/themes/warm-antique.js')
const [, mainColor] = theme.toString().match(/primary: '(.*)'/)

const plugins = [
  `gatsby-plugin-ts`,
  `gatsby-plugin-netlify-cache`,
  `gatsby-plugin-react-helmet`,
  {
    resolve: `gatsby-plugin-page-creator`,
    options: {
      path: path.join(sourcePath, 'src', 'pages'),
    },
  },
  // Add static assets before markdown files
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: 'img',
      path: path.join(sourcePath, 'static', 'img'),
    },
  },
  {
    resolve: 'gatsby-source-filesystem',
    options: {
      name: 'pages',
      path: path.join(sourcePath, 'content'),
    },
  },
  {
    resolve: `@jamesdanylik/gatsby-source-goodreads`,
    onlineOnly: true,
    options: {
      key: GOODREADS_API_KEY,
      id: GOODREADS_USER_ID,
    },
  },
  {
    resolve: `gatsby-plugin-emotion`,
    options: {
      // Accepts all options defined by `babel-plugin-emotion` plugin.
    },
  },
  'gatsby-plugin-lodash',
  'gatsby-plugin-react-helmet',
  {
    resolve: 'gatsby-transformer-remark',
    options: {
      plugins: [
        `gatsby-remark-external-links`,
        `gatsby-remark-autolink-headers`,
        /* {
          resolve: 'gatsby-remark-graph',
          options: {
            language: 'mermaid',
            theme: 'forest',
            startOnLoad: false,
          },
        },*/
        `gatsby-remark-embed-youtube`,
        {
          resolve: `gatsby-remark-code-titles`,
          options: {
            className: 'remark-code-title',
          },
        },
        `gatsby-remark-prismjs`,
        // must appear before gatsby-remark-responsive-image
        `gatsby-remark-relative-images`,
        {
          resolve: `gatsby-remark-responsive-image`,
          options: {
            maxWidth: 1400,
          },
        },
        {
          resolve: 'gatsby-remark-emojis',
          options: {
            active: true,
            size: 64,
          },
        },
        `@weknow/gatsby-remark-twitter`,
        'gatsby-remark-smartypants',
        {
          resolve: 'gatsby-remark-external-links',
          options: {
            target: '_blank',
            rel: 'nofollow noopener noreferrer',
          },
        },
      ],
    },
  },
  'gatsby-plugin-sharp',
  'gatsby-transformer-sharp',
  {
    resolve: 'gatsby-plugin-netlify-cms',
    options: {
      modulePath: path.join(sourcePath, 'src', 'cms', 'cms.js'),
    },
  },
  {
    resolve: `gatsby-plugin-sitemap`,
    options: {
      output: `/sitemap.xml`,
      exclude: ['/tags/*', '/recipes', '/recipes/*', '/books'],
    },
  },
  {
    resolve: 'gatsby-plugin-robots-txt',
    options: {
      resolveEnv: () => NETLIFY_ENV,
      env: {
        production: {
          policy: [{ userAgent: '*' }],
        },
        'branch-deploy': {
          policy: [{ userAgent: '*', disallow: ['/'] }],
          sitemap: null,
          host: null,
        },
        'deploy-preview': {
          policy: [{ userAgent: '*', disallow: ['/'] }],
          sitemap: null,
          host: null,
        },
      },
    },
  },
  {
    resolve: `gatsby-plugin-google-analytics`,
    options: {
      trackingId: GA_TRACKING_ID,
      anonymize: true,
      respectDNT: true,
    },
  },
  {
    resolve: `gatsby-plugin-nprogress`,
    options: {
      color: mainColor,
      showSpinner: false,
    },
  },
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: 'Ornous',
      short_name: 'Ornous',
      start_url: '/',
      background_color: mainColor,
      theme_color: mainColor,
      display: 'minimal-ui',
      icon: 'src/images/favicon-512.png',
    },
  },
  {
    resolve: `gatsby-plugin-graphql-codegen`,
  },
  {
    resolve: 'gatsby-plugin-webpack-bundle-analyzer',
    options: {
      analyzerPort: 3000,
      production: true,
      disable: ANALYSE_WEBPACK_BUNDLE,
      openAnalyzer: false,
    },
  },
].filter(plugin => online || !!!plugin.onlineOnly) // TODO Make it simpler

 // place after manifest so it is cached by sw
plugins.push({
  resolve: 'gatsby-plugin-offline',
  options: {
    precachePages: [`/about/`, `/blog/*`],
  },
})

 // make sure to keep it last in the array
plugins.push({
  resolve: 'gatsby-plugin-netlify',
  options: {
    allPageHeaders: [
      "Link: </img/logo.png>; rel=preload; as=image"
    ],
  },
})

module.exports = plugins
