require('dotenv').config()

const { createProxyMiddleware } = require('http-proxy-middleware')

const plugins = require('./plugins')
const socialLinks = require('./social-links')

const {
  NODE_ENV,
  URL: NETLIFY_SITE_URL = 'https://www.ornous.com',
  DEPLOY_PRIME_URL: NETLIFY_DEPLOY_URL = NETLIFY_SITE_URL,
  CONTEXT: NETLIFY_ENV = NODE_ENV,
} = process.env
const isNetlifyProduction = NETLIFY_ENV === 'production'
const siteUrl = isNetlifyProduction ? NETLIFY_SITE_URL : NETLIFY_DEPLOY_URL

module.exports = {
  siteMetadata: {
    lang: 'en-GB',
    title: 'Ousmane Ndiaye, Software Engineer',
    defaultTitle: 'Ousmane Ndiaye, Software Engineer',
    titleTemplate: '%s | ornous.com',
    description: `Welcome, I am Ousmane Ndiaye,
        a software engineer and life-long learner.
        On this website I share some things I've learned along the way.`,
    image: '/icons/icon-48x48.png',
    facebookAppId: '464901247584397',
    twitterUsername: '@ornous',
    siteUrl,
    socialLinks,
    headerMenuItems: [
      { name: 'Home', path: '/' },
      { name: 'Blog', path: '/blog' },
      { name: 'Reading', path: '/books' },
      { name: 'About Me', path: '/about' },
    ],
  },
  pathPrefix: '/',
  developMiddleware: app => {
    app.use(
      '/.netlify/functions/',
      createProxyMiddleware({
        target: 'http://localhost:9000',
        pathRewrite: {
          '/.netlify/functions': '',
        },
      })
    )
  },
  plugins,
}
